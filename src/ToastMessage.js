import React, { Component } from 'react';
import styled from 'styled-components'
import DoneOutline from '@material-ui/icons/DoneOutline'
import { makeStyles } from '@material-ui/core/styles';
import { teal } from '@material-ui/core/colors';

const useStyles = makeStyles(theme => ({
  root: {
    '& > span': {
      margin: theme.spacing(2),
    },
  },
  iconHover: {
    '&:hover': {
      color: teal[50],
    },
  },
}));

const OriginalToast = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 10px;
  position: fixed;
  right: 12px;
  top: 70px; /*header　の高さ */
  z-index: 10000;
  width: 200px;
  color: black;
  background-color: white;
  height: 50px;
  border-left: solid 2px #00AA00;
  @keyframes RightToLeft {
    0% {
      opacity: 1;/* 透明 */
      transform: translateX(50px);/* X軸方向に50px */
    }
    100% {
      transform: translateX(0);
    }
  }
  animation-duration: 0.5s;/* アニメーション時間 */
  animation-name: RightToLeft;/* アニメーション名 */
  animation-iteration-count: 1;/* アニメーションの繰り返し（無限）*/

  p {
    animation-duration: 0.5s;/* アニメーション時間 */
    animation-name: RightToLeft;/* アニメーション名 */
    animation-iteration-count: 1;/* アニメーションの繰り返し（無限）*/
  }
`

const DoneIconContainer = styled.div`
  height: 40px;
  width: 40px;
  border-radius: 50%;
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: #66FF66;
`

class ToastMessage extends Component {

  constructor(props) {
    super(props);
    this.state = {
      display: false
    }
  }

  componentDidUpdate(prevProps) {
    if (prevProps.toast_message !== this.props.toast_message) {
      console.log('props changed')
      this.setState({ display: true })
      this.setState(prevState => ({
        display: true
      }), () => {
        setTimeout(() => {
          this.setState({
            display: false
          });
        }, 4000);
      }
      );
    }
  }

  render() {
    return (
      <>
        {
          this.state.display
            ?
            <OriginalToast>
              <DoneIconContainer>
                <DoneOutline />
              </DoneIconContainer>
              <p style={{ marginLeft: '5px' }}>{this.props.toast_message}</p>
            </OriginalToast>
            :
            null
        }
      </>

    )
  }
}

export default ToastMessage;
