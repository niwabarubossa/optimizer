import firebase from 'firebase';
import { firestore } from '../plugins/firebase'
import 'firebase/firestore';
var storage = firebase.storage();
var storageRef = storage.ref();

export const FIREBASELOGOUT = 'FIREBASELOGOUT'
export const firebaseLogout = () => ({
  type: FIREBASELOGOUT
})
export const SUBMITTWEET = 'SUBMITTEXT'
export const submitTweet = (input_arguments, downloadURL_arr) => async dispatch => {
  //TODO 絆創膏的な措置なので直さなきゃいけない
  if (input_arguments.tweet_status === undefined) {
    input_arguments.tweet_status = "public"
  }
  var hashtagRef = firestore.collection('hashtag');
  var exist_hashtag_array = []
  var exist_hashtag_array_objectid = []
  var not_exist_hashtag_array = []
  var counter;
  const hashtag_in_body = input_arguments.body.split(/\s+/).filter(n => n.includes('#'))
  let firestore_body = input_arguments.body.split(/\s+/).filter(n => !(n.includes('#')))
  firestore_body = firestore_body.join(" ")

  for (counter = 0; counter < hashtag_in_body.length; counter++)
    hashtag_in_body[counter] = hashtag_in_body[counter].slice(1)

  for (counter = 0; counter < hashtag_in_body.length; counter++) {
    var search_parameter = hashtag_in_body[counter]
    await hashtagRef.where("name", "==", search_parameter).get()
      .then(function (querySnapshot) {
        querySnapshot.forEach(function (doc) {
          if (doc.exists) {
            if (!exist_hashtag_array.includes(search_parameter))
              exist_hashtag_array.push(search_parameter)
            if (!exist_hashtag_array_objectid.includes(doc.id))
              exist_hashtag_array_objectid.push(doc.id)
          }
        });
      })
      .catch(function (error) {
        console.log("Error getting documents: ", error);
      });
  }

  for (counter = 0; counter < hashtag_in_body.length; counter++) {
    if (!exist_hashtag_array.includes(hashtag_in_body[counter]))
      not_exist_hashtag_array.push(hashtag_in_body[counter])
  }

  for (counter = 0; counter < not_exist_hashtag_array.length; counter++) {
    await hashtagRef.add({ name: not_exist_hashtag_array[counter], amount: 1 })
  }

  for (counter = 0; counter < exist_hashtag_array_objectid.length; counter++) {
    await hashtagRef.doc(exist_hashtag_array_objectid[counter]).update({
      amount: firebase.firestore.FieldValue.increment(1)
    })
  }

  const tweetDocRef = await firestore.collection('users').doc(input_arguments.current_user.uid).collection('tweets').doc()

  await firestore.collection('users').doc(input_arguments.current_user.uid).collection('tweets').doc(tweetDocRef.id).set({
    score: parseInt(input_arguments.score, 10),
    body: firestore_body,
    image_url: downloadURL_arr[0],
    author_id: input_arguments.current_user.uid,
    author_name: input_arguments.user_in_firestore.displayName,
    author_photo: input_arguments.user_in_firestore.photoURL,
    tweet_status: input_arguments.tweet_status,
    hashtags: hashtag_in_body,
    likers: [],
    like_count: 0,
    tweet_id: Math.floor(Math.random() * 1000000),
    created_at: new Date().getTime(),
    timestamp: firebase.firestore.FieldValue.serverTimestamp()
  }).then(() => {
    dispatch({ type: SUBMITTWEET })
  });

  var temperature = []
  await firestore.collection('users').doc(input_arguments.current_user.uid).get().then(function (doc) {
    if (doc.exists) {
      temperature.push(doc.data())
    } else {
      console.log("No such document!");
    }
  }).catch(function (error) {
    console.log("Error getting document:", error);
  });

  await firestore.collection('users').doc(input_arguments.current_user.uid).update({
    total_action_amount: temperature[0].total_action_amount + 1,
    total_score_amount: temperature[0].total_score_amount + parseInt(input_arguments.score, 10)
  });

  if (input_arguments.tweet_status !== 'private') {
    await firestore.collection('tweets').doc(tweetDocRef.id).set({
      score: parseInt(input_arguments.score, 10),
      body: firestore_body,
      image_url: downloadURL_arr[0],
      author_id: input_arguments.current_user.uid,
      author_name: input_arguments.user_in_firestore.displayName,
      author_photo: input_arguments.user_in_firestore.photoURL,
      tweet_status: input_arguments.tweet_status,
      hashtags: hashtag_in_body,
      likers: [],
      like_count: 0,
      tweet_id: Math.floor(Math.random() * 1000000),
      created_at: new Date().getTime(),
      timestamp: firebase.firestore.FieldValue.serverTimestamp()
    }).then(() => {
      dispatch({ type: SUBMITTWEET })
    });
  }
}

export const SUBMIT_LOADING_START = 'SUBMIT_LOADING_START'
export const submitLoadingStart = () => {
  return {
    type: SUBMIT_LOADING_START,
    submit_loading: true
  }
}
export const SUBMIT_LOADING_FINISH = 'SUBMIT_LOADING_FINISH'
export const submitLoadingFinish = () => {
  return {
    type: SUBMIT_LOADING_FINISH,
    submit_loading: false
  }
}

export const GET_POSTS_SUCCESS = 'GET_POSTS_SUCCESS'
export const getPostsSuccess = (json) => {
  return {
    type: GET_POSTS_SUCCESS,
    get_posts_fetching: false,
    posts: json,
    receivedAt: Date.now()
  }
}

export const getPosts = () => async dispatch => {
  await firestore.collection('tweets').orderBy('created_at', 'desc').limit(30).onSnapshot(function (snapshot) {
    const added_tweet = []
    snapshot.docChanges().forEach(function (change) {
      const data = change.doc.data()
      data.id = change.doc.id
      data.good_loading = false
      if (change.type === "added")
        added_tweet.push(data)
      if (change.type === "modified")
        dispatch(modified(data))
      if (change.type === "removed")
        dispatch(snapshotDeleteTweet(data))
    })
    dispatch(getSnapTweetsSuccess(added_tweet))
  })
}

export const GET_SNAP_TWEETS_SUCCESS = 'GET_SNAP_TWEETS_SUCCESS'
export const getSnapTweetsSuccess = (json) => {
  return {
    type: GET_SNAP_TWEETS_SUCCESS,
    tweets: json,
  }
}

export const MODIFIED = 'MODIFIED'
export const modified = (json) => ({
  type: MODIFIED,
  modifed_tweets: json
})

export const SNAPSHOT_DELETE_TWEET = 'SNAPSHOT_DELETE_TWEET'
export const snapshotDeleteTweet = (data) => {
  return {
    type: SNAPSHOT_DELETE_TWEET,
    delete_tweet: data
  }
}

export const getWeeklyRankingPosts = () => async dispatch => {
  await firestore.collection('weekly_ranking').orderBy('like_count', 'desc').limit(30).onSnapshot(function (snapshot) {
    const added_tweet = []
    snapshot.docChanges().forEach(function (change) {
      const data = change.doc.data()
      data.id = change.doc.id
      data.good_loading = false
      if (change.type === "added")
        added_tweet.push(data)
      if (change.type === "modified")
        dispatch(WeeklyRankingPostsModified(data))
      if (change.type === "removed")
        dispatch(snapshotWeeklyRankingDeleteTweet(data))
    })
    dispatch(getWeeklyRankingPostsSuccess(added_tweet))
  })
}

export const GET_WEEKLY_RANKING_POSTS_SUCCESS = 'GET_WEEKLY_RANKING_POSTS_SUCCESS'
export const getWeeklyRankingPostsSuccess = (json) => {
  return {
    type: GET_WEEKLY_RANKING_POSTS_SUCCESS,
    tweets: json,
  }
}

export const WEEKLY_RANKING_POSTS_MODIFIED = 'WEEKLY_RANKING_POSTS_MODIFIED'
export const WeeklyRankingPostsModified = (json) => ({
  type: WEEKLY_RANKING_POSTS_MODIFIED,
  modifed_tweets: json
})

export const SNAPSHOT_WEEKLY_RANKING_DELETE_TWEET = 'SNAPSHOT_WEEKLY_RANKING_DELETE_TWEET'
export const snapshotWeeklyRankingDeleteTweet = (json) => ({
  type: SNAPSHOT_WEEKLY_RANKING_DELETE_TWEET,
  deleted_tweet: json
})

export const WEEKLY_RANKING_POSTS_GOOD_LOADING_START = 'WEEKLY_RANKING_POSTS_GOOD_LOADING_START'
export const weeklyRankingPostsGoodLoadingStart = (tweet_id) => {
  return {
    type: WEEKLY_RANKING_POSTS_GOOD_LOADING_START,
    tweet_id: tweet_id,
  }
}

export const FAV_POSTS_GOOD_LOADING_START = 'FAV_POSTS_GOOD_LOADING_START'
export const favPostsGoodLoadingStart = (tweet_id) => {
  return {
    type: FAV_POSTS_GOOD_LOADING_START,
    tweet_id: tweet_id,
  }
}

export const MY_POSTS_GOOD_LOADING_START = 'MY_POSTS_GOOD_LOADING_START'
export const myPostsGoodLoadingStart = (tweet_id) => {
  return {
    type: MY_POSTS_GOOD_LOADING_START,
    tweet_id: tweet_id,
  }
}

export const addUserToWeeklyRankingPostsLiker = (current_user, tweet_id) => async dispatch => {
  await firestore.collection('weekly_ranking').doc(tweet_id).collection('liker').doc(current_user.uid).set({
    liker_id: current_user.uid
  }).then(() => {
  });
}

export const removeUserFromWeeklyRankingPostsLiker = (current_user, tweet_id) => async dispatch => {
  await firestore.collection("weekly_ranking").doc(tweet_id).collection('liker').doc(current_user.uid).delete().then(function () {
  }).catch(function (error) {
    console.error("Error removing document: ", error);
  });
}


export const getMyPosts = (uid) => async dispatch => {
  await firestore.collection('tweets').where('author_id', '==', uid).orderBy("created_at", 'desc').limit(10).onSnapshot(function (snapshot) {
    const added_data = []
    snapshot.docChanges().forEach(function (change) {
      const data = change.doc.data()
      data.id = change.doc.id
      if (change.type === "added")
        added_data.push(data)
      if (change.type === "modified")
        dispatch(modified_myposts(data))
      if (change.type === "removed")
        dispatch(snapshotMyPostsDeleteTweet(data))
    })
    dispatch(getMyPostsSnapTweetsSuccess(added_data))
  })
}

export const GET_MY_POSTS_SNAP_TWEETS_SUCCESS = 'GET_MY_POSTS_SNAP_TWEETS_SUCCESS'
export const getMyPostsSnapTweetsSuccess = (json) => {
  return {
    type: GET_MY_POSTS_SNAP_TWEETS_SUCCESS,
    my_posts: json,
  }
}

export const MODIFIED_MY_POSTS = 'MODIFIED_MY_POSTS'
export const modified_myposts = (json) => ({
  type: MODIFIED_MY_POSTS,
  modifed_tweets: json
})

export const SNAPSHOT_MY_POSTS_DELETE_TWEET = 'SNAPSHOT_MY_POSTS_DELETE_TWEET'
export const snapshotMyPostsDeleteTweet = (json) => ({
  type: SNAPSHOT_MY_POSTS_DELETE_TWEET,
  deleted_tweet: json
})

export const GET_MY_FAV_POSTS = 'GET_MY_FAV_POSTS'
export const getMyFavPosts = (uid) => async dispatch => {
  var temp = []
  await firestore.collection('tweets').where('likers', 'array-contains', uid).orderBy("created_at", 'desc').onSnapshot(function (snapshot) {
    const added_data = []
    snapshot.docChanges().forEach(function (change) {
      const data = change.doc.data()
      data.id = change.doc.id
      if (change.type === "added") {
        added_data.push(data)
      }
      if (change.type === "removed") {
        dispatch(deleteMyFavPosts(data))
      }
    })
    dispatch(getMyFavPostsSuccess(added_data))
  })
}

export const DELETE_MY_FAV_POSTS = 'DELETE_MY_FAV_POSTS'
export const deleteMyFavPosts = (json) => {
  return {
    type: DELETE_MY_FAV_POSTS,
    delete_fav_posts: json
  }
}

export const GET_MY_FAV_POSTS_SUCCESS = 'GET_MY_FAV_POSTS'
export const getMyFavPostsSuccess = (json) => {
  return {
    type: GET_MY_FAV_POSTS_SUCCESS,
    my_fav_posts: json
  }
}

export const MODIFIED_MY_FAV_POSTS = 'MODIFIED_MY_FAV_POSTS'
export const modified_my_fav_posts = (json) => ({
  type: MODIFIED_MY_FAV_POSTS,
  modifed_tweets: json
})

export const GET_MY_POSTS_SUCCESS = 'GET_MY_POSTS_SUCCESS'
export const getMyPostsSuccess = (json) => {
  return {
    type: GET_MY_POSTS_SUCCESS,
    my_posts: json,
  }
}

export const getDisplayUserTweets = (uid) => async dispatch => {
  await firestore.collection('tweets').where('author_id', '==', uid).orderBy("created_at", 'desc').onSnapshot(function (snapshot) {
    var temp = []
    snapshot.docChanges().forEach(function (change) {
      if (change.type === "added") {
        var data = change.doc.data()
        data.id = change.doc.id
        temp.push(data)
      }
      if (change.type === "modified") {
        var data = change.doc.data()
        data.id = change.doc.id
        dispatch(modifiedDisplayUserTweet(data))
      }
    })
    dispatch(getDisplayUserTweetsSuccess(temp))
  })
}
export const MODIFIED_DISPLAY_USER_TWEET = 'MODIFIED_DISPLAY_USER_TWEET'
export const modifiedDisplayUserTweet = (json) => ({
  type: MODIFIED_DISPLAY_USER_TWEET,
  modifed_tweets: json
})

export const GET_DISPLAY_USER_TWEETS_SUCCESS = 'GET_DISPLAY_USER_TWEETS_SUCCESS'
export const getDisplayUserTweetsSuccess = (json) => {
  return {
    type: GET_DISPLAY_USER_TWEETS_SUCCESS,
    display_user_tweets: json,
  }
}

export const RESET_DISPLAY_USER_TWEETS = 'RESET_DISPLAY_USER_TWEETS'
export const resetDisplayUserTweets = () => {
  return {
    type: RESET_DISPLAY_USER_TWEETS
  }
}

export const RESET_DISPLAY_USER = 'RESET_DISPLAY_USER'
export const resetDisplayUser = () => {
  return {
    type: RESET_DISPLAY_USER
  }
}

export const GET_WEEKLY_POSTS_SUCCESS = 'GET_WEEKLY_POSTS_SUCCESS'
export const getWeeklyPostsSuccess = (json) => {
  return {
    type: GET_WEEKLY_POSTS_SUCCESS,
    weekly_posts: json,
  }
}
export const GET_WEEKLY_POSTS = 'GET_WEEKLY_POSTS'
export const getWeeklyPosts = (uid) => async dispatch => {
  var today = new Date()
  var last_week_miliseconds = new Date().setDate(new Date().getDate() - 7);
  var last_week = new Date(last_week_miliseconds)
  last_week.setHours(0, 0, 0, 0)
  // await firestore.collection('users').doc(uid).collection('tweets').where('created_at', '>', last_week.getTime())
  await firestore.collection('tweets').where('author_id', '==', uid).where('created_at', '>', last_week.getTime())
    .onSnapshot(function (snapshot) {
      const added_tweet = []
      snapshot.docChanges().forEach(function (change) {
        const data = change.doc.data()
        data.id = change.doc.id
        data.good_loading = false
        if (change.type === "added")
          added_tweet.push(data)
      })
      dispatch(getWeeklyPostsSuccess(added_tweet))
    })
}


export const getWeeklyActionLogs = (uid) => async dispatch => {
  var today = new Date()
  var last_week_miliseconds = new Date().setDate(new Date().getDate() - 31);
  var last_week = new Date(last_week_miliseconds)
  last_week.setHours(0, 0, 0, 0)
  let my_weekly_action_logs = []
  await firestore.collection('users').doc(uid).collection('action_logs').where('created_at', '>', last_week.getTime()).get().then(function (querySnapshot) {
    querySnapshot.forEach(function (doc) {
      my_weekly_action_logs.push(doc.data())
    })
  })
  dispatch(getWeeklyActionLogsSuccess(my_weekly_action_logs))
}

export const GET_WEEKLY_ACTION_LOGS_SUCCESS = 'GET_WEEKLY_ACTION_LOGS_SUCCESS'
export const getWeeklyActionLogsSuccess = (my_weekly_action_logs) => {
  return {
    type: GET_WEEKLY_ACTION_LOGS_SUCCESS,
    my_weekly_action_logs: my_weekly_action_logs
  }
}


export const getSelectedPosts = (tweet_id) => async dispatch => {
  const temperature = []
  await firestore.collection("tweets").where("tweet_id", "==", tweet_id).get().then(function (querySnapshot) {
    querySnapshot.forEach(function (doc) {
      temperature.push(doc.data())
    });
  });
  dispatch(getPostsSuccess(temperature))
}
export const HANDLE_DRAWER_TOGGLE = 'HANDLE_DRAWER_TOGGLE'
export const handleDrawerToggle = () => ({
  type: HANDLE_DRAWER_TOGGLE
})

export const HANDLE_DRAWER_TOGGLE_RESET = 'HANDLE_DRAWER_TOGGLE_reset'
export const handleDrawerToggleReset = () => ({
  type: HANDLE_DRAWER_TOGGLE_RESET
})

export const LOGIN_LOADING_START = 'LOGIN_LOADING_START'
export const loginLoadingStart = () => ({
  type: LOGIN_LOADING_START,
  login_loading: true
})
export const LOGIN_LOADING_FINISH = 'LOGIN_LOADING_FINISH'
export const loginLoadingFinish = () => ({
  type: LOGIN_LOADING_FINISH,
  login_loading: false
})

export const LOGIN_WITH_TWITTER = 'LOGIN_WITH_TWITTER'
export const loginWithTwitter = () => async dispatch => {
  try {
    const user = await (
      signInWithProvider()
    );
    if (user) {
      dispatch(loginWithTwitterSuccess())
    }
    await firestore.collection("users").doc(user.uid).get().then(function (doc) {
      if (doc.exists) {
        dispatch(set_current_user_and_in_firestore(user))
      } else {
        dispatch(first_user(user))
      }
    }).catch(function (error) {
      console.log("Error getting document:", error);
    });
    return user;
  } catch (error) {
    console.log(error);
  }
}


export const persistLoginWithTwitter = () => async dispatch => {
  var credential
  var provider = new firebase.auth.TwitterAuthProvider();
  var anonymous_user = await firebase.auth().currentUser
  await anonymous_user.linkWithPopup(provider).then(function (usercred) {
    var user = usercred.user.providerData[0]
    //ここで、新規データを上書き twitterを
    console.log(user)
    console.log(user.photoURL)
    dispatch(anonymousUserUpdate(user, anonymous_user))
    console.log("Anonymous account successfully upgraded", user);
  }, function (error) {
    console.log("Error upgrading anonymous account", error);
  });
}

export const anonymousUserUpdate = (user, anonymous_user) => async dispatch => {
  await firestore.collection('users').doc(anonymous_user.uid).update({
    displayName: user.displayName,
    email: user.email
  })
}


async function signInWithProvider() {
  try {
    var provider = new firebase.auth.TwitterAuthProvider();
    const response = await firebase.auth().signInWithPopup(provider);
    return response.user;
  } catch (error) {
    throw error;
  }
}

export const first_user = (user) => async dispatch => {
  // 0〜11までの乱数を吐き出す
  const randNum = Math.floor(Math.random() * (11 - 0));
  const photoURL_array = ['https://firebasestorage.googleapis.com/v0/b/optimizer-60ecc.appspot.com/o/anonymous_user_image%2Falpaca-4478245_640.jpg?alt=media&token=2db0f9a2-ded3-4dcc-9c4b-81915d70b4b6', 'https://firebasestorage.googleapis.com/v0/b/optimizer-60ecc.appspot.com/o/anonymous_user_image%2Fbmw-4480738_640.jpg?alt=media&token=b565e36b-bb96-4846-8d21-972e274dd1cf', 'https://firebasestorage.googleapis.com/v0/b/optimizer-60ecc.appspot.com/o/anonymous_user_image%2Fbusiness-4271251_640.png?alt=media&token=14140f05-29b6-497e-9a31-a7fa196ff8db', 'https://firebasestorage.googleapis.com/v0/b/optimizer-60ecc.appspot.com/o/anonymous_user_image%2Fcat-4470578_640.jpg?alt=media&token=8669070c-30e5-442d-ab6f-a75c2f68e9c6', 'https://firebasestorage.googleapis.com/v0/b/optimizer-60ecc.appspot.com/o/anonymous_user_image%2Fcep-4460441_640.jpg?alt=media&token=3f1736a0-1ed8-44e4-b137-c6f3cdb38d76', 'https://firebasestorage.googleapis.com/v0/b/optimizer-60ecc.appspot.com/o/anonymous_user_image%2Fgrapes-4479871_640.jpg?alt=media&token=26db716d-9336-4166-a43c-5b0821df3d1a', 'https://firebasestorage.googleapis.com/v0/b/optimizer-60ecc.appspot.com/o/anonymous_user_image%2Fgunter-grass-4479828_640.jpg?alt=media&token=c6587afb-01e1-41bc-90dd-bfc7b08b5952', 'https://firebasestorage.googleapis.com/v0/b/optimizer-60ecc.appspot.com/o/anonymous_user_image%2Fhappy-4486986_640.jpg?alt=media&token=27cfff8b-eb67-4029-ad65-9492e66b04d2', 'https://firebasestorage.googleapis.com/v0/b/optimizer-60ecc.appspot.com/o/anonymous_user_image%2Fhygge-4477395_640.png?alt=media&token=3f9760cd-878c-4f88-bad3-91e4d36ac5b7', 'https://firebasestorage.googleapis.com/v0/b/optimizer-60ecc.appspot.com/o/anonymous_user_image%2Fmodel-4480916_640.jpg?alt=media&token=e1d21d10-c06e-44b5-805d-3b61b689163c', 'https://firebasestorage.googleapis.com/v0/b/optimizer-60ecc.appspot.com/o/anonymous_user_image%2Fselfie-4483110_640.jpg?alt=media&token=1f7d2229-09ac-4d34-b084-4fabfca2d69f']
  let displayName;
  if (user.displayName === null) {
    displayName = 'USER' + Math.floor(Math.random() * 100000)
  } else {
    displayName = user.displayName
  }
  await firestore.collection('users').doc(user.uid).set({
    uid: user.uid,
    createdAt: new Date(),
    contribution: 0,
    total_action_amount: 0,
    total_score_amount: 0,
    photoURL: photoURL_array[randNum],
    displayName: displayName,
    email: user.email,
    profile: 'プロフィールはまだ入力されていません',
    my_done_actions_total: 0,
    my_logs_score_total: 0,
    my_logs_amount: 0
  }).then(() => {
    console.log('first login!!!')
    console.log(user)
    dispatch(set_current_user_and_in_firestore(user))
  }).catch((err) => {
    console.log(err)
  })
}


export const SET_CURRENT_USER_AND_IN_FIRESTORE = 'SET_CURRENT_USER_AND_IN_FIRESTORE'
export const set_current_user_and_in_firestore = (current_user) => async dispatch => {
  const user_in_firestore = []
  await firestore.collection("users").doc(current_user.uid).get().then(function (doc) {
    if (doc.exists) {
      user_in_firestore.push(doc.data())
    } else {
      console.log("No such document!");
    }
  }).catch(function (error) {
    console.log("Error getting document:", error);
  });
  dispatch({
    type: SET_CURRENT_USER_AND_IN_FIRESTORE,
    user_in_firestore: user_in_firestore[0],
    current_user: current_user
  })
}

export const LOGIN_WITH_TWITTER_SUCCESS = 'LOGIN_WITH_TWITTER_SUCCESS'
export const loginWithTwitterSuccess = () => ({
  type: LOGIN_WITH_TWITTER_SUCCESS,
})

export const GET_USER_INFORMATION = 'GET_USER_INFORMATION'
export const getUserInformation = () => async dispatch => {
  await firebase.auth().onAuthStateChanged(user => {
    if (user) {
      console.log('二回目以降です')
      if (user.isAnonymous) {
        dispatch(anonymousUserStatus())
        dispatch(existUserInFirestore(user))
      }
      dispatch(getUserChartInformation(user))
    } else {
      console.log('初めてです')
      dispatch(loginWithAnonymous())
    }
  });
};

export const existUserInFirestore = (user) => async dispatch => {
  await firestore.collection('users').doc(user.uid).get()
    .then(function (doc) {
      if (!(doc.exists))
        dispatch(first_user(user))
      else
        dispatch(set_current_user_and_in_firestore(user))
    })
}

export const ANONYMOUS_USER_STATUS = 'ANONYMOUS_USER_STATUS'
export const anonymousUserStatus = () => {
  return {
    type: ANONYMOUS_USER_STATUS
  }
}

//匿名ログインアクション
export const loginWithAnonymous = () => async dispatch => {
  await firebase.auth().signInAnonymously().then(
    function (user) {
      dispatch(first_user(user.user))
    })
    .catch(function (error) {
      var errorCode = error.code;
      var errorMessage = error.message;
    });
}

export const GET_USER_CHART_INFORMATION = 'GET_USER_CHART_INFORMATION'
export const getUserChartInformation = (user) => async dispatch => {
  await firestore.collection("users").doc(user.uid).get().then(function (doc) {
    if (doc.exists) {
      dispatch(getUserInformationSuccess(user, doc.data()))
    } else {
      dispatch(getUserInformationFailure())
    }
  }).catch(function (error) {
    console.log("Error getting document:", error);
  });
}

export const GET_USER_INFORMATION_FAILURE = 'GET_USER_INFORMATION_FAILURE'
export const getUserInformationFailure = () => {
  return {
    type: GET_USER_INFORMATION_FAILURE,
    login_loading: false
  }
}

export const GET_USER_INFORMATION_SUCCESS = 'GET_USER_INFORMATION_SUCCESS'
export const getUserInformationSuccess = (current_user, user_in_firestore) => {
  return {
    type: GET_USER_INFORMATION_SUCCESS,
    current_user: current_user,
    user_in_firestore: user_in_firestore,
    login_loading: false
  }
}

export const GET_USER_CHART_INFORMATION_SUCCESS = 'GET_USER_CHART_INFORMATION_SUCCESS'
export const getUserChartInformationSuccess = (current_user) => {
  return {
    type: GET_USER_CHART_INFORMATION_SUCCESS,
    chart_user: current_user
  }
}

export const GET_DISPLAY_USER_INFORMATION = 'GET_DISPLAY_USER_INFORMATION'
export const getDisplayUserInformation = (uid) => async dispatch => {
  await firestore.collection("users").where("uid", "==", uid).get().then(function (querySnapshot) {
    querySnapshot.forEach(function (doc) {
      dispatch(getDisplayUserInformationSuccess(doc.data()))
    });
  });
};
export const GET_DISPLAY_USER_INFORMATION_SUCCESS = 'GET_DISPLAY_USER_INFORMATION_SUCCESS'
export const getDisplayUserInformationSuccess = (display_user) => {
  return {
    type: GET_DISPLAY_USER_INFORMATION_SUCCESS,
    display_user: display_user
  }
}

export const submitImageBeforeSubmitTweet = (input_arguments) => async dispatch => {
  let downloadURL_arr = ['']
  await storageRef.child('tweet_images/' + input_arguments.file_name).put(input_arguments.blob).then(function (snapshot) {
    snapshot.ref.getDownloadURL().then(function (downloadURL) {
      downloadURL_arr[0] = downloadURL
      dispatch(submitTweet(input_arguments, downloadURL_arr))
    }).catch(function (error) {
      console.log('submit image error')
    })
  })
}

export const SUBMIT_IMAGE_TWEET = 'SUBMIT_IMAGE_TWEET'
export const submitImageTweet = (input, downloadURL) => async dispatch => {

  await firestore.collection('test_image_tweet').add({
    image_url: downloadURL[0],
    tweet_id: Math.floor(Math.random() * 1000000),
    created_at: new Date(),
  }).then(() => {
  }).catch(() => {
    console.log('eror')
  })
}

export const GOOD_BUTTON_CLICKED = 'GOOD_BUTTON_CLICKED'
export const goodButtonClicked = (current_user, tweet_id) => async dispatch => {
  await firestore.collection('tweets').doc(tweet_id).collection('liker').doc(current_user.uid).get().then(function (doc) {
    if (doc.exists) {
      console.log("Document data:", doc.data());
      dispatch(removeUserFromLiker(current_user, tweet_id))
    } else {
      dispatch(addUserToLiker(current_user, tweet_id))
      console.log('add user success')
    }
  }).catch(function (error) {
    console.log("Error getting document:", error);
  });
}

export const addUserToLiker = (current_user, tweet_id) => async dispatch => {
  await firestore.collection('tweets').doc(tweet_id).collection('liker').doc(current_user.uid).set({
    liker_id: current_user.uid
  }).then(() => {
    console.log('add success!!!!!!!!!')
  });
}

export const REMOVE_USER_FROM_LIKER = 'REMOVE_USER_FROM_LIKER'
export const removeUserFromLiker = (current_user, tweet_id) => async dispatch => {
  await firestore.collection("tweets").doc(tweet_id).collection('liker').doc(current_user.uid).delete().then(function () {
  }).catch(function (error) {
    console.error("Error removing document: ", error);
  });
}

export const GOOD_BUTTON_LOADING_START = 'GOOD_BUTTON_LOADING_START'
export const goodButtonLoadingStart = (tweet_id) => {
  return {
    type: GOOD_BUTTON_LOADING_START,
    tweet_id: tweet_id,
  }
}
export const GOOD_BUTTON_LOADING_START_IN_USERPAGE = 'GOOD_BUTTON_LOADING_START_IN_USERPAGE'
export const goodButtonLoadingStartInUserPage = (tweet_id) => {
  return {
    type: GOOD_BUTTON_LOADING_START_IN_USERPAGE,
    tweet_id: tweet_id,
  }
}

export const GOOD_BUTTON_IN_SEARCH_PAGE_LOADING_START = 'GOOD_BUTTON_IN_SEARCH_PAGE_LOADING_START'
export const goodButtonLoadingInSearchStart = (objectID) => {
  return {
    type: GOOD_BUTTON_IN_SEARCH_PAGE_LOADING_START,
    objectID: objectID,
  }
}

export const SEARCH_TWEETS = 'SEARCH_TWEETS'
export const searchTweets = (temp_results) => {
  return {
    type: SEARCH_TWEETS,
    search_results: temp_results
  }
}

export const SEARCH_WEEKLY_RANKING_TWEETS = 'SEARCH_WEEKLY_RANKING_TWEETS'
export const searchWeeklyRankingTweets = (weekly_ranking_search_results) => {
  return {
    type: SEARCH_WEEKLY_RANKING_TWEETS,
    weekly_ranking_search_results: weekly_ranking_search_results
  }
}

export const GOOD_BUTTON_LOADING_FINISH_IN_SEARCH_AND_REMOVE = 'GOOD_BUTTON_LOADING_FINISH_IN_SEARCH_AND_REMOVE'
export const goodButtonLoadingFinishInSearchAndRemove = (objectID) => {
  return {
    type: GOOD_BUTTON_LOADING_FINISH_IN_SEARCH_AND_REMOVE,
    objectID: objectID
  }
}

export const GOOD_BUTTON_LOADING_FINISH_IN_SEARCH_AND_ADD = 'GOOD_BUTTON_LOADING_FINISH_IN_SEARCH_AND_ADD'
export const goodButtonLoadingFinishInSearchAndAdd = (objectID) => {
  return {
    type: GOOD_BUTTON_LOADING_FINISH_IN_SEARCH_AND_ADD,
    objectID: objectID
  }
}

export const GET_MORE_POSTS_REQUEST = 'GET_MORE_POSTS_REQUEST'
export const getMorePostsRequest = (end_tweets) => async dispatch => {
  const tweet_per_click = 30
  dispatch(getMorePostsStart())
  await firestore.collection('tweets').orderBy('created_at', 'desc').startAfter(end_tweets.created_at).limit(tweet_per_click)
    .onSnapshot(function (snapshot) {
      const added_tweet = []
      snapshot.docChanges().forEach(function (change) {
        const data = change.doc.data()
        data.id = change.doc.id
        data.good_loading = false
        if (change.type === "added") {
          added_tweet.push(data)
        }
        if (change.type === "modified") {
          dispatch(getMorePostsModified(data))
        }
      })
      dispatch(getMorePostsSuccess(added_tweet))
    })
}

export const GET_MORE_POSTS_START = 'GET_MORE_POSTS_START'
export const getMorePostsStart = () => {
  return {
    type: GET_MORE_POSTS_START
  }
}
export const GET_MORE_POSTS_MODIFIED = 'GET_MORE_POSTS_MODIFIED'
export const getMorePostsModified = (json) => {
  return {
    type: GET_MORE_POSTS_MODIFIED,
    modified_tweet: json
  }
}

export const GET_MORE_POSTS_SUCCESS = 'GET_MORE_POSTS_SUCCESS'
export const getMorePostsSuccess = (json) => {
  return {
    type: GET_MORE_POSTS_SUCCESS,
    tweets: json
  }
}

export const getPopularHashtags = () => async dispatch => {
  const popular_hashtags = []
  await firestore.collection('hashtag').orderBy('amount', 'desc').orderBy('name', 'desc').limit(10).get()
    .then(function (querySnapshot) {
      querySnapshot.forEach(function (doc) {
        popular_hashtags.push(doc.data())
      })
      dispatch(getPopularHashtagSuccess(popular_hashtags))
    }
    )
}

export const GET_POPULAR_HASHTAGS_SUCCESS = 'GET_POPULAR_HASHTAGS_SUCCESS'
export const getPopularHashtagSuccess = (popular_hashtags) => {
  return {
    type: GET_POPULAR_HASHTAGS_SUCCESS,
    popular_hashtags: popular_hashtags
  }
}

export const getMorePopularHashtags = (end_at) => async dispatch => {
  let more_popular_hashtags = []
  await firestore.collection('hashtag').orderBy('amount', 'desc').orderBy('name', 'desc').startAfter(end_at.amount, end_at.name).limit(10).get().then(function (querySnapshot) {
    querySnapshot.forEach(function (doc) {
      more_popular_hashtags.push(doc.data())
    })
    dispatch(getMorePopularHashtagSuccess(more_popular_hashtags))
  }
  )
}

export const GET_MORE_POPULAR_HASHTAGS_SUCCESS = 'GET_MORE_POPULAR_HASHTAGS_SUCCESS'
export const getMorePopularHashtagSuccess = (more_popular_hashtags) => {
  return {
    type: GET_MORE_POPULAR_HASHTAGS_SUCCESS,
    more_popular_hashtags: more_popular_hashtags
  }
}


export const updateUserInformationRequest = (argument, image_url_arr) => async dispatch => {
  await firestore.collection('users').doc(argument.current_user.uid).update({
    displayName: argument.displayName,
    profile: argument.profile,
    photoURL: image_url_arr[0]
  }).then(
    dispatch(updateUserInformationSuccess())
  ).catch(
    dispatch(updateUserInformationFailure())
  )
}

export const UPDATE_USER_INFORMATION_SUCCESS = 'UPDATE_USER_INFORMATION_SUCCESS'
export const updateUserInformationSuccess = () => {
  return {
    type: UPDATE_USER_INFORMATION_SUCCESS
  }
}

export const UPDATE_USER_INFORMATION_FAILURE = 'UPDATE_USER_INFORMATION_FAILURE'
export const updateUserInformationFailure = () => {
  return {
    type: UPDATE_USER_INFORMATION_FAILURE
  }
}

export const deleteTweet = (current_user, tweet_data) => async dispatch => {

  await firestore.collection('tweets').doc(tweet_data.id).delete().then(function () {
    dispatch(userDataUpdateAfterDeleteTweet(current_user, tweet_data))
  }).catch(function (error) {
    console.error("Error removing document: ", error);
  });

  await firestore.collection('users').doc(current_user.uid).collection('tweets').doc(tweet_data.id).delete().then(function () {
    console.log('success')
  }).catch(function (error) {
    console.error("Error removing document: ", error);
  });

  await firestore.collection('weekly_ranking').doc(tweet_data.id).delete().then(function () {
  }).catch(function (error) {
    console.error("Error removing document: ", error);
  });
}

export const userDataUpdateAfterDeleteTweet = (current_user, tweet_data) => async dispatch => {
  await firestore.collection('users').doc(current_user.uid).update({
    total_action_amount: firebase.firestore.FieldValue.increment(-1.0),
    total_score_amount: firebase.firestore.FieldValue.increment(-(tweet_data.score)),
    contribution: firebase.firestore.FieldValue.increment(-(tweet_data.like_count))
  }).then(function () {
    console.log('update success')
  })
}

export const GOOD_BUTTON_LOADING_START_IN_TOP_PAGE = 'GOOD_BUTTON_LOADING_START_IN_TOP_PAGE'
export const goodButtonLoadingStartInTopPage = (objectID) => {
  return {
    type: GOOD_BUTTON_LOADING_START_IN_TOP_PAGE,
    objectID: objectID,
  }
}

export const GOOD_BUTTON_LOADING_FINISH_IN_TOPPAGE_AND_REMOVE = 'GOOD_BUTTON_LOADING_FINISH_IN_TOPPAGE_AND_REMOVE'
export const goodButtonLoadingFinishInToppageAndRemove = (objectID) => {
  return {
    type: GOOD_BUTTON_LOADING_FINISH_IN_TOPPAGE_AND_REMOVE,
    objectID: objectID
  }
}

export const GOOD_BUTTON_LOADING_FINISH_IN_TOPPAGE_AND_ADD = 'GOOD_BUTTON_LOADING_FINISH_IN_TOPPAGE_AND_ADD'
export const goodButtonLoadingFinishInToppageAndAdd = (objectID) => {
  return {
    type: GOOD_BUTTON_LOADING_FINISH_IN_TOPPAGE_AND_ADD,
    objectID: objectID
  }
}

export const matchActionRefUpdate = (argument) => async dispatch => {
  await firestore.collection('actions').doc(argument.completely_match.objectID).update({
    log_amount: firebase.firestore.FieldValue.increment(1),
    total_logs_score: firebase.firestore.FieldValue.increment(argument.input_action_score),
    total_done_actions_amount: firebase.firestore.FieldValue.increment(argument.input_action_amount)
  })
}

export const myActionsRefUpdate = (argument) => async dispatch => {
  let currentUserRef = firestore.collection('users').doc(argument.uid)
  let myActionsRef = firestore.collection('users').doc(argument.uid).collection('actions')
  await myActionsRef.doc(argument.completely_match.objectID).update({
    log_amount: firebase.firestore.FieldValue.increment(1),
    total_logs_score: firebase.firestore.FieldValue.increment(argument.input_action_score),
    total_done_actions_amount: firebase.firestore.FieldValue.increment(argument.input_action_amount)
  })
}

export const myActionsRefSet = (argument) => async dispatch => {
  let myActionsRef = firestore.collection('users').doc(argument.uid).collection('actions')
  await myActionsRef.doc(argument.completely_match.objectID).set({
    id: argument.completely_match.objectID,
    name: argument.completely_match.name,
    log_amount: 1,
    total_logs_score: argument.input_action_score,
    total_done_actions_amount: argument.input_action_amount
  })
}

export const newActionToRoot = (argument) => async dispatch => {
  await argument.newActionDocRef.set({
    id: argument.newActionDocRef.id,
    name: argument.input_action_name,
    log_amount: 1,
    kaizen_amount: 0,
    total_done_actions_amount: argument.input_action_amount,
    total_logs_score: argument.input_action_score
  })
}

export const myActionsRefNewSet = (argument) => async dispatch => {
  let myActionsRef = firestore.collection('users').doc(argument.uid).collection('actions')
  await myActionsRef.doc(argument.newActionDocRef.id).set({
    id: argument.newActionDocRef.id,
    name: argument.input_action_name,
    log_amount: 1,
    total_done_actions_amount: argument.input_action_amount,
    total_logs_score: argument.input_action_score
  });
}

export const getActionLogRequest = () => async dispatch => {
  await firestore.collection('action_logs').orderBy('created_at', 'desc').limit(30).onSnapshot(function (snapshot) {
    const added_action_logs = []
    snapshot.docChanges().forEach(function (change) {
      const data = change.doc.data()
      data.id = change.doc.id
      data.good_loading = false
      if (change.type === "added")
        added_action_logs.push(data)
      if (change.type === "modified")
        dispatch(modifiedActionLog(data))
      if (change.type === "removed")
        dispatch(deleteActionLog(data))
    })
    dispatch(getActionLogRequestSuccess(added_action_logs))
  })
}

export const GET_ACTION_LOG_REQUEST_SUCCESS = 'GET_ACTION_LOG_REQUEST_SUCCESS'
export const getActionLogRequestSuccess = (data) => {
  return {
    type: GET_ACTION_LOG_REQUEST_SUCCESS,
    action_logs: data,
  }
}

export const MODIFIED_ACTION_LOG = 'MODIFIED_ACTION_LOG'
export const modifiedActionLog = (data) => ({
  type: MODIFIED_ACTION_LOG,
  modifed_action_logs: data
})

export const DELETE_ACTION_LOG = 'DELETE_ACTION_LOG'
export const deleteActionLog = (data) => {
  return {
    type: SNAPSHOT_DELETE_TWEET,
    delete_action_logs: data
  }
}

export const ACTION_LOG_GOOD_BUTTON_LOADING_START = 'ACTION_LOG_GOOD_BUTTON_LOADING_START'
export const actionLogGoodButtonLoadingStart = (actoin_log_id, state_name) => {
  return {
    type: ACTION_LOG_GOOD_BUTTON_LOADING_START,
    actoin_log_id: actoin_log_id,
    state_name: state_name
  }
}

export const addUserToActionLogLiker = (current_user, action_log, user_in_firestore) => async dispatch => {
  const liker_data = {
    liker_id: current_user.uid,
    liker_photoURL: user_in_firestore.photoURL,
    liker_displayName: current_user.displayName,
    liker_profile: user_in_firestore.profile
  }
  var batch = firestore.batch();
  const rootActionLogRef = firestore.collection("action_logs").doc(action_log.id).collection('liker').doc(current_user.uid)
  const userActionLogRef = firestore.collection("users").doc(action_log.author_id).collection('action_logs').doc(action_log.id).collection('likers').doc(current_user.uid)
  await batch.set(rootActionLogRef, liker_data);
  await batch.set(userActionLogRef, liker_data)
  await batch.commit().then(function () {
  });
}

export const removeUserToActionLogLiker = (current_user, action_log) => async dispatch => {
  var batch = firestore.batch();
  const rootActionLogRef = firestore.collection("action_logs").doc(action_log.id).collection('liker').doc(current_user.uid)
  const userActionLogRef = firestore.collection("users").doc(action_log.author_id).collection('action_logs').doc(action_log.id).collection('likers').doc(current_user.uid)
  await batch.delete(rootActionLogRef)
  await batch.delete(userActionLogRef)
  await batch.commit().then(function () {
  });
}

export const uploadUserImageAndUpdate = (input_arguments) => async dispatch => {
  let downloadURL_arr = ['']
  await storageRef.child('user_images/' + input_arguments.file_name).put(input_arguments.blob).then(function (snapshot) {
    snapshot.ref.getDownloadURL().then(function (downloadURL) {
      downloadURL_arr[0] = downloadURL
      dispatch(updateUserInformationRequest(input_arguments, downloadURL_arr))
    }).catch(function (error) {
      console.log('submit image error')
    })
  })
}

export const getDisplayUserActionLogRequest = (uid) => async dispatch => {
  await firestore.collection('action_logs').where('author_id', '==', uid).orderBy('created_at', 'desc').limit(30).onSnapshot(function (snapshot) {
    const added_action_logs = []
    snapshot.docChanges().forEach(function (change) {
      const data = change.doc.data()
      data.id = change.doc.id
      data.good_loading = false
      if (change.type === "added")
        added_action_logs.push(data)
      if (change.type === "modified")
        dispatch(modifiedDisplayUserActionLog(data))
      if (change.type === "removed")
        dispatch(deleteDisplayUserActionLog(data))
    })
    dispatch(getDisplayUserActionLogRequestSuccess(added_action_logs))
  })
}

export const GET_DISPLAY_USER_ACTION_LOG_REQUEST_SUCCESS = 'GET_DISPLAY_USER_ACTION_LOG_REQUEST_SUCCESS'
export const getDisplayUserActionLogRequestSuccess = (data) => {
  return {
    type: GET_DISPLAY_USER_ACTION_LOG_REQUEST_SUCCESS,
    display_user_action_logs: data,
  }
}

export const MODIFIED_DISPLAY_USER_ACTION_LOG = 'MODIFIED_DISPLAY_USER_ACTION_LOG'
export const modifiedDisplayUserActionLog = (data) => ({
  type: MODIFIED_DISPLAY_USER_ACTION_LOG,
  modifed_display_user_action_logs: data
})

export const DELETE_DISPLAY_USER_ACTION_LOG = 'DELETE_DISPLAY_USER_ACTION_LOG'
export const deleteDisplayUserActionLog = (data) => {
  return {
    type: DELETE_DISPLAY_USER_ACTION_LOG,
    delete_display_user_action_logs: data
  }
}

export const getDisplayUserMonthlyPosts = (uid) => async dispatch => {
  var last_month_miliseconds = new Date().setDate(new Date().getDate() - 31);
  var last_month = new Date(last_month_miliseconds)
  last_month.setHours(0, 0, 0, 0)
  // await firestore.collection('users').doc(uid).collection('tweets').where('created_at', '>', last_week.getTime())
  await firestore.collection('tweets').where('author_id', '==', uid).where('created_at', '>', last_month.getTime())
    .onSnapshot(function (snapshot) {
      const added_tweet = []
      snapshot.docChanges().forEach(function (change) {
        const data = change.doc.data()
        data.id = change.doc.id
        data.good_loading = false
        if (change.type === "added")
          added_tweet.push(data)
      })
      dispatch(getDisplayUserMonthlyPostsSuccess(added_tweet))
    })
}

export const GET_DISPLAY_USER_MONTHLY_POSTS_SUCCESS = 'GET_DISPLAY_USER_MONTHLY_POSTS_SUCCESS'
export const getDisplayUserMonthlyPostsSuccess = (posts) => {
  return {
    type: GET_DISPLAY_USER_MONTHLY_POSTS_SUCCESS,
    display_user_monthly_posts: posts
  }
}

export const getDisplayUserMonthlyActionLogs = (uid) => async dispatch => {
  var last_month_miliseconds = new Date().setDate(new Date().getDate() - 31);
  var last_month = new Date(last_month_miliseconds)
  last_month.setHours(0, 0, 0, 0)
  let display_user_monthly_action_logs = []
  await firestore.collection('users').doc(uid).collection('action_logs').where('created_at', '>', last_month.getTime()).get().then(function (querySnapshot) {
    querySnapshot.forEach(function (doc) {
      display_user_monthly_action_logs.push(doc.data())
    })
  })
  dispatch(getDisplayUserMonthlyActionLogsSuccess(display_user_monthly_action_logs))
}

export const GET_DISPLAY_USER_MONTHLY_ACTION_LOGS_SUCCESS = 'GET_DISPLAY_USER_MONTHLY_ACTION_LOGS_SUCCESS'
export const getDisplayUserMonthlyActionLogsSuccess = (display_user_monthly_action_logs) => {
  return {
    type: GET_DISPLAY_USER_MONTHLY_ACTION_LOGS_SUCCESS,
    display_user_monthly_action_logs: display_user_monthly_action_logs
  }
}

export const getMoreActionLogsRequest = (end_action_log) => async dispatch => {
  const action_logs_per_click = 30
  // dispatch(getMorePostsStart())
  await firestore.collection('action_logs').orderBy('created_at', 'desc').startAfter(end_action_log.created_at).limit(action_logs_per_click)
    .onSnapshot(function (snapshot) {
      const added_action_logs = []
      snapshot.docChanges().forEach(function (change) {
        const data = change.doc.data()
        data.id = change.doc.id
        data.good_loading = false
        if (change.type === "added")
          added_action_logs.push(data)
        if (change.type === "modified")
          dispatch(getMoreActionLogsModified(data))
      })
      dispatch(getMoreActionLogsSuccess(added_action_logs))
    })
}

export const GET_MORE_ACTION_LOGS_MODIFIED = 'GET_MORE_ACTION_LOGS_MODIFIED'
export const getMoreActionLogsModified = (json) => {
  return {
    type: GET_MORE_ACTION_LOGS_MODIFIED,
    modified_action_log: json
  }
}

export const GET_MORE_ACTION_LOGS_SUCCESS = 'GET_MORE_ACTION_LOGS_SUCCESS'
export const getMoreActionLogsSuccess = (json) => {
  return {
    type: GET_MORE_ACTION_LOGS_SUCCESS,
    more_action_logs: json
  }
}

export const GET_MORE_ACTION_LOGS_START = 'GET_MORE_ACTION_LOGS_START'
export const getMoreActionLogsStart = () => {
  return{
    type: GET_MORE_ACTION_LOGS_START
  }
}

export const GET_MORE_ACTION_LOGS_FINISH = 'GET_MORE_ACTION_LOGS_FINISH'
export const getMoreActionLogsFinish = () => {
  return {
    type: GET_MORE_ACTION_LOGS_FINISH
  }
}