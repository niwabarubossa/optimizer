import React, { Component } from 'react';
import classes from '../../../assets/firstTaskPage/MainContainer.css'
import TopContent from './TopContent'
import MiddleContent from './MiddleContent'
// import { } from '../actions'
import { connect } from 'react-redux'
import RegisterForm from './RegisterForm'

class MainContainer extends Component {

  render() {
    return (
      <div className={classes.managementMainContainer}>
        {
          this.props.user_in_firestore && this.props.user_in_firestore.first_task
            ?
            <React.Fragment>
              <TopContent />
              <MiddleContent />
            </React.Fragment>
            :
            <RegisterForm />
        }
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    user_in_firestore: state.firebase.user_in_firestore
  }
}

const mapDispatchToProps = ({})

export default connect(mapStateToProps, mapDispatchToProps)(MainContainer)
