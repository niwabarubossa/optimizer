import React, { Component } from 'react';
import classes from '../../../assets/firstTaskPage/ChartContainer.css'
import ComposedChartContainer from '../ComposedChartContainer'
import { getPosts, getWeeklyPosts, getUserInformation, set_current_user_and_in_firestore } from '../../../actions'
import { connect } from 'react-redux'
import CircularProgress from '@material-ui/core/CircularProgress';
import moment from 'moment';
import Typography from '@material-ui/core/Typography'

function makeLocalMoldedData(first_tasks, total_score_amount, total_action_amount) {
  let counter
  let weekly_score_array = [0, 0, 0, 0, 0, 0, 0, 0]
  let weekly_amount_array = [0, 0, 0, 0, 0, 0, 0, 0]
  let local_molded_data = {}
  let key_date_value_arrayindex = {}
  let new_today_moment = moment(new Date())
  let index_to_date_pair = []
  for (counter = 0; counter <= 7; counter++) {
    key_date_value_arrayindex[new_today_moment.clone().add(counter - 7, 'days').format('MM-DD')] = { index: counter, score_amount: 0, action_amount: 0 }
    index_to_date_pair[counter] = new_today_moment.clone().add(counter - 7, 'days').format('MM-DD')
  }
  //エラー発生を防ぐ珠江、一応todayのところは使わないが、対応できるようにさせとく

  for (counter = 0; counter <= first_tasks.length - 1; counter++) {
    let weekly_post_date_is = moment(first_tasks[counter].created_at).format('MM-DD')
    let hairu_index = key_date_value_arrayindex[weekly_post_date_is].index
    weekly_amount_array[hairu_index]++
    weekly_score_array[hairu_index] += first_tasks[counter].score
  }

  weekly_amount_array.shift()
  weekly_score_array.shift()

  let object_length = Object.keys(key_date_value_arrayindex).length;
  let temp_total_amount_minus = 0
  let temp_total_score_minus = 0

  for (counter = weekly_amount_array.length - 1; counter >= 0; counter--) {
    temp_total_amount_minus -= weekly_amount_array[counter]
    temp_total_score_minus -= weekly_score_array[counter]
    weekly_amount_array[counter] = temp_total_amount_minus
    weekly_score_array[counter] = temp_total_score_minus
  }

  var chart_score_data = [0, 0, 0, 0, 0, 0, 0, 0]
  var chart_action_amount_data = [0, 0, 0, 0, 0, 0, 0, 0]
  var chart_index_to_date_paiindex_to_date_pair = index_to_date_pair

  for (counter = 0; counter < object_length - 1; counter++) {
    chart_score_data[counter] = total_score_amount + weekly_score_array[counter]
    chart_action_amount_data[counter] = total_action_amount + weekly_amount_array[counter]
  }
  chart_score_data[object_length - 1] = total_score_amount
  chart_action_amount_data[object_length - 1] = total_action_amount

  local_molded_data.chart_score_data = chart_score_data
  local_molded_data.chart_action_amount_data = chart_action_amount_data
  local_molded_data.chart_index_to_date_paiindex_to_date_pair = chart_index_to_date_paiindex_to_date_pair

  return local_molded_data
}

class ChartContainer extends Component {

  constructor(props) {
    super(props);
    this.state = {
      local_molded_data: null
    }
  }

  async componentDidMount() {
    if (this.props.first_tasks.length === 0 && this.props.current_user)
      await this.props.getWeeklyPosts(this.props.current_user.uid)

    if (this.props.first_tasks && this.props.user_in_firestore) {
      var local_molded_data = makeLocalMoldedData(this.props.first_tasks, this.props.user_in_firestore.first_task_total_score_amount, this.props.user_in_firestore.first_task_total_action_amount)
      this.setState({ local_molded_data: local_molded_data })
    }
  }

  async componentDidUpdate(prevProps) {
    if (prevProps.current_user !== this.props.current_user)
      await this.props.getWeeklyPosts(this.props.current_user.uid)

    if (!this.props.user_in_firestore && this.props.current_user)
      await this.props.set_current_user_and_in_firestore(this.props.current_user)

    if ((prevProps.user_in_firestore !== this.props.user_in_firestore) || (prevProps.first_tasks !== this.props.first_tasks)) {
      if (this.props.first_tasks) {
        var local_molded_data = makeLocalMoldedData(this.props.first_tasks, this.props.user_in_firestore.first_task_total_score_amount, this.props.user_in_firestore.first_task_total_action_amount)
        this.setState({ local_molded_data: local_molded_data })
      }
    }
  }

  render() {
    return (
      this.props.current_user
        ?
        <div className={classes.chartContainer}>
          <div className={classes.barChartContainer}>
            {this.state.local_molded_data
              ?
              <ComposedChartContainer height={500} local_molded_data={this.state.local_molded_data} />
              :
              <CircularProgress />
            }
          </div>
        </div>
        :
        <div style={{ marginTop: '20px' }}>
          <Typography>登録すると記録が表示されます</Typography>
        </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    first_tasks: state.firebase.first_tasks,
    current_user: state.firebase.current_user,
    user_in_firestore: state.firebase.user_in_firestore
  }
}
const mapDispatchToProps = ({ getPosts, getWeeklyPosts, getUserInformation, set_current_user_and_in_firestore })
export default connect(mapStateToProps, mapDispatchToProps)(ChartContainer)