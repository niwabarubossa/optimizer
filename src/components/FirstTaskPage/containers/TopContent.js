import React, { Component } from 'react';
import ColorBox from '../ColorBox'
import classes from '../../../assets/firstTaskPage/TopContent.css'
import { Grid } from "@material-ui/core"
import { getUserChartInformation, getUserInformation, set_current_user_and_in_firestore } from '../../../actions'
import { connect } from 'react-redux'
import Button from '@material-ui/core/Button'
import { Link } from 'react-router-dom'

class TopContent extends Component {

  constructor(props) {
    super(props);
    this.onReNewActionClicked = this.onReNewActionClicked.bind(this)
  }

  componentDidMount() {
    // this.props.user_in_firestore.first_task === null
    // first_task_id,task1_start_date,task1_action_rate: null を値を新たにセットする
  }

  onReNewActionClicked() {
    window.confirm('本当によろしいですか？ 今までのタスクの積み上げは加算されたまま残ります。')
    // users/user の first_task_id,task1_start_date,task1_action_rate: null にリロード
    // task1_action_rate; 確率  task1_start_date:始めた時期
    //新たに actionを作成して、そのidをfirst_action_idにset doc.idは共通化すること
  }

  render() {
    return (
      <div className={classes.topContentContainer}>
        {
          this.props.user_in_firestore
            ?
            <React.Fragment>
              <div style={{ marginBottom: '20px' }}>
                <h4>タスク１</h4>
              </div>
              <div style={{ marginBottom: '20px' }}>
                <Button variant="contained">
                  <Link to={"/first_task/edit"}>
                    現在のタスク情報を編集する
                </Link>
                </Button>
              </div>
              <div>
                <Button variant="contained" onClick={this.onReNewActionClicked} >
                  別のタスクを新たに作る
                </Button>
              </div>
              <div>
                現在のタスクについての自由記述欄です...
              </div>
            </React.Fragment>
            :
            null
        }
        <Grid container alignItems="center" justify="center">
          <Grid item xs={4}>
            {
              this.props.user_in_firestore ?
                <ColorBox style={{ backgroundColor: '#1fa8d8', height: '100px' }} header='積み上げ数' score={this.props.user_in_firestore.first_task_total_action_amount} />
                :
                <ColorBox style={{ backgroundColor: '#1fa8d8', height: '100px' }} header='積み上げ数' score="" />
            }
          </Grid>
          <Grid item xs={4}>
            {
              this.props.user_in_firestore ?
                <ColorBox style={{ backgroundColor: '#63c2de', height: '100px' }} header='累積スコア' score={this.props.user_in_firestore.first_task_total_score_amount} />
                :
                <ColorBox style={{ backgroundColor: '#63c2de', height: '100px' }} header='累積スコア' score='' />
            }
          </Grid>
          <Grid item xs={4}>
            {
              this.props.user_in_firestore ?
                <ColorBox style={{ backgroundColor: '#1fa8d8', height: '100px' }} header='contribution' score={this.props.user_in_firestore.first_task_contribution} />
                :
                <ColorBox style={{ backgroundColor: '#1fa8d8', height: '100px' }} header='contribution' score="" />
            }
          </Grid>
        </Grid>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    current_user: state.firebase.current_user,
    user_in_firestore: state.firebase.user_in_firestore
  }
}
const mapDispatchToProps = ({ getUserChartInformation, getUserInformation, set_current_user_and_in_firestore })
export default connect(mapStateToProps, mapDispatchToProps)(TopContent)