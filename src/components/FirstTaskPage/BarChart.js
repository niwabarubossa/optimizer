import React, { Component } from 'react';
import { Bar } from 'react-chartjs-2';
import * as zoom from 'chartjs-plugin-zoom';

class BarChart extends Component {

  constructor(props) {
    super(props);
    this.state = {
      labels: this.props.labels,
      chart_action_amount_data: this.props.chart_action_amount_data
    }
  }

  render() {
    const data = {
      labels: this.state.labels,
      datasets: [
        {
          "backgroundColor": "rgb(99, 194, 222)",
          "label": "タスク１行動数",
          data: this.props.chart_action_amount_data,
        },
      ],
    };
    const options = {
      legend: {
        position: 'top',
      },
      scales: {
        xAxes: [{
          ticks: {
            autoSkip: true,
            maxRotation: 0,
            minRotation: 0
          }
        }]
      },
      pan: {
        enabled: true,
        mode: 'xy'
      },
      zoom: {
        sensitivity: 2,
        speed: 10,
        drag: false,
        enabled: true,
        mode: 'x'
      }
    }
    return (
      <Bar data={data} options={options} />
    );
  }
}

export default BarChart