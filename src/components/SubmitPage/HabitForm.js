import React, { Component } from 'react';
import { } from '../../actions'
import { connect } from 'react-redux'
import { Field, reduxForm } from 'redux-form'
import firebase from 'firebase'
import Button from '@material-ui/core/Button'
import AccessibilityNew from '@material-ui/icons/AccessibilityNew'
import HorizontalSplit from '@material-ui/icons/HorizontalSplit'
import TrendingUp from '@material-ui/icons/TrendingUp'
import styled from 'styled-components'
import { DivFlexCenterWrapper } from '../atomic/Layout'
import { SelectWrap } from '../atomic/Parts.js'
import { firestore } from '../../plugins/firebase'
import moment from 'moment';
import { matchActionRefUpdate, myActionsRefUpdate, myActionsRefSet, newActionToRoot, myActionsRefNewSet, submitLoadingStart, submitLoadingFinish } from '../../actions'
import LinearProgress from '@material-ui/core/LinearProgress';
import CircularProgress from '@material-ui/core/CircularProgress';
import Typography from '@material-ui/core/Typography';

const algoliasearch = require('algoliasearch')
const client = algoliasearch(process.env.REACT_APP_ALGOLIA_ID, process.env.REACT_APP_ADMIN_API_KEY)

const BaseForm = styled.input`
  border: 1px solid #aaa;
  border-radius: 4px;
  margin: 8px 0;
  outline: none;
  padding: 8px;
  box-sizing: border-box;
  padding-left: 40px;
  font-size: 16px;
`
const ActionNameForm = styled(BaseForm)`
  width: 300px;
`;
const SmallInput = styled(BaseForm)`
  width: 120px;
`
const MuiIconWrap = styled.div`
padding:0;
margin: 0;
  svg{
    position: absolute;
    left: 0;
    top: 6px;
    padding: 9px 8px;
    color: #aaa;
    width: 40px;
    height: 40px;
  }
`

class HabitForm extends Component {
  constructor(props) {
    super(props)
    this.renderField = this.renderField.bind(this)
    this.renderProbabilityField = this.renderProbabilityField.bind(this)
    this.renderActionNameField = this.renderActionNameField.bind(this)
    this.renderActionAmountField = this.renderActionAmountField.bind(this)
    this.renderActionScoreField = this.renderActionScoreField.bind(this)
    this.onSubmit = this.onSubmit.bind(this)
    this.state = {
      suggest_actions: [],
      linear_progress_value: 0,
    }
  }
  async onSubmit(values) {
    this.props.submitLoadingStart()
    let action_name_field = 'action_name'
    let action_amount_field = 'action_amount'
    let action_score_field = 'action_score'
    let input_action_name = values[action_name_field]
    let input_action_amount = parseInt(values[action_amount_field], 10)
    let input_action_score = parseInt(values[action_score_field], 10)
    let log_status = 'public'
    var log_action_id = ''
    var log_action_name = ''
    var newActionDocId = ''
    let action_log_suggest_index = client.initIndex('action_log_suggest')
    let action_log_suggest_results;

    await action_log_suggest_index
      .search({
        query: input_action_name
      })
      .then(function (responses) {
        action_log_suggest_results = responses.hits
      })
      .catch(function (error) {
        console.log(error)
      })

    let completely_match = []
    for (let counter = 0; counter < action_log_suggest_results.length; counter++) {
      if (action_log_suggest_results[counter].name === input_action_name)
        completely_match = action_log_suggest_results[counter];
    }

    var batch = firestore.batch();
    var exist_my_action_swtich = false
    let exist_action_id = false
    for (let counter = 0; counter < values.from_date; counter++) {
      if (Math.random() < (values.probability) / 100) {
        let rootActionsRef = firestore.collection('actions')
        let currentUserRef = firestore.collection('users').doc(this.props.current_user.uid)
        let myActionsRef = currentUserRef.collection('actions')
        if (Object.keys(completely_match).length > 0 || exist_action_id) {
          if (exist_action_id === false)
            exist_action_id = completely_match.objectID
          batch.update(firestore.collection('actions').doc(exist_action_id), {
            log_amount: firebase.firestore.FieldValue.increment(1),
            total_logs_score: firebase.firestore.FieldValue.increment(input_action_score),
            total_done_actions_amount: firebase.firestore.FieldValue.increment(input_action_amount)
          });

          let my_action;
          if (counter === 0) {
            await myActionsRef.doc(exist_action_id).get().then(function (doc) {
              my_action = doc.data()
            })
            if (my_action)
              exist_my_action_swtich = true
          }

          if (exist_my_action_swtich) {
            let currentUserRef = firestore.collection('users').doc(this.props.current_user.uid)
            let myActionsRef = firestore.collection('users').doc(this.props.current_user.uid).collection('actions')
            batch.update(myActionsRef.doc(exist_action_id), {
              log_amount: firebase.firestore.FieldValue.increment(1),
              total_logs_score: firebase.firestore.FieldValue.increment(input_action_score),
              total_done_actions_amount: firebase.firestore.FieldValue.increment(input_action_amount)
            })
          }
          else {
            let myActionsRef = firestore.collection('users').doc(this.props.current_user.uid).collection('actions')
            batch.set(myActionsRef.doc(exist_action_id), {
              id: exist_action_id,
              name: input_action_name,
              log_amount: 1,
              total_logs_score: input_action_score,
              total_done_actions_amount: input_action_amount
            })
          }
        } else {
          exist_my_action_swtich = true
          let newActionDocRef = firestore.collection('actions').doc()
          exist_action_id = newActionDocRef.id
          batch.set(newActionDocRef, {
            id: newActionDocRef.id,
            name: input_action_name,
            log_amount: 1,
            kaizen_amount: 0,
            total_done_actions_amount: input_action_amount,
            total_logs_score: input_action_score
          })
          let myActionsRef = firestore.collection('users').doc(this.props.current_user.uid).collection('actions')
          batch.set(myActionsRef.doc(newActionDocRef.id), {
            id: newActionDocRef.id,
            name: input_action_name,
            log_amount: 1,
            total_done_actions_amount: input_action_amount,
            total_logs_score: input_action_score
          })
        }

        let myActionLogsDocRef = firestore.collection('users').doc(this.props.current_user.uid).collection('action_logs').doc()
        if (Object.keys(completely_match).length > 0) {
          log_action_id = completely_match.objectID
          log_action_name = completely_match.name
        } else {
          log_action_id = newActionDocId
          log_action_name = input_action_name
        }
        let action_logs_data = {
          action_id: log_action_id,
          action_name: log_action_name,
          id: myActionLogsDocRef.id,
          score: input_action_score,
          done_action_amount: input_action_amount,
          body: '',
          image_url: '',
          author_id: this.props.current_user.uid,
          author_name: this.props.user_in_firestore.displayName,
          author_photo: this.props.user_in_firestore.photoURL,
          log_status: log_status,
          likers: [],
          like_count: 0,
          created_at: moment(new Date()).add((-1) * counter, 'days')._d.getTime(),
          timestamp: firebase.firestore.FieldValue.serverTimestamp()
        }
        batch.set(myActionLogsDocRef, action_logs_data)
        if (log_status === 'public') {
          batch.set(firestore.collection('action_logs').doc(myActionLogsDocRef.id), action_logs_data)
        }
        batch.update(currentUserRef, {
          my_logs_amount: firebase.firestore.FieldValue.increment(1),
          my_done_actions_total: firebase.firestore.FieldValue.increment(input_action_amount),
          my_logs_score_total: firebase.firestore.FieldValue.increment(input_action_score)
        })
      }
      this.setState({ linear_progress_value: counter / values.from_date * 100 })
    }
    await batch.commit().then(function () {
    });
    this.props.submitLoadingFinish()
    this.props.history.push('/')
  };


  renderField(field) {
    const { input } = field
    return (
      <>
      <div style={{ width: '24px', height: '24px' }}></div>
      <SelectWrap>
        <select {...input} name="blood" variant="outlined">
          <option key={0} value={0}>日数を選択</option>
          <option value={1}>１日前から</option>
          <option value={2}>２日前から</option>
          <option value={3}>３日前から</option>
          <option value={4}>４日前から</option>
          <option value={5}>５日前から</option>
          <option value={6}>６日前から</option>
          <option value={7}>１週間前から</option>
          <option value={14}>２週間前から</option>
          <option value={21}>３週間前から</option>
          <option value={30}>１ヶ月前から</option>
        </select>
      </SelectWrap>
      </>
    )
  }
  renderProbabilityField(field) {
    const { input } = field
    return (
      <>
        <div style={{ width: '24px', height: '24px' }}></div>
      <SelectWrap>
        <select {...input} name="blood" variant="outlined">
          <option key={0} value={0}>実行確率を選択</option>
          {[10, 20, 30, 40, 50, 60, 70, 80, 90, 100].map((probability) => <option key={probability} value={probability}>{probability}%</option>)}
        </select>
      </SelectWrap>
      </>
    )
  }

  renderActionNameField(field) {
    const { input } = field
    return (
      <>
        <AccessibilityNew
          style={{
            color: '#aaa',
          }}
        />
        <div style={{ display: 'flex', alignItems: 'center', margin: '10px' }}>
          <input
            style={{
              width: '260px',
              border: '1px solid #aaa',
              borderRadius: '4px',
              outline: 'none',
              padding: '8px',
              boxSizing: 'border-box',
              fontSize: '16px'
            }}
            placeholder="アクション名"
            list="browsers"
            name="action_name"
            {...input}
          />
        </div>
        <datalist id="browsers">
          {this.state.suggest_actions.map((suggest_action, index) =>
            <option key={index} value={suggest_action.name} />
          )}
        </datalist>
      </>
    )
  }

  renderActionAmountField(field) {
    const { input } = field
    const amount_array = [...Array(100).keys()].map(i => ++i)
    return (
      <>
        <HorizontalSplit style={{ color: 'gray' }} />
        <SelectWrap>
          <select {...input} variant="outlined">
            <option key={0} value={0}>単位を選択</option>
            {amount_array.map((amount) => <option key={amount} value={amount}>{amount}</option>)}
          </select>
        </SelectWrap>
      </>
    )
  }

  renderActionScoreField(field) {
    const { input } = field
    const score_array = [...Array(100).keys()].map(i => ++i)
    return (
      <>
        <TrendingUp style={{ color: 'gray' }} />
        <SelectWrap>
          <select {...input} variant="outlined">
            <option key={0} value={0}>スコアを選択</option>
            {score_array.map((score) => <option key={score} value={score}>{score}</option>)}
          </select>
        </SelectWrap>
      </>
    )
  }

  async componentDidMount() {
    let temp_suggest_actions = []
    if (this.props.current_user) {
      //初回レンダリングときに、ここが走らないのが問題
      await firestore.collection('users').doc(this.props.current_user.uid).collection('actions').get().then(function (querySnapshot) {
        querySnapshot.forEach(function (doc) {
          temp_suggest_actions.push(doc.data())
        });
      });
    }
    this.setState({ suggest_actions: temp_suggest_actions })
  }

  render() {
    const { handleSubmit } = this.props
    return (
      <>
        {
          this.state.linear_progress_value === 0
            ?
            null
            :
            <LinearProgress variant="determinate" value={this.state.linear_progress_value} />
        }

        {
          this.props.submit_loading
            ?
            <CircularProgress />
            :
            <>
              <div style={{ marginTop: '10px', marginBottom: '10px' }}>
                <Typography>過去１ヶ月分までまとめて記録できます</Typography>
              </div>
              <form onSubmit={handleSubmit(this.onSubmit)}>
                <DivFlexCenterWrapper>
                  <Field label="labelname" name="action_name" type="number" component={this.renderActionNameField} />
                </DivFlexCenterWrapper>
                <DivFlexCenterWrapper>
                  <Field label="action_amount" name="action_amount" type="number" component={this.renderActionAmountField} />
                </DivFlexCenterWrapper>
                <DivFlexCenterWrapper>
                  <Field label="action_amount" name="action_score" type="number" component={this.renderActionScoreField} />
                </DivFlexCenterWrapper>
                <DivFlexCenterWrapper>
                  <Field label="labelname" name="from_date" type="number" component={this.renderField} />
                </DivFlexCenterWrapper>
                <DivFlexCenterWrapper>
                  <Field label="labelname" name="probability" type="number" component={this.renderProbabilityField} />
                </DivFlexCenterWrapper>
                {/* <Button label="Submit" type="submit" variant="outlined">Submit</Button> */}
                <div style={{ paddingTop: '10px' }}>
                  <Button style={{ backgroundColor: '#00b7ce', color: 'white', border: 'none', fontWeight: 'bold' }} label="Submit" type="submit" variant="outlined">送信する</Button>
                </div>
              </form>
            </>
        }
      </>
    )
  }

}

const mapStateToProps = (state) => {
  return {
    current_user: state.firebase.current_user,
    submit_loading: state.firebase.submit_loading,
    user_in_firestore: state.firebase.user_in_firestore
  }
}
const mapDispatchToProps = ({ matchActionRefUpdate, myActionsRefUpdate, myActionsRefSet, newActionToRoot, myActionsRefNewSet, submitLoadingStart, submitLoadingFinish })

export default reduxForm({
  form: 'formname'
})(connect(mapStateToProps, mapDispatchToProps)(HabitForm))
