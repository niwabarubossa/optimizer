import React, { Component } from 'react';
import { firestore } from '../../plugins/firebase'
import firebase from 'firebase'
import { matchActionRefUpdate, myActionsRefUpdate, myActionsRefSet, newActionToRoot, myActionsRefNewSet, submitLoadingStart, submitLoadingFinish } from '../../actions'
import { connect } from 'react-redux'
import { Field, reduxForm } from 'redux-form'
import Button from '@material-ui/core/Button'
import AccessibilityNew from '@material-ui/icons/AccessibilityNew'
import HorizontalSplit from '@material-ui/icons/HorizontalSplit'
import TrendingUp from '@material-ui/icons/TrendingUp'
import { DivFlexCenterWrapper, Padding } from '../atomic/Layout'
import { SelectWrap } from '../atomic/Parts.js'
import Typography from '@material-ui/core/Typography';
import CircularProgress from '@material-ui/core/CircularProgress';

const algoliasearch = require('algoliasearch')
const client = algoliasearch(process.env.REACT_APP_ALGOLIA_ID, process.env.REACT_APP_ADMIN_API_KEY)

class ActionLogForm extends Component {

  constructor(props) {
    super(props)
    this.onSubmit = this.onSubmit.bind(this)
    this.renderActionNameField = this.renderActionNameField.bind(this)
    this.renderActionAmountField = this.renderActionAmountField.bind(this)
    this.renderActionScoreField = this.renderActionScoreField.bind(this)
    this.onSearchAlgolia = this.onSearchAlgolia.bind(this)
    this.state = {
      my_actions_in_firestore: [],
      suggest_actions: [],
      my_actions: [],
      more_submit: false,
      local_submit_loading: false
    }
  }

  async onSubmit(values) {
    //TODO 新規２つ
    // this.props.submitLoadingStart()
    this.setState({ local_submit_loading: true })
    for (let i = 0; i < (Object.keys(values).length / 3); i++) {
      let action_name_field = 'action_name_' + i
      let action_amount_field = 'action_amount_' + i
      let action_score_field = 'action_score_' + i
      let input_action_name = values[action_name_field]
      let input_action_amount = parseInt(values[action_amount_field], 10)
      let input_action_score = parseInt(values[action_score_field], 10)
      let log_status = 'public'
      var log_action_id = ''
      var log_action_name = ''
      var newActionDocId = ''
      let newActionDodumentId;
      let action_log_suggest_index = client.initIndex('action_log_suggest')
      let action_log_suggest_results;

      await action_log_suggest_index
        .search({
          query: input_action_name
        })
        .then(function (responses) {
          action_log_suggest_results = responses.hits
        })
        .catch(function (error) {
          console.log(error)
        })

      let completely_match = []
      for (let counter = 0; counter < action_log_suggest_results.length; counter++) {
        if (action_log_suggest_results[counter].name === input_action_name)
          completely_match = action_log_suggest_results[counter];
      }

      let rootActionsRef = firestore.collection('actions')
      let currentUserRef = firestore.collection('users').doc(this.props.current_user.uid)
      let myActionsRef = currentUserRef.collection('actions')
      if (Object.keys(completely_match).length > 0) {
        const argument = {
          completely_match: completely_match,
          input_action_score: input_action_score,
          input_action_amount: input_action_amount,
          uid: this.props.current_user.uid
        }
        await this.props.matchActionRefUpdate(argument)
        let my_action;
        await myActionsRef.doc(completely_match.objectID).get().then(function (doc) {
          my_action = doc.data()
        })
        if (my_action)
          await this.props.myActionsRefUpdate(argument)
        else
          await this.props.myActionsRefSet(argument)
      } else {
        let newActionDocumentRef = firestore.collection('actions').doc()
        newActionDodumentId = newActionDocumentRef.id
        const argument = {
          newActionDocRef: newActionDocumentRef,
          input_action_score: input_action_score,
          input_action_amount: input_action_amount,
          input_action_name: input_action_name,
          uid: this.props.current_user.uid
        }
        await this.props.newActionToRoot(argument)
        await this.props.myActionsRefNewSet(argument)
      }

      let myActionLogsDocRef = firestore.collection('users').doc(this.props.current_user.uid).collection('action_logs').doc()
      if (Object.keys(completely_match).length > 0) {
        log_action_id = completely_match.objectID
        log_action_name = completely_match.name
      } else {
        log_action_id = newActionDodumentId
        log_action_name = input_action_name
      }
      let action_logs_data = {
        action_id: log_action_id,
        action_name: log_action_name,
        id: myActionLogsDocRef.id,
        score: input_action_score,
        done_action_amount: input_action_amount,
        body: '',
        image_url: '',
        author_id: this.props.current_user.uid,
        author_name: this.props.user_in_firestore.displayName,
        author_photo: this.props.user_in_firestore.photoURL,
        log_status: log_status,
        likers: [],
        like_count: 0,
        created_at: new Date().getTime(),
        timestamp: firebase.firestore.FieldValue.serverTimestamp()
      }
      await myActionLogsDocRef.set(action_logs_data)
      if (log_status === 'public')
        await firestore.collection('action_logs').doc(myActionLogsDocRef.id).set(action_logs_data)
      await currentUserRef.update({
        my_logs_amount: firebase.firestore.FieldValue.increment(1),
        my_done_actions_total: firebase.firestore.FieldValue.increment(input_action_amount),
        my_logs_score_total: firebase.firestore.FieldValue.increment(input_action_score)
      })
      this.setState({ local_submit_loading: false })
      this.props.submitLoadingFinish()
      this.props.history.push('/')
    };
  }

  async componentDidMount() {
    let action_log_suggest_index = client.initIndex('action_log_suggest')
    let algolia_log_suggest_results;
    await action_log_suggest_index
      .search({
        query: ''
      })
      .then(function (responses) {
        algolia_log_suggest_results = responses.hits
      })
      .catch(function (error) {
        console.log(error)
      })
    let temp_suggest_actions = []

    if (this.props.current_user) {
      //初回レンダリングときに、ここが走らないのが問題
      await firestore.collection('users').doc(this.props.current_user.uid).collection('actions').orderBy("log_amount", "desc").get().then(function (querySnapshot) {
        querySnapshot.forEach(function (doc) {
          temp_suggest_actions.push(doc.data())
        });
      });
      this.setState({ my_actions_in_firestore: temp_suggest_actions })
    }

    let merge_suggests = this.state.my_actions_in_firestore.concat(algolia_log_suggest_results)
    var tempObject = {};
    for (var i = 0; i < merge_suggests.length; i++) {
      tempObject[merge_suggests[i]['name']] = merge_suggests[i];
    }
    var no_duplicate_suggest_arr = [];
    for (var key in tempObject) {
      no_duplicate_suggest_arr.push(tempObject[key]);
    }
    this.setState({ suggest_actions: no_duplicate_suggest_arr })
  }

  async componentDidUpdate(prevProps) {
    let temp_suggest_actions = []
    if (prevProps.current_user !== this.props.current_user) {
      //初回レンダリングときに、ここが走らないのが問題
      await firestore.collection('users').doc(this.props.current_user.uid).collection('actions').orderBy("log_amount", "desc").get().then(function (querySnapshot) {
        querySnapshot.forEach(function (doc) {
          temp_suggest_actions.push(doc.data())
        });
      });
      this.setState({ my_actions_in_firestore: temp_suggest_actions })

      let action_log_suggest_index = client.initIndex('action_log_suggest')
      let algolia_log_suggest_results;
      await action_log_suggest_index
        .search({
          query: ''
        })
        .then(function (responses) {
          algolia_log_suggest_results = responses.hits
        })
        .catch(function (error) {
          console.log(error)
        })
      let merge_suggests = temp_suggest_actions.concat(algolia_log_suggest_results)
      var tempObject = {};
      for (var i = 0; i < merge_suggests.length; i++) {
        tempObject[merge_suggests[i]['name']] = merge_suggests[i];
      }
      var no_duplicate_suggest_arr = [];
      for (var key in tempObject) {
        no_duplicate_suggest_arr.push(tempObject[key]);
      }
      this.setState({ suggest_actions: no_duplicate_suggest_arr })
    }
  }

  renderActionNameField(field) {
    const { input } = field
    return (
      <>
        <AccessibilityNew
          style={{
            color: '#aaa',
          }}
        />
        <div style={{ display: 'flex', alignItems: 'center', margin: '10px' }}>
          <input
            style={{
              width: '260px',
              border: '1px solid #aaa',
              borderRadius: '4px',
              outline: 'none',
              padding: '8px',
              boxSizing: 'border-box',
              fontSize: '16px'
            }}
            placeholder="アクション名"
            list="browsers"
            name="action_name"
            {...input}
          />
        </div>
        <datalist id="browsers">
          {this.state.suggest_actions.map((suggest_action, index) =>
            <option key={index} value={suggest_action.name} />
          )}
        </datalist>
      </>
    )
  }

  renderActionAmountField(field) {
    const { input } = field
    const amount_array = [...Array(100).keys()].map(i => ++i)
    return (
      <>
        <HorizontalSplit style={{ color: 'gray' }} />
        <SelectWrap>
          <select {...input} variant="outlined">
            <option key={0} value={0}>単位を選択</option>
            {amount_array.map((amount) => <option key={amount} value={amount}>{amount}</option>)}
          </select>
        </SelectWrap>
      </>
    )
  }
  renderActionScoreField(field) {
    const { input } = field
    const score_array = [...Array(100).keys()].map(i => ++i)
    return (
      <>
        <TrendingUp style={{ color: 'gray' }} />
        <SelectWrap>
          <select {...input} variant="outlined">
            <option key={0} value={0}>スコアを選択</option>
            {score_array.map((score) => <option key={score} value={score}>{score}</option>)}
          </select>
        </SelectWrap>
      </>
    )
  }

  async onSearchAlgolia(e) {
    let action_log_suggest_index = client.initIndex('action_log_suggest')
    let algolia_log_suggest_results = []
    await action_log_suggest_index
      .search({
        query: e.target.value
      })
      .then(function (responses) {
        algolia_log_suggest_results = responses.hits
      })
      .catch(function (error) {
        console.log(error)
      })

    let merge_suggests = algolia_log_suggest_results.concat(this.state.my_actions_in_firestore)
    var tempObject = {};
    for (var i = 0; i < merge_suggests.length; i++) {
      tempObject[merge_suggests[i]['name']] = merge_suggests[i];
    }
    var no_duplicate_suggest_arr = [];
    for (var key in tempObject) {
      no_duplicate_suggest_arr.push(tempObject[key]);
    }
    this.setState({ suggest_actions: no_duplicate_suggest_arr })
  }

  render() {

    const { handleSubmit } = this.props

    return (
      <>
        {
          this.state.local_submit_loading
            ?
            <CircularProgress />
            :
            <form onSubmit={handleSubmit(this.onSubmit)}>
              <div>
                <Typography>アクション1</Typography>
                <DivFlexCenterWrapper>
                  <Field onChange={this.onSearchAlgolia} label="action_name" name="action_name_0" type="text" component={this.renderActionNameField} />
                </DivFlexCenterWrapper>
                <DivFlexCenterWrapper>
                  <Field label="action_amount" name="action_amount_0" type="number" component={this.renderActionAmountField} />
                </DivFlexCenterWrapper>
                <DivFlexCenterWrapper>
                  <Field label="action_score" name="action_score_0" type="number" component={this.renderActionScoreField} />
                </DivFlexCenterWrapper>

                {
                  this.state.more_submit
                    ?
                    <>
                      <Padding top={20}>
                        <Typography>アクション2</Typography>
                        <DivFlexCenterWrapper>
                          <Field onChange={this.onSearchAlgolia} label="action_name" name="action_name_1" type="text" component={this.renderActionNameField} />
                        </DivFlexCenterWrapper>
                        <DivFlexCenterWrapper>
                          <Field label="action_amount" name="action_amount_1" type="number" component={this.renderActionAmountField} />
                        </DivFlexCenterWrapper>
                        <DivFlexCenterWrapper>
                          <Field label="action_score" name="action_score_1" type="number" component={this.renderActionScoreField} />
                        </DivFlexCenterWrapper>
                      </Padding>

                      <Padding top={20}>
                        <Typography>アクション3</Typography>
                        <DivFlexCenterWrapper>
                          <Field onChange={this.onSearchAlgolia} label="action_name" name="action_name_2" type="text" component={this.renderActionNameField} />
                        </DivFlexCenterWrapper>
                        <DivFlexCenterWrapper>
                          <Field label="action_amount" name="action_amount_2" type="number" component={this.renderActionAmountField} />
                        </DivFlexCenterWrapper>
                        <DivFlexCenterWrapper>
                          <Field label="action_score" name="action_score_2" type="number" component={this.renderActionScoreField} />
                        </DivFlexCenterWrapper>
                      </Padding>

                      <Padding top={20}>
                        <Typography>アクション4</Typography>
                        <DivFlexCenterWrapper>
                          <Field onChange={this.onSearchAlgolia} label="action_name" name="action_name_3" type="text" component={this.renderActionNameField} />
                        </DivFlexCenterWrapper>
                        <DivFlexCenterWrapper>
                          <Field label="action_amount" name="action_amount_3" type="number" component={this.renderActionAmountField} />
                        </DivFlexCenterWrapper>
                        <DivFlexCenterWrapper>
                          <Field label="action_score" name="action_score_3" type="number" component={this.renderActionScoreField} />
                        </DivFlexCenterWrapper>
                      </Padding>

                      <Padding top={20}>
                        <Typography>アクション5</Typography>
                        <DivFlexCenterWrapper>
                          <Field onChange={this.onSearchAlgolia} label="action_name" name="action_name_4" type="text" component={this.renderActionNameField} />
                        </DivFlexCenterWrapper>
                        <DivFlexCenterWrapper>
                          <Field label="action_amount" name="action_amount_4" type="number" component={this.renderActionAmountField} />
                        </DivFlexCenterWrapper>
                        <DivFlexCenterWrapper>
                          <Field label="action_score" name="action_score_4" type="number" component={this.renderActionScoreField} />
                        </DivFlexCenterWrapper>
                      </Padding>
                    </>
                    :
                    null
                }
                <DivFlexCenterWrapper>
                  <Padding top={20}>
                    <Button label="Submit" variant="outlined" onClick={() => this.setState({ more_submit: !(this.state.more_submit) })} >さらに記録する</Button>
                  </Padding>
                </DivFlexCenterWrapper>
                <DivFlexCenterWrapper>
                  <Padding top={20}>
                    <Button style={{ backgroundColor: '#00b7ce', color: 'white', border: 'none', fontWeight: 'bold' }} label="Submit" type="submit" variant="outlined">送信する</Button>
                  </Padding>
                </DivFlexCenterWrapper>

              </div>
            </form>
        }
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    current_user: state.firebase.current_user,
    user_in_firestore: state.firebase.user_in_firestore
  }
}
const mapDispatchToProps = ({ matchActionRefUpdate, myActionsRefUpdate, myActionsRefSet, newActionToRoot, myActionsRefNewSet, submitLoadingStart, submitLoadingFinish })

export default reduxForm({
  form: 'formname'
})(connect(mapStateToProps, mapDispatchToProps)(ActionLogForm))
