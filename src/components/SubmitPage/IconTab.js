import ImprovementForm from './ImprovementForm'
import ActionLogForm from './ActionLogForm'
import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import HorizontalSplit from '@material-ui/icons/HorizontalSplit'
import TrendingUp from '@material-ui/icons/TrendingUp'
import Today from '@material-ui/icons/Today'
import HabitForm from './HabitForm'

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <Typography
      component="div"
      role="tabpanel"
      hidden={value !== index}
      id={`scrollable-prevent-tabpanel-${index}`}
      aria-labelledby={`scrollable-prevent-tab-${index}`}
      {...other}
    >
      <Box p={3}>{children}</Box>
    </Typography>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `scrollable-prevent-tab-${index}`,
    'aria-controls': `scrollable-prevent-tabpanel-${index}`,
  };
}

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    width: '100%',
    backgroundColor: '#FCFCFC'
  },
}));

export default function ScrollableTabsButtonPrevent(props) {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);

  function handleChange(event, newValue) {
    setValue(newValue);
  }

  return (
    <div className={classes.root}>
      <AppBar position="static" >
        <Tabs
          style={{ backgroundColor: '#2196F3' }}
          value={value}
          onChange={handleChange}
          variant="fullWidth"
          scrollButtons="off"
          aria-label="scrollable prevent tabs example"
        >
          <Tab icon={<HorizontalSplit />} aria-label="phone" {...a11yProps(0)} label="積み上げ記録" />
          <Tab icon={<Today />} aria-label="phone" {...a11yProps(1)} label="最近の習慣" />
          <Tab icon={<TrendingUp />} aria-label="favorite" {...a11yProps(2)} label="改善を記録" />
        </Tabs>
      </AppBar>
      <TabPanel value={value} index={0}>
        <ActionLogForm history={props.history} />
      </TabPanel>
      <TabPanel value={value} index={1}>
        <HabitForm history={props.history} />
      </TabPanel>
      <TabPanel value={value} index={2}>
        <ImprovementForm />
      </TabPanel>
    </div>
  );
}