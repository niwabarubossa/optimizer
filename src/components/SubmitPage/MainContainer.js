import React, { Component } from 'react';
import { Grid } from "@material-ui/core"
import TrendContainer from '../TopPage/TrendContainer'
import IconTab from './IconTab'
import { Padding } from '../atomic/Layout'
import { connect } from 'react-redux'

class MainContainer extends Component {

  render() {
    return (
      <Grid container spacing={16}>
        <Grid item xs={12} md={8}>
          <Padding top={10} left={10} right={10} >
            <IconTab {...this.props} />
          </Padding>
        </Grid>
        <Grid item xs={12} md={4}>
          <TrendContainer />
        </Grid>
      </Grid>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    current_user: state.firebase.current_user
  }
}

export default connect(mapStateToProps, null)(MainContainer)