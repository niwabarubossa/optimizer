import React, { Component } from 'react';
import 'firebase/firestore';
import { Field, reduxForm } from 'redux-form'
import { connect } from 'react-redux'
import { submitTweet, submitImageBeforeSubmitTweet, set_current_user_and_in_firestore, submitLoadingStart, submitLoadingFinish } from '../../actions'
import css from '../../assets/submitPage/ImprovementForm.css'
import FieldFileInput from '../TopPage/FieldFileInput'
import { withRouter } from 'react-router';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
import CircularProgress from '@material-ui/core/CircularProgress';
import { Editor, EditorState, CompositeDecorator, Modifier } from 'draft-js';
import 'draft-js/dist/Draft.css'
import Button from '@material-ui/core/Button'
import FormControl from '@material-ui/core/FormControl'
import FormHelperText from '@material-ui/core/FormHelperText'
import Radio from '@material-ui/core/Radio'
import RadioGroup from '@material-ui/core/RadioGroup'
import FormLabel from '@material-ui/core/FormLabel'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import { SelectWrap } from '../atomic/Parts.js'
import { DivFlexCenterWrapper } from '../atomic/Layout.js'

const algoliasearch = require('algoliasearch')
const client = algoliasearch(process.env.REACT_APP_ALGOLIA_ID, process.env.REACT_APP_ADMIN_API_KEY)


function hashtagStrategy(contentBlock, callback, contentState) {
  const HASHTAG_REGEX = /\#(?!\,)[\S]+/g;
  findWithRegex(HASHTAG_REGEX, contentBlock, callback);
}
function findWithRegex(regex, contentBlock, callback) {
  const text = contentBlock.getText();
  let matchArr, start;
  while ((matchArr = regex.exec(text)) !== null) {
    start = matchArr.index;
    callback(start, start + matchArr[0].length);
  }
}
const HashtagSpan = props => {
  return (
    <span
      style={{ color: 'rgb(27, 149, 224)', fontWeight: 'bold' }}
      data-offset-key={props.offsetKey}
    >
      {props.children}
    </span>
  );
};

class ImprovementForm extends Component {

  constructor(props) {
    super(props)
    this.onSubmit = this.onSubmit.bind(this)
    this.onChange = this.onChange.bind(this)
    this.onSuggestClick = this.onSuggestClick.bind(this)
    const decorator = new CompositeDecorator([
      {
        strategy: hashtagStrategy,
        component: HashtagSpan,
      }
    ]);
    this.state = {
      dataSource: [],
      search_result_posts: [],
      temp_input: '',
      autocomplete_input: '',
      editorState: EditorState.createEmpty(decorator),
      showSuggest: false
    }
    this.setEditor = (editor) => {
      this.editor = editor;
    };
    this.focusEditor = () => {
      if (this.editor) {
        this.editor.focus();
      }
    };
  }

  onSuggestClick(e) {
    const hashtag_array = this.state.editorState.getCurrentContent().getPlainText().split(/\s+/).filter(n => n.includes('#'))
    const now_hashtag_content = hashtag_array[hashtag_array.length - 1]
    const clicked_content = e.target.innerText.split(/\s+/)[0]
    const max_length = now_hashtag_content.length > clicked_content.length ? now_hashtag_content.length : clicked_content.length
    for (var counter = 0; counter < max_length; counter++) {
      if (now_hashtag_content[counter] !== clicked_content[counter])
        break;
    }
    const diff_content = clicked_content.slice(counter)
    let contentState = this.state.editorState.getCurrentContent();
    let targetRange = this.state.editorState.getSelection();
    let newContentState = Modifier.replaceText(
      contentState,
      targetRange,
      diff_content
    );
    this.setState({
      editorState: EditorState.push(this.state.editorState, newContentState),
      showSuggest: false
    })
  }

  async onChange(editorState) {
    editorState.getCurrentContent().getPlainText().split(/\s+/)
    const hashtag_array = editorState.getCurrentContent().getPlainText().split(/\s+/).filter(n => n.includes('#'))
    let temp_results = []
    if (hashtag_array.length) {
      var search_param = hashtag_array[hashtag_array.length - 1].slice(1)
      if (search_param.length > 0) {
        const hashtag_search_index = client.initIndex('hashtag_search')
        await hashtag_search_index.search({ query: search_param }).then(function (responses) {
          temp_results = responses.hits
        })
      }
      this.setState({ showSuggest: true, search_result_posts: temp_results })
    }
    this.setState({ temp_input: editorState.getCurrentContent().getPlainText() })
    this.setState({ editorState })
  }

  componentDidMount() {
    document.getElementById('option_form').options[20].selected = true;
  }

  renderSample(field) {
    const { input } = field
    var score_input = []
    var counter = 0;
    var score = 0;
    for (counter = 0, score = -100; score < 101; counter++ , score += 5) {
      score_input[counter] = score;
    }
    var option_list = [];
    for (counter = 0; counter < score_input.length; counter++) {
      option_list.push(<option key={counter} value={score_input[counter]}>{score_input[counter]}</option>);
    }
    return (
      <React.Fragment>
        <DivFlexCenterWrapper>
        <div className={css.selectContainer}>
          <p><label>改善スコア</label></p>
          <SelectWrap>
            <select {...input} id="option_form">
              {option_list}
            </select>
          </SelectWrap>
          </div>
        </DivFlexCenterWrapper>
      </React.Fragment>
    )
  }

  async onSubmit(values) {
    // TODO:全てredux-formのレールに乗せる
    this.props.submitLoadingStart()
    const body = this.state.temp_input
    const score = document.getElementById('option_form').value
    const tweet_status = values.tweet_status
    var blob = new Blob([values.image], { type: "image/jpg" });
    const input_arguments = { body: body, score: score, blob: blob, tweet_status: tweet_status, current_user: this.props.current_user, user_in_firestore: this.props.user_in_firestore }
    if (values.image !== undefined && input_arguments.blob.size < 5000000) {
      input_arguments.file_name = values.image.name
      await this.props.submitImageBeforeSubmitTweet(input_arguments)
    } else {
      await this.props.submitTweet(input_arguments, [null])
    }

    await this.props.set_current_user_and_in_firestore(this.props.current_user)
    this.props.submitLoadingFinish()
    this.props.history.push('/')
  }

  render() {
    const { handleSubmit, type, input } = this.props
    const style = { marginTop: 15 }
    const renderRadio = ({
      input: { value, onChange },
      label,
      children,
      meta: { touched, error },
      onFieldChange,
      row = true,
      required = false,
      rootClass = '',
    }) => (
        <FormControl classes={{ root: rootClass }} required={required} component='fieldset' error={!!(touched && error)}>
          <FormLabel component='legend'>{label}</FormLabel>
          <RadioGroup
            defaultValue="public"
            row={row}
            value={value}
            onChange={(e) => {
              onChange(e.target.value)
              onFieldChange && onFieldChange(e.target.value)
            }}
          >
            {children}
          </RadioGroup>
          {touched && error && <FormHelperText>{error}</FormHelperText>}
        </FormControl>
      )
    return (
      <div style={{ textAlign: 'center' }}>
        {
          this.props.submit_loading
            ?
            <CircularProgress />
            :
            <form onSubmit={handleSubmit(this.onSubmit)} className={css.form}>
              <div><Field label="sampleScore" name="sampleScore" type="number" component={this.renderSample} /></div>
              <p>改善内容</p>
              <div className={css.editorContainer} onClick={this.focusEditor}>
                <Editor
                  ref={this.setEditor}
                  editorState={this.state.editorState}
                  onChange={this.onChange}
                />
              </div>
              <div style={{ marginTop: '10px', width: '100%', textAlign: 'center' }}>
                <Field name='tweet_status' label='公開範囲' component={renderRadio} required >
                  <FormControlLabel value='private' control={<Radio />} label='自分のみ' />
                  <FormControlLabel value='anonymous' control={<Radio />} label='匿名公開' />
                  <FormControlLabel checked value='public' control={<Radio />} label='公開' />
                </Field>
              </div>
              {
                this.state.showSuggest
                  ?
                  this.state.search_result_posts.map(search_result_post => {
                    return (
                      <React.Fragment>
                        <ListItem onClick={this.onSuggestClick} className={css.suggestItem} style={{ zIndex: "100", backgroundColor: '#d7d4cd' }}>
                          <ListItemText
                            primary={`#${search_result_post.name} (${search_result_post.amount})`}
                          />
                        </ListItem>
                        <Divider />
                      </React.Fragment>
                    )
                  })
                  :
                  null
              }

              <div><Field label="Body" name="image" type="file" component={FieldFileInput} /></div>
              {/* <RaisedButton onClick={handleSubmit(this.onSubmit)} label="Submit" type="submit" style={style} /> */}
              <Button variant="contained" onClick={handleSubmit(this.onSubmit)} label="Submit" type="submit" style={{ marginTop: '20px', marginBottom: '30px', backgroundColor: 'rgb(0, 188, 212)' }}><span style={{ fontWeight: 'bold', color: 'white' }}>送信する</span></Button>
            </form>
        }
      </div>
    )
  }
}

const validate = values => {
  const error = {}
  // if (!values.body) errors.body = "内容が空です"
  return error
}
const mapStateToProps = (state) => {
  return {
    current_user: state.firebase.current_user,
    user_in_firestore: state.firebase.user_in_firestore,
    submit_loading: state.firebase.submit_loading
  }
}

const mapDispatchToProps = ({ submitTweet, submitImageBeforeSubmitTweet, set_current_user_and_in_firestore, submitLoadingStart, submitLoadingFinish })

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(
  reduxForm({ validate, form: 'contentsContainerForm' })(ImprovementForm)
))  