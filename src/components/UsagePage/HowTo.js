import React, { Component } from 'react';
import { Grid } from "@material-ui/core"
import Typography from '@material-ui/core/Typography';

class ClassName extends Component {
  render() {
    const sample_style = {
      width: '100%',
      height: '300px',
      backgroundColor: 'white',
      padding: '10px'
    }
    const sample_background = {
      width: '100%',
      height: `${300 - (10 * 2) - 47}px`,
      textAlign: 'left'
    }
    return (
      <Grid item xs={12} md={5}>
        <div style={sample_style}>
          <Grid container alignItems="center" justify="center" spacing={1} >
            <Grid item xs={12} style={{ textAlign: 'left' }}><Typography variant="h5" style={{ color: "#00b6ce" }}>{this.props.title}</Typography></Grid>
            <Grid item xs={5}>
              <div style={sample_background}><img style={{ width: '100%', height: 'auto' }} src={this.props.img_url} /></div>
            </Grid>
            <Grid item xs={7}>
              <div style={sample_background}><Typography>{this.props.LOREM_IPSUM}</Typography></div>
            </Grid>
          </Grid>
        </div>
      </Grid>
    )
  }
}

export default ClassName;
