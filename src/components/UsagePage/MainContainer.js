import React, { Component } from 'react';
import css from '../../assets/usagePage/MainContainer.css'
import { Grid } from "@material-ui/core"
import Typography from '@material-ui/core/Typography';
import HowTo from './HowTo'
import HOW_TO_IMAGE_1 from '../../assets/images/howto_image_1.png'
import HOW_TO_IMAGE_2 from '../../assets/images/howto_image_2.png'
import HOW_TO_IMAGE_3 from '../../assets/images/howto_image_3.png'
import HOW_TO_IMAGE_4 from '../../assets/images/howto_image_4.jpg'
import HOW_TO_IMAGE_5 from '../../assets/images/howto_image_5.jpg'

class MainContainer extends Component {
  render() {
    const sample_style = {
      width: '100%',
      height: '300px',
      backgroundColor: 'white',
      padding: '10px'
    }
    const sample_background = {
      width: '100%',
      height: `${300 - (10 * 2) - 47}px`,
      textAlign: 'left'
    }

    return (
      <div style={{ marginTop: '20px' }}>
        <Grid container alignItems="center" justify="center" spacing={4} >
          <Grid item xs={12}><Typography variant="h5">Optimizerとは？</Typography></Grid>
          <Grid item xs={12} md={5}>
            <div style={sample_style}>
              <Grid container alignItems="center" justify="center" spacing={1} >
                <Grid item xs={12} style={{ textAlign: 'left' }}><Typography variant="h5" style={{ color: "#00b6ce" }}>科学的根拠</Typography></Grid>
                <Grid item xs={5}>
                  <div style={sample_background}><img style={{ width: '100%', height: 'auto' }} src={HOW_TO_IMAGE_4} /></div>
                </Grid>
                <Grid item xs={7}>
                  <div style={sample_background}><Typography>このアプリは、<a style={{ fontWeight: 'bold' }} href="https://daigoblog.jp/growth-mindset/">「しなやかマインドセット」</a> <a style={{ fontWeight: 'bold' }} href="https://daigoblog.jp/smallwin/">「進捗の法則」</a>という２つの科学的根拠を意識して作成しました。 どちらも楽しく努力を続けていくために必要な重要なものだと解釈しています。もし興味を持たれた方がいましたら、この２つを参照していただけると嬉しいです。</Typography></div>
                </Grid>
              </Grid>
            </div>
          </Grid>
          <Grid item xs={12} md={5}>
            <div style={sample_style}>
              <Grid container alignItems="center" justify="center" spacing={1} >
                <Grid item xs={12} style={{ textAlign: 'left' }}><Typography variant="h5" style={{ color: "#00b6ce" }}>しなやかマインドセット？</Typography></Grid>
                <Grid item xs={5}>
                  <div style={sample_background}><img style={{ width: '100%', height: 'auto' }} src={HOW_TO_IMAGE_5} /></div>
                </Grid>
                <Grid item xs={7}>
                  <div style={sample_background}><Typography>才能や地頭は正しく努力をすればある程度身につけられるという、科学論文で示された考え方のことです。努力しているのにうまく行かない場合、やり方が間違っているのかもしれません。そんな時に変えるべきはやり方です。比べるべきは他人ではなく昨日の自分であり、成果だけにこだわらず過程も楽しむ..そんな形で役立てたら嬉しいです。</Typography></div>
                </Grid>
              </Grid>
            </div>
          </Grid>
          <HowTo LOREM_IPSUM="あなたの日々の発見や改善、ライフハックや改善を投稿しましょう。 そのライフハック・改善に自分なりのスコアをつけて、改善を積み重ねていきましょう。" title="改善内容を記録" img_url={HOW_TO_IMAGE_2} />
          <HowTo LOREM_IPSUM="モチベーションを維持するために必要な事は、「ウソでもいいから前に進んでいる感覚を持つこと（進捗の法則)」です。このグラフはその手助けとして使っていただけたら嬉しいです。" title="進捗の法則とは？" img_url={HOW_TO_IMAGE_1} />
          <HowTo LOREM_IPSUM="どんな改善をしようか迷ったときには、他の方の投稿を参考にしてみてはいかがでしょうか。それを探すためにワード検索、ハッシュタグ検索がお使いいただければ嬉しいです。" title="改善を探す" img_url={HOW_TO_IMAGE_3} />
        </Grid>
      </div>
    )
  }
}

export default MainContainer;
