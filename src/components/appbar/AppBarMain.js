import React, { Component } from 'react';
import { compose } from 'redux'
import { connect } from 'react-redux'
import { handleDrawerToggle, getUserInformation, getPosts, getWeeklyRankingPosts, loginLoadingStart, loginLoadingFinish } from '../../actions'
import { Link } from 'react-router-dom'
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar';
import CssBaseline from '@material-ui/core/CssBaseline';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import firebase from 'firebase'
import { firestore } from '../../plugins/firebase'

const drawerWidth = 240;
const styles = theme => ({
  appBar: {
    marginLeft: drawerWidth,
    [theme.breakpoints.up('sm')]: {
      width: `calc(100% - ${drawerWidth}px)`,
    },
    backgroundColor: '#2196F3'
  },
  menuButton: {
    marginRight: 20,
    [theme.breakpoints.up('sm')]: {
      display: 'none',
    },
  }
});

class AppBarMain extends Component {

  async componentDidMount() {
    this.props.loginLoadingStart()
    await this.props.getUserInformation()
  }

  async componentDidUpdate() {
    if (!this.props.current_user) {
    };
  }

  render() {
    const { classes, theme } = this.props;
    return (
      <React.Fragment>
        <CssBaseline />
        <AppBar position="fixed" className={classes.appBar}>
          <Toolbar>
            <IconButton
              color="inherit"
              aria-label="Open drawer"
              onClick={this.props.handleDrawerToggle}
              className={classes.menuButton}
            >
              <MenuIcon />
            </IconButton>
            <Typography variant="h6" color="inherit" noWrap>
              <Link to={'/'} style={{ textDecoration: 'none', color: 'white' }}>Optimizer</Link>
            </Typography>
          </Toolbar>
        </AppBar>
      </React.Fragment>
    )
  }
}

AppBarMain.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
};

const mapDispatchToProps = ({
  handleDrawerToggle,
  getUserInformation,
  getPosts,
  getWeeklyRankingPosts,
  loginLoadingStart,
  loginLoadingFinish
})
const mapStateToProps = (state) => {
  return {
    current_user: state.firebase.current_user,
    tweets: state.firebase.tweets,
    weekly_ranking_tweets: state.firebase.weekly_ranking_tweets
  }
}

export default compose(
  withStyles(styles, { withTheme: true }),
  connect(
    mapStateToProps,
    mapDispatchToProps
  ))(AppBarMain)