import React, { Component } from 'react';
import { compose } from 'redux'
import { Link } from 'react-router-dom'
import { withStyles } from '@material-ui/core/styles';
import Divider from '@material-ui/core/Divider';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import FiberNew from '@material-ui/icons/FiberNew'
import { getUserInformation, firebaseLogout, handleDrawerToggleReset } from '../../actions'
import { connect } from 'react-redux'
import SendIcon from '@material-ui/icons/Send';
import Tv from '@material-ui/icons/Tv'
import FavoriteBorder from '@material-ui/icons/FavoriteBorder'
import Search from '@material-ui/icons/Search'
import DirectionsRun from '@material-ui/icons/DirectionsRun'
import Forward from '@material-ui/icons/Forward'
import PersonOutline from '@material-ui/icons/PersonOutline'
import Help from '@material-ui/icons/Help'
import Edit from '@material-ui/icons/Edit'
import CircularProgress from '@material-ui/core/CircularProgress';

const styles = theme => ({
  toolbar: theme.mixins.toolbar,
});
class AppBarDrawer extends Component {

  logoutClicked() {
    this.props.firebaseLogout()
  }

  render() {
    const { classes, theme } = this.props;
    return (
      <div>
        <div className={classes.toolbar} />
        <Divider />
        {
          this.props.login_loading
            ?
            <CircularProgress />
            :
            <List>
              <Link to={'/'} style={{ textDecoration: 'none', color: '#555555' }} onClick={() => this.props.handleDrawerToggleReset()}>
                <ListItem button key={'aaa'}>
                  <ListItemIcon> <Tv /> </ListItemIcon>
                  <ListItemText primary={'トップページ'} />
                </ListItem>
              </Link>
              <Link to={'/latest'} style={{ textDecoration: 'none', color: '#555555' }} onClick={() => this.props.handleDrawerToggleReset()}>
                <ListItem button key={'aaa'}>
                  <ListItemIcon> <FiberNew /> </ListItemIcon>
                  <ListItemText primary={'改善一覧'} />
                </ListItem>
              </Link>
              <Link to={'/search'} style={{ textDecoration: 'none', color: '#555555' }} onClick={() => this.props.handleDrawerToggleReset()}>
                <ListItem button key={'aaa'}>
                  <ListItemIcon> <Search /> </ListItemIcon>
                  <ListItemText primary={'改善を探す'} />
                </ListItem>
              </Link>

              {
                this.props.current_user ?
                  <React.Fragment>
                    <Link to={'/management'} style={{ textDecoration: 'none', color: '#555555' }} onClick={() => this.props.handleDrawerToggleReset()} >
                      <ListItem button key={'aaa'}>
                        <ListItemIcon> <PersonOutline /> </ListItemIcon>
                        <ListItemText primary={'マイページ'} />
                      </ListItem>
                    </Link>
                    <Link to={'/submit'} style={{ textDecoration: 'none', color: '#555555' }} onClick={() => this.props.handleDrawerToggleReset()} >
                      <ListItem button key={'submitButton'}>
                        <ListItemIcon> <SendIcon /> </ListItemIcon>
                        <ListItemText primary={'記録する'} />
                      </ListItem>
                    </Link>
                    <Link to={'/favorite'} style={{ textDecoration: 'none', color: '#555555' }} onClick={() => this.props.handleDrawerToggleReset()} >
                      <ListItem button key={'submitButton'}>
                        <ListItemIcon> <FavoriteBorder /> </ListItemIcon>
                        <ListItemText primary={'お気に入り'} />
                      </ListItem>
                    </Link>

                    <Divider />

                    {
                      this.props.anonymous_user
                        ?
                        <Link to={'/permanent_login'} style={{ textDecoration: 'none', color: '#555555' }} onClick={() => this.props.handleDrawerToggleReset()} >
                          <ListItem button key={'permanent_login_button'}>
                            <ListItemIcon> <DirectionsRun /> </ListItemIcon>
                            <ListItemText primary={'ログインを永続化'} />
                          </ListItem>
                        </Link>
                        :
                        <Link to={'/logout'} style={{ textDecoration: 'none', color: '#555555' }} onClick={() => this.props.handleDrawerToggleReset()} >
                          <ListItem button key={'logout_button'} onClick={this.logoutClicked.bind(this)}>
                            <ListItemIcon> <DirectionsRun /> </ListItemIcon>
                            <ListItemText primary={'ログアウト'} />
                          </ListItem>
                        </Link>
                    }
                  </React.Fragment>
                  :
                  <>
                    <Link to={'/login'} style={{ textDecoration: 'none', color: '#555555' }} onClick={() => this.props.handleDrawerToggleReset()} >
                      <ListItem button key={'aaa'}>
                        <ListItemIcon> <Forward /> </ListItemIcon>
                        <ListItemText primary={'ログイン'} />
                      </ListItem>
                    </Link>
                    <Link to={'/logout'} style={{ textDecoration: 'none', color: '#555555' }} onClick={() => this.props.handleDrawerToggleReset()} >
                      <ListItem button key={'logout_button'} onClick={this.logoutClicked.bind(this)}>
                        <ListItemIcon> <DirectionsRun /> </ListItemIcon>
                        <ListItemText primary={'ログアウト'} />
                      </ListItem>
                    </Link>
                  </>
              }
              <Link to={'/usage'} style={{ textDecoration: 'none', color: '#555555' }} onClick={() => this.props.handleDrawerToggleReset()} >
                <ListItem button key={'aaa'}>
                  <ListItemIcon> <Help /> </ListItemIcon>
                  <ListItemText primary={'使い方'} />
                </ListItem>
              </Link>
            </List>
        }

      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    current_user: state.firebase.current_user,
    login_loading: state.firebase.login_loading,
    anonymous_user: state.firebase.anonymous_user
  }
}

const mapDispatchToProps = ({ getUserInformation, firebaseLogout, handleDrawerToggleReset })

export default compose(
  withStyles(styles, { withTheme: true }),
  connect(
    mapStateToProps,
    mapDispatchToProps
  ))(AppBarDrawer)