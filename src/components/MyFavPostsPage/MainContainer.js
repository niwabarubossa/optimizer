import React, { Component } from 'react';
import { getMyFavPosts, getUserInformation } from '../../actions'
import { connect } from 'react-redux'
import MyFavTweet from './MyFavTweet'
import CircularProgress from '@material-ui/core/CircularProgress';
import { Grid } from "@material-ui/core"
import TrendContainer from '../TopPage/TrendContainer'
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';

class MainContainer extends Component {

  async componentDidMount() {
    if (this.props.current_user && !this.props.my_fav_posts)
      await this.props.getMyFavPosts(this.props.current_user.uid)
  }
  async componentDidUpdate(prevProps) {
    if (prevProps.current_user !== (this.props.current_user) && !this.props.my_fav_posts) {
      await this.props.getMyFavPosts(this.props.current_user.uid)
    }
  }

  render() {
    return (
      <Grid container spacing={3}>
        <Grid item xs={12} md={8}>
          <ListItem alignItems="flex-start" style={{ backgroundColor: '#2196F3', marginTop: "8px", marginBottom: '10px' }} >
            <ListItemText>
              <span style={{ fontWeight: 'bold', color: 'white' }}>お気に入りの投稿</span>
            </ListItemText>
          </ListItem>
          {
            this.props.my_fav_posts ?
              this.props.my_fav_posts && this.props.my_fav_posts.map(project => {
                return (
                  <div key={project.tweet_id}>
                    <MyFavTweet tweet={project} />
                  </div>
                )
              })
              :
              <CircularProgress style={{ marginTop: '10px' }} />
          }
        </Grid>
        <Grid item xs={12} md={4}>
          <TrendContainer />
        </Grid>
      </Grid>

    )
  }
}

const mapStateToProps = (state) => {
  return {
    my_fav_posts: state.firebase.my_fav_posts,
    current_user: state.firebase.current_user
  }
}
const mapDispatchToProps = ({ getMyFavPosts, getUserInformation })

export default connect(mapStateToProps, mapDispatchToProps)(MainContainer)