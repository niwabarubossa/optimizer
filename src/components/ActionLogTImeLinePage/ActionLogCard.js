import React, { Component } from 'react';
import { actionLogGoodButtonLoadingStart, addUserToActionLogLiker, removeUserToActionLogLiker } from '../../actions'
import { connect } from 'react-redux'
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import AccessibilityNew from '@material-ui/icons/AccessibilityNew'
import HorizontalSplit from '@material-ui/icons/HorizontalSplit'
import TrendingUp from '@material-ui/icons/TrendingUp'
import NoUser from '../../assets/images/no_user.png'
import { Link } from 'react-router-dom'
import CardActions from '@material-ui/core/CardActions';
import FavIconContainer from './containers/FavIconContainer'
import moment from 'moment';

class ActionLogCard extends Component {


  async goodButtonClicked() {
    this.props.actionLogGoodButtonLoadingStart(this.props.action_log.id, 'action_logs')
    if (this.props.action_log.likers.includes(this.props.current_user.uid)) {
      await this.props.removeUserToActionLogLiker(this.props.current_user, this.props.action_log)
    } else {
      await this.props.addUserToActionLogLiker(this.props.current_user, this.props.action_log,this.props.user_in_firestore)
    }
  }

  render() {
    return (
      <Card>
        <CardActionArea>
          <Link to={`user/${this.props.action_log.author_id}`} >
            <ListItem>
              <ListItemAvatar>
                <img src={this.props.action_log.author_photo} style={{ width: '40px', height: '40px', borderRadius: '20px' }} onError={(e) => e.target.src = NoUser}></img>
              </ListItemAvatar>
              <ListItemText
                primary={this.props.action_log.author_name}
                secondary={moment(this.props.action_log.created_at).format('YYYY年MM月DD日 HH:mm')}
              />
            </ListItem>
          </Link>
          <CardContent>
            <Typography gutterBottom variant="h5" component="h3">
              <AccessibilityNew />
              {this.props.action_log.action_name}
            </Typography>
            <Typography variant="body2" color="textSecondary">
              <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
                <HorizontalSplit />{this.props.action_log.done_action_amount}単位
            </div>
              <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
                <TrendingUp />{this.props.action_log.score} スコア
            </div>
            </Typography>
          </CardContent>
        </CardActionArea>

        {
          this.props.current_user
            ?
            <CardActions disableActionSpacing style={{ display: 'flex', justifyContent: 'center' }}>
              <FavIconContainer
                local_like_state={this.props.action_log.likers.includes(this.props.current_user.uid)}
                local_loading={this.props.good_loading}
                like_count={this.props.action_log.like_count}
                goodButtonClicked={() => this.goodButtonClicked()} />
            </CardActions>
            :
            null
        }

      </Card>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    current_user: state.firebase.current_user,
    user_in_firestore: state.firebase.user_in_firestore
  }
}
const mapDispatchToProps = ({ actionLogGoodButtonLoadingStart, addUserToActionLogLiker, removeUserToActionLogLiker })

export default connect(mapStateToProps, mapDispatchToProps)(ActionLogCard)
