import React, { Component } from 'react';
import ActionLogsContainer from './containers/ActionLogsContainer'
import Grid from '@material-ui/core/Grid'
import TrendContainer from '../TopPage/TrendContainer'
import { Padding } from '../atomic/Layout'

class MainContainer extends Component {

  render() {
    return (
      <Padding left={5} right={5}>
        <Grid container spacing={3} >
          <Grid item xs={12} md={8}>
            <ActionLogsContainer />
          </Grid>
          <Grid item xs={12} md={4}>
            <TrendContainer />
          </Grid>
        </Grid>
      </Padding>
    )
  }
}


export default MainContainer;
