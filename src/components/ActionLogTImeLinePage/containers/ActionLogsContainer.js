import React, { Component } from 'react';
import { connect } from 'react-redux'
import { getActionLogRequest,getMoreActionLogsRequest,getMoreActionLogsStart,getMoreActionLogsFinish } from '../../../actions'
import Grid from '@material-ui/core/Grid'
import { OriginalHeader } from '../../atomic/Parts'
import ActionLogCard from '../ActionLogCard'
import Button from '@material-ui/core/Button'
import CircularProgress from '@material-ui/core/CircularProgress';
import { DivFlexCenterWrapper } from '../../atomic/Layout.js'

class ActionLogsContainer extends Component {

  constructor(props) {
    super(props);
    this.onGetMore = this.onGetMore.bind(this);
  }

  async onGetMore() {
    this.props.getMoreActionLogsStart()
    await this.props.getMoreActionLogsRequest(this.props.action_logs[this.props.action_logs.length - 1])
    this.props.getMoreActionLogsFinish()
  }

  async componentDidMount() {
    if (this.props.action_logs.length === 0)
      await this.props.getActionLogRequest()
  }

  render() {
    return (
      <>
        <OriginalHeader name="アクション一覧" />
        {
          this.props.action_logs.length !== 0
            ?
            this.props.action_logs.map(action_log => {
              return (
                <ActionLogCard action_log={action_log} good_loading={action_log.good_loading} />
              )
            })
            :
            <DivFlexCenterWrapper style={{ margin: '10px' }}>
              <CircularProgress />
            </DivFlexCenterWrapper>

        }
        {
          this.props.more_action_logs_loading
            ?
            <CircularProgress />
            :
            <Button style={{ marginTop: '10px' }} variant="outlined" onClick={this.onGetMore}>さらに読み込む</Button>
        }
      </>
    )

  }
}

const mapStateToProps = (state) => {
  return {
    action_logs: state.firebase.action_logs,
    more_action_logs_loading: state.firebase.more_action_logs_loading
  }
}
const mapDispatchToProps = ({ getActionLogRequest, getMoreActionLogsRequest, getMoreActionLogsStart, getMoreActionLogsFinish })

export default connect(mapStateToProps, mapDispatchToProps)(ActionLogsContainer)
