import React from 'react';
import styled from 'styled-components'
import Add from '@material-ui/icons/Add'
import Remove from '@material-ui/icons/Remove'
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';

export const DivFlexCenterWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center; 
`

export const PlusButtonWrap = styled.div`
  display: inline-block;
  width: 50px;
  height: 50px;
  border-radius: 50%;
  position: relative;
  box-shadow: 0 0 0 1px rgba(0,0,0,.1);
  cursor: pointer;
`

const MuiIconPlusButtonWrap = styled.div`
height: 24px;
width: 24px;
background-color: orange;
`
export const MuiPlusButton = () => {
  return (
    < MuiIconPlusButtonWrap >
      <Add />
    </MuiIconPlusButtonWrap >
  )
}

const Header = styled.div`
    height: 40px;
    line-height:40px;
    background-color: white;
    font-size: 12px;
    font-weight:bold;
    border-radius: 3px;
    border-top:solid 2px skyblue;
`
export const OriginalHeader = (props) => {
  return (
    <ListItem alignItems="flex-start" style={{ backgroundColor: '#2196F3', marginTop: "8px", marginBottom: '10px' }} >
      <ListItemText>
        <span style={{ fontWeight: 'bold', color: 'white' }}>{props.name}</span>
      </ListItemText>
    </ListItem>
  )
}

export const SelectWrap = styled.div`
	overflow: hidden;
	width: 262px;
	margin: 10px;
	text-align: center;
select {
	width: 100%;
	padding-right: 1em;
	cursor: pointer;
	text-indent: 0.01px;
	text-overflow: ellipsis;
	border: none;
	outline: none;
	background: transparent;
	background-image: none;
	box-shadow: none;
	-webkit-appearance: none;
	appearance: none;
}
select::-ms-expand {
    display: none;
}
	position: relative;
	border: 1px solid #aaa;
	border-radius: 4px;
	background: #ffffff;
::before {
	position: absolute;
	top: 1.0em;
	right: 0.9em;
	width: 0;
	height: 0;
	padding: 0;
	content: '';
	border-left: 6px solid transparent;
	border-right: 6px solid transparent;
	border-top: 6px solid #666666;
	pointer-events: none;
}
:after {
	position: absolute;
	top: 0;
	right: 2.5em;
	bottom: 0;
	width: 1px;
	content: '';
	border-left: 1px solid #bbbbbb;
}
select {
	padding: 8px 38px 8px 8px;
	color: #666666;
}
`