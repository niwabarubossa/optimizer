import React, { Component } from 'react';
import classes from '../../assets/atomic/ColorBox.css'
import CircularProgress from '@material-ui/core/CircularProgress';

class ColorBox extends Component {

  render() {
    return (
      <React.Fragment>
        <div className={classes.colorBoxContainer} style={{ height: this.props.height }}>
          <div className={classes.colorBox} style={{ backgroundColor: this.props.backgroundColor }}>
            <div className={classes.colorBoxContent}>
              <p>{this.props.header}</p>
              {
                this.props.score >= 0
                  ?
                  <p className={classes.colorBoxScore}>{this.props.score}</p>
                  :
                  <CircularProgress />
              }
            </div>
          </div>
        </div>
      </React.Fragment>
    )
  }
}

export default ColorBox;

