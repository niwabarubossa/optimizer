import React, { Component } from 'react';
import { persistLoginWithTwitter, loginLoadingStart, loginWithTwitter, loginLoadingFinish } from '../../actions'
import { connect } from 'react-redux'
import Button from '@material-ui/core/Button'

class MainContainer extends Component {

  async loginWithTwitter() {
    this.props.loginLoadingStart()
    await this.props.loginWithTwitter()
    this.props.history.push('/')
    this.props.loginLoadingFinish()
  }

  render() {
    return (
      <div>
        <p>初めて登録される方はこちら</p>
        <Button variant="outlined" onClick={() => { this.props.persistLoginWithTwitter() }}>Twitterで新規登録</Button>
        <p>既にアカウントがある方はこちらからログイン</p>
        <Button variant="outlined" onClick={this.loginWithTwitter.bind(this)} >Twitterでログイン</Button>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
  }
}
const mapDispatchToProps = ({
  persistLoginWithTwitter,
  loginLoadingStart,
  loginWithTwitter,
  loginLoadingFinish
})

export default connect(mapStateToProps, mapDispatchToProps)(MainContainer)
