import React, { Component } from 'react';
import classes from '../../assets/managementPage/ChartContainer.css'
import BarChart from './BarChart'
import LineChart from './LineChart'

class ComposedChartContainer extends Component {

  render() {
    return (
      <div className={classes.chartContainer}>
        <div className={classes.barChartContainer}>
          <BarChart chart_action_amount_data={this.props.local_molded_data.chart_action_amount_data} labels={this.props.local_molded_data.chart_index_to_date_paiindex_to_date_pair} />
          <LineChart chart_score_data={this.props.local_molded_data.chart_score_data} labels={this.props.local_molded_data.chart_index_to_date_paiindex_to_date_pair} />
        </div>
      </div>
    )
  }
}

export default ComposedChartContainer;