import React, { Component } from 'react';
import ChartContainer from './ChartContainer'
import MyPost from '../MyPost'
import { getMyPosts, getDisplayUserActionLogRequest } from '../../../actions'
import { connect } from 'react-redux'
import classes from '../../../assets/mainPage/ContentCardContainer.css'
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ActoinLogCard from './ActionLogCard'

class MiddleContent extends Component {

  async componentDidMount() {
    if (this.props.current_user && !this.props.my_posts) {
      await this.props.getMyPosts(this.props.current_user.uid)
    }
    if (this.props.current_user && this.props.display_user_action_logs.length === 0) {
      await this.props.getDisplayUserActionLogRequest(this.props.current_user.uid)
    }
  }

  async componentDidUpdate(prevProps) {
    if (prevProps.current_user !== this.props.current_user && !this.props.my_posts) {
      await this.props.getMyPosts(this.props.current_user.uid)
    }
    if (prevProps.current_user !== this.props.current_user && this.props.display_user_action_logs.length === 0) {
      await this.props.getDisplayUserActionLogRequest(this.props.current_user.uid)
    }
  }

  render() {
    return (
      <div className={classes.middleContentContainer}>
        <ChartContainer height={450} />
        <div className={classes.ContentCardContainer}>
          <ListItem alignItems="flex-start" style={{ backgroundColor: '#2196F3', marginTop: "8px", marginBottom: '10px' }} >
            <ListItemText>
              <span style={{ fontWeight: 'bold', color: 'white' }}>あなたの投稿</span>
            </ListItemText>
          </ListItem>


          {this.props.my_posts && this.props.my_posts.map(project => {
            return (
              <div key={project.tweet_id}>
                <MyPost tweet={project} good_loading={project.good_loading} />
              </div>
            )
          })}

          {
            this.props.display_user_action_logs && this.props.display_user_action_logs.map(action_log => {
              return (
                <div key={action_log.id}>
                  <ActoinLogCard action_log={action_log} good_loading={action_log.good_loading} />
                </div>
              )
            })
          }

        </div>
      </div>
    )
  }
}

const mapDispatchToProps = ({ getMyPosts, getDisplayUserActionLogRequest })
const mapStateToProps = (state) => {
  return {
    state_posts: state.firebase.items,
    my_posts: state.firebase.my_posts,
    current_user: state.firebase.current_user,
    display_user_action_logs: state.firebase.display_user_action_logs
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(MiddleContent)
