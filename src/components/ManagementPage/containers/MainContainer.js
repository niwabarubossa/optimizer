import React, { Component } from 'react';
import classes from '../../../assets/managementPage/MainContainer.css'
import TopContent from './TopContent'
import MiddleContent from './MiddleContent'

class MainContainer extends Component {

  render() {
    return (
      <div className={classes.managementMainContainer}>
        <TopContent />
        <MiddleContent />
      </div>
    )
  }
}

export default MainContainer;
