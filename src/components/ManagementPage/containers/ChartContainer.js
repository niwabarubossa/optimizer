import React, { Component } from 'react';
import classes from '../../../assets/managementPage/ChartContainer.css'
import ComposedChartContainer from '../ComposedChartContainer'
import { getPosts, getWeeklyPosts, getUserInformation, set_current_user_and_in_firestore, getWeeklyActionLogs } from '../../../actions'
import { connect } from 'react-redux'
import CircularProgress from '@material-ui/core/CircularProgress';
import moment from 'moment';
import Typography from '@material-ui/core/Typography'
import ActionLogsChartContainer from '../ActionLogsChartContainer'
import ColorBox from '../ColorBox'
import Grid from '@material-ui/core/Grid'

function makeLocalMoldedData(weekly_posts, total_score_amount, total_action_amount) {
  let counter
  let line_chart_value_array = [0, 0, 0, 0, 0, 0, 0, 0]
  let bar_chart_value_array = [0, 0, 0, 0, 0, 0, 0, 0]
  let local_molded_data = {}
  let key_date_value_arrayindex = {}
  let new_today_moment = moment(new Date())
  let index_to_date_pair = []
  for (counter = 0; counter <= 7; counter++) {
    key_date_value_arrayindex[new_today_moment.clone().add(counter - 7, 'days').format('MM-DD')] = { index: counter, score_amount: 0, action_amount: 0 }
    index_to_date_pair[counter] = new_today_moment.clone().add(counter - 7, 'days').format('MM-DD')
  }
  //エラー発生を防ぐ珠江、一応todayのところは使わないが、対応できるようにさせとく
  for (counter = 0; counter <= weekly_posts.length - 1; counter++) {
    let weekly_post_date_is = moment(weekly_posts[counter].created_at).format('MM-DD')
    let hairu_index = key_date_value_arrayindex[weekly_post_date_is].index
    bar_chart_value_array[hairu_index]++
    line_chart_value_array[hairu_index] += weekly_posts[counter].score
  }

  bar_chart_value_array.shift()
  line_chart_value_array.shift()

  let object_length = Object.keys(key_date_value_arrayindex).length;
  let temp_total_bar_chart_value_minus = 0
  let temp_total_line_chart_value_minus = 0

  for (counter = bar_chart_value_array.length - 1; counter >= 0; counter--) {
    temp_total_bar_chart_value_minus -= bar_chart_value_array[counter]
    temp_total_line_chart_value_minus -= line_chart_value_array[counter]
    bar_chart_value_array[counter] = temp_total_bar_chart_value_minus
    line_chart_value_array[counter] = temp_total_line_chart_value_minus
  }

  var line_chart_data = [0, 0, 0, 0, 0, 0, 0, 0]
  var bar_chart_data = [0, 0, 0, 0, 0, 0, 0, 0]
  var chart_index_to_date_paiindex_to_date_pair = index_to_date_pair

  for (counter = 0; counter < object_length - 1; counter++) {
    line_chart_data[counter] = total_score_amount + line_chart_value_array[counter]
    bar_chart_data[counter] = total_action_amount + bar_chart_value_array[counter]
  }
  line_chart_data[object_length - 1] = total_score_amount
  bar_chart_data[object_length - 1] = total_action_amount

  local_molded_data.line_chart_data = line_chart_data
  local_molded_data.bar_chart_data = bar_chart_data
  local_molded_data.chart_index_to_date_paiindex_to_date_pair = chart_index_to_date_paiindex_to_date_pair
  return local_molded_data
}

function makeActionLogMoldedData(weekly_posts, total_score_amount, total_action_amount) {
  let counter
  // let line_chart_value_array = [0, 0, 0, 0, 0, 0, 0, 0]
  var line_chart_value_array = Array.apply(null, Array(32)).map(function () { return 0 });
  // let bar_chart_value_array = [0, 0, 0, 0, 0, 0, 0, 0]
  let bar_chart_value_array = Array.apply(null, Array(32)).map(function () { return 0 });
  let local_molded_data = {}
  let key_date_value_arrayindex = {}
  let new_today_moment = moment(new Date())
  let index_to_date_pair = []
  for (counter = 0; counter <= 31; counter++) {
    key_date_value_arrayindex[new_today_moment.clone().add(counter - 31, 'days').format('MM-DD')] = { index: counter, score_amount: 0, action_amount: 0 }
    index_to_date_pair[counter] = new_today_moment.clone().add(counter - 31, 'days').format('MM-DD')
  }
  //エラー発生を防ぐ珠江、一応todayのところは使わないが、対応できるようにさせとく
  for (counter = 0; counter <= weekly_posts.length - 1; counter++) {
    let weekly_post_date_is = moment(weekly_posts[counter].created_at).format('MM-DD')
    let hairu_index = key_date_value_arrayindex[weekly_post_date_is].index
    bar_chart_value_array[hairu_index] += weekly_posts[counter].done_action_amount
    line_chart_value_array[hairu_index] += weekly_posts[counter].score
  }

  bar_chart_value_array.shift()
  line_chart_value_array.shift()

  let object_length = Object.keys(key_date_value_arrayindex).length;
  let temp_total_bar_chart_value_minus = 0
  let temp_total_line_chart_value_minus = 0

  for (counter = bar_chart_value_array.length - 1; counter >= 0; counter--) {
    temp_total_bar_chart_value_minus -= bar_chart_value_array[counter]
    temp_total_line_chart_value_minus -= line_chart_value_array[counter]
    bar_chart_value_array[counter] = temp_total_bar_chart_value_minus
    line_chart_value_array[counter] = temp_total_line_chart_value_minus
  }

  // var line_chart_data = [0, 0, 0, 0, 0, 0, 0, 0]
  var line_chart_data = Array.apply(null, Array(32)).map(function () { return 0 });
  // var bar_chart_data = [0, 0, 0, 0, 0, 0, 0, 0]
  var bar_chart_data = Array.apply(null, Array(32)).map(function () { return 0 });
  var chart_index_to_date_paiindex_to_date_pair = index_to_date_pair

  for (counter = 0; counter < object_length - 1; counter++) {
    line_chart_data[counter] = total_score_amount + line_chart_value_array[counter]
    bar_chart_data[counter] = total_action_amount + bar_chart_value_array[counter]
  }

  line_chart_data[object_length - 1] = total_score_amount
  bar_chart_data[object_length - 1] = total_action_amount

  local_molded_data.line_chart_data = line_chart_data
  local_molded_data.bar_chart_data = bar_chart_data
  local_molded_data.chart_index_to_date_paiindex_to_date_pair = chart_index_to_date_paiindex_to_date_pair
  return local_molded_data
}

class ChartContainer extends Component {

  constructor(props) {
    super(props);
    this.state = {
      local_molded_data: null,
      action_logs_molded_data: null
    }
  }

  async componentDidMount() {

    if (this.props.current_user)
      await this.props.set_current_user_and_in_firestore(this.props.current_user)

    if (this.props.weekly_posts.length === 0 && this.props.current_user)
      await this.props.getWeeklyPosts(this.props.current_user.uid)

    if (this.props.my_weekly_action_logs.length === 0 && this.props.current_user)
      await this.props.getWeeklyActionLogs(this.props.current_user.uid)

    if (this.props.weekly_posts && this.props.user_in_firestore) {
      var local_molded_data = makeLocalMoldedData(this.props.weekly_posts, this.props.user_in_firestore.total_score_amount, this.props.user_in_firestore.total_action_amount)
      this.setState({ local_molded_data: local_molded_data })
    }

    if (this.props.my_weekly_action_logs && this.props.user_in_firestore) {
      var action_logs_molded_data = makeActionLogMoldedData(this.props.my_weekly_action_logs, this.props.user_in_firestore.my_logs_score_total, this.props.user_in_firestore.my_done_actions_total)
      this.setState({ action_logs_molded_data: action_logs_molded_data })
    }

  }

  async componentDidUpdate(prevProps) {
    if (prevProps.current_user !== this.props.current_user) {
      await this.props.getWeeklyPosts(this.props.current_user.uid)
      await this.props.getWeeklyActionLogs(this.props.current_user.uid)
    }

    if (this.props.current_user && this.props.user_in_firestore === null)
      await this.props.set_current_user_and_in_firestore(this.props.current_user)

    if (prevProps.weekly_posts !== this.props.weekly_posts || prevProps.user_in_firestore.my_logs_score_total !== this.props.user_in_firestore.my_logs_score_total) {

      if (this.props.weekly_posts) {
        var local_molded_data = makeLocalMoldedData(this.props.weekly_posts, this.props.user_in_firestore.total_score_amount, this.props.user_in_firestore.total_action_amount)
        this.setState({ local_molded_data: local_molded_data })
        var action_logs_graph_data = makeActionLogMoldedData(this.props.my_weekly_action_logs, this.props.user_in_firestore.my_logs_score_total, this.props.user_in_firestore.my_done_actions_total)
        this.setState({ action_logs_molded_data: action_logs_graph_data })
      }
    }
  }

  render() {
    return (
      this.props.current_user
        ?
        <div className={classes.chartContainer}>
          <div className={classes.barChartContainer}>
            {
              this.props.user_in_firestore
                ?
                <Grid container>
                  <Grid item xs={6}>
                    <ColorBox style={{ backgroundColor: '#1fa8d8', height: '100px' }} header='累積アクション数' score={this.props.user_in_firestore.my_done_actions_total} />
                  </Grid>
                  <Grid item xs={6}>
                    <ColorBox style={{ backgroundColor: '#63c2de', height: '100px' }} header='累積アクションスコア' score={this.props.user_in_firestore.my_logs_score_total} />
                  </Grid>
                </Grid>
                :
                <>
                  <ColorBox style={{ backgroundColor: '#1fa8d8', height: '100px' }} header='総改善数' score="" />
                  <ColorBox style={{ backgroundColor: '#63c2de', height: '100px' }} header='累積スコア' score='' />
                  <ColorBox style={{ backgroundColor: '#1fa8d8', height: '100px' }} header='contribution' score="" />
                </>

            }
            {
              this.state.action_logs_molded_data
                ?
                <ActionLogsChartContainer height={500} local_molded_data={this.state.action_logs_molded_data} />
                :
                <CircularProgress />
            }
            {
              this.props.user_in_firestore
                ?
                <>
                  <Grid container>
                    <Grid item xs={4}>
                      <ColorBox style={{ backgroundColor: '#1fa8d8', height: '100px' }} header='総改善数' score={this.props.user_in_firestore.total_action_amount} />
                    </Grid>
                    <Grid item xs={4}>
                      <ColorBox style={{ backgroundColor: '#63c2de', height: '100px' }} header='累積スコア' score={this.props.user_in_firestore.total_score_amount} />
                    </Grid>
                    <Grid item xs={4}>
                      <ColorBox style={{ backgroundColor: '#1fa8d8', height: '100px' }} header='contribution' score={this.props.user_in_firestore.contribution} />
                    </Grid>
                  </Grid>
                </>
                :
                <>
                  <ColorBox style={{ backgroundColor: '#1fa8d8', height: '100px' }} header='総改善数' score="" />
                  <ColorBox style={{ backgroundColor: '#63c2de', height: '100px' }} header='累積スコア' score='' />
                  <ColorBox style={{ backgroundColor: '#1fa8d8', height: '100px' }} header='contribution' score="" />
                </>

            }
            {this.state.local_molded_data
              ?
              <ComposedChartContainer height={500} local_molded_data={this.state.local_molded_data} />
              :
              <CircularProgress />
            }
          </div>
        </div>
        :
        <div style={{ marginTop: '20px' }}>
          <Typography>登録すると記録が表示されます</Typography>
        </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    weekly_posts: state.firebase.weekly_posts,
    current_user: state.firebase.current_user,
    user_in_firestore: state.firebase.user_in_firestore,
    my_weekly_action_logs: state.firebase.my_weekly_action_logs
  }
}
const mapDispatchToProps = ({
  getPosts, getWeeklyPosts, getUserInformation, set_current_user_and_in_firestore, getWeeklyActionLogs
})
export default connect(mapStateToProps, mapDispatchToProps)(ChartContainer)