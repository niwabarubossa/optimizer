import React, { Component } from 'react';
import ColorBox from '../ColorBox'
import classes from '../../../assets/managementPage/TopContent.css'
import { Grid } from "@material-ui/core"
import { getUserChartInformation, getUserInformation, set_current_user_and_in_firestore } from '../../../actions'
import { connect } from 'react-redux'
import UserImage from '../../UserPage/UserImage'
import UserProfileContainer from '../../UserPage/container/UserProfileContainer'
import UserInformation from '../../UserPage/UserInformation'
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button'
import { Link } from 'react-router-dom'

class TopContent extends Component {

  render() {
    return (
      <div className={classes.topContentContainer}>
        {
          this.props.user_in_firestore
            ?
            <React.Fragment>
              <div style={{ marginBottom: '20px' }}>
                <UserImage photoURL={this.props.user_in_firestore.photoURL} />
                <h4>{this.props.user_in_firestore.displayName}</h4>
                <Typography>{this.props.user_in_firestore.profile}</Typography>
              </div>
              <div style={{ marginBottom: '20px' }}>
                <Button variant="contained">
                  <Link to={"/edit"}>
                    プロフィールを編集する
                </Link>
                </Button>
              </div>
            </React.Fragment>
            :
            null
        }
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    current_user: state.firebase.current_user,
    user_in_firestore: state.firebase.user_in_firestore
  }
}
const mapDispatchToProps = ({ getUserChartInformation, getUserInformation, set_current_user_and_in_firestore })
export default connect(mapStateToProps, mapDispatchToProps)(TopContent)