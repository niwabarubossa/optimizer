import React, { Component } from 'react';
import ColorBox from './ColorBox'
import Grid from "@material-ui/core/Grid"

class ClassName extends Component {
  render() {
    return (
      <Grid container alignItems="center" justify="center">
        <Grid item xs={4}>
          {
            this.props.user_in_firestore ?
              <ColorBox style={{ backgroundColor: '#1fa8d8', height: '100px' }} header='総改善数' score={this.props.user_in_firestore.total_action_amount} />
              :
              <ColorBox style={{ backgroundColor: '#1fa8d8', height: '100px' }} header='総改善数' score="" />
          }
        </Grid>
        <Grid item xs={4}>
          {
            this.props.user_in_firestore ?
              <ColorBox style={{ backgroundColor: '#63c2de', height: '100px' }} header='累積スコア' score={this.props.user_in_firestore.total_score_amount} />
              :
              <ColorBox style={{ backgroundColor: '#63c2de', height: '100px' }} header='累積スコア' score='' />
          }
        </Grid>
        <Grid item xs={4}>
          {
            this.props.user_in_firestore ?
              <ColorBox style={{ backgroundColor: '#1fa8d8', height: '100px' }} header='contribution' score={this.props.user_in_firestore.contribution} />
              :
              <ColorBox style={{ backgroundColor: '#1fa8d8', height: '100px' }} header='contribution' score="" />
          }
        </Grid>
      </Grid>
    )
  }
}

export default ClassName;
