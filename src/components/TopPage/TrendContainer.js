import React, { Component } from 'react';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
import { getPopularHashtags, getMorePopularHashtags } from '../../actions'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'

class TrendContainer extends Component {

  async componentDidMount() {
    if (this.props.popular_hashtags.length === 0)
      await this.props.getPopularHashtags()
  }

  render() {

    return (
      <List>
        <ListItem alignItems="flex-start" style={{ backgroundColor: '#2196F3' }} >
          <ListItemText>
            <span style={{ fontWeight: 'bold', color: 'white' }}>人気のタグ</span>
          </ListItemText>
        </ListItem>
        <Divider />
        {
          this.props.popular_hashtags
            ?
            this.props.popular_hashtags.map(popular_hashtag => {
              return (
                <Link to={`/search/${popular_hashtag.name}`}>
                  <ListItem alignItems="flex-start" >
                    <ListItemText>
                      <span style={{ fontWeight: 'bold' }}>#{popular_hashtag.name + '     '}</span>
                      <span>{popular_hashtag.amount}件の投稿</span>
                    </ListItemText>
                  </ListItem>
                  <Divider />
                </Link>
              )
            })
            :
            null
        }
        {
          this.props.popular_hashtags
            ?
            <ListItem alignItems="flex-start" onClick={() => { this.props.getMorePopularHashtags(this.props.popular_hashtags[this.props.popular_hashtags.length - 1]) }}>
              <ListItemText>
                <span style={{ fontWeight: 'bold' }}>さらに読み込む</span>
              </ListItemText>
              <Divider />
            </ListItem>
            :
            null
        }

      </List>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    popular_hashtags: state.firebase.popular_hashtags
  }
}

const mapDispatchToProps = ({
  getPopularHashtags,
  getMorePopularHashtags
})

export default connect(mapStateToProps, mapDispatchToProps)(TrendContainer)