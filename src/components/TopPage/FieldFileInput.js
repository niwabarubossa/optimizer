import React, { Component } from 'react'
import css from '../../assets/submitPage/FieldFileInput.css'
import uploadIcon from '../../assets/images/picture.svg'

export default class FieldFileInput extends Component {
  constructor(props) {
    super(props)
    this.onChange = this.onChange.bind(this)
    this.state = {
      file_name: ''
    }
  }

  onChange(e) {
    const { input: { onChange } } = this.props
    console.log(e.target.files[0])
    onChange(e.target.files[0])
    this.setState({ file_name: e.target.files[0].name })
  }

  render() {
    const { input: { value } } = this.props
    const { input, label } = this.props  //whatever props you send to the component from redux-form Field
    return (
      <div style={{ marginTop: '8px' }}>
        <label>
          <span class={css.filelabel} title="ファイルを選択">
            <img src={uploadIcon} width="26" height="26" alt="＋画像"></img>
            <span style={{ fontWeight: 'bold', color: 'white', marginLeft: '3px' }}>選択</span>
          </span>
          <input style={{ display: 'none' }} accept='.jpg, .png, .jpeg' onChange={this.onChange} type="file" name="datafile" id="filesend" />
        </label>
        <span>{this.state.file_name}</span>
      </div>
    )
  }
}