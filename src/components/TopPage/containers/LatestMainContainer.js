import React, { Component } from 'react';
import LatestTweet from '../LatestTweet'
import { connect } from 'react-redux'
import classes from '../../../assets/mainPage/ContentCardContainer.css'
import CircularProgress from '@material-ui/core/CircularProgress';
import { getPosts, getMorePostsRequest } from '../../../actions'
import { Grid } from '@material-ui/core'
import TrendContaienr from '../TrendContainer'
import Button from '@material-ui/core/Button'
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';

class LatestMainContainer extends Component {

  constructor(props) {
    super(props);
    this.getMorePosts = this.getMorePosts.bind(this)
  }

  async getMorePosts() {
    await this.props.getMorePostsRequest(this.props.tweets[this.props.tweets.length - 1])
  }

  async componentDidMount() {
    if (this.props.tweets.length === 0)
      await this.props.getPosts()
  }

  render() {
    return (
      <Grid container spacing={3} >
        <Grid item xs={12} md={7}>
          <div className={classes.ContentCardContainer}>
            <ListItem alignItems="flex-start" style={{ backgroundColor: '#2196F3', marginTop: "8px", marginBottom: '10px' }} >
              <ListItemText>
                <span style={{ fontWeight: 'bold', color: 'white' }}>最新の改善</span>
              </ListItemText>
            </ListItem>
            {this.props.tweets.length === 0 ?
              <CircularProgress />
              :
              this.props.tweets && this.props.tweets.map(tweet => {
                return (
                  <div key={tweet.tweet_id}>
                    <LatestTweet tweet={tweet} local_loading={tweet.good_loading} />
                  </div>
                )
              })
            }
          </div>
          {
            this.props.more_posts_loading
              ?
              <CircularProgress />
              :
              <Button style={{ marginTop: '15px' }} onClick={this.getMorePosts} variant="outlined" color="primary" >さらに読み込む</Button>
          }
        </Grid>
        <Grid item xs={12} md={4}>
          <TrendContaienr />
        </Grid>

      </Grid>
    )
  }
}

const mapDispatchToProps = ({
  getPosts,
  getMorePostsRequest
})

const mapStateToProps = (state) => {
  return {
    state_posts: state.firebase.items,
    tweets: state.firebase.tweets,
    get_posts_fetching: state.firebase.get_posts_fetching,
    more_posts_loading: state.firebase.more_posts_loading
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(LatestMainContainer)