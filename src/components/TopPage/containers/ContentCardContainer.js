import React, { Component } from 'react';
import Tweet from '../Tweet'
import { connect } from 'react-redux'
import classes from '../../../assets/mainPage/ContentCardContainer.css'
import CircularProgress from '@material-ui/core/CircularProgress';
import { searchWeeklyRankingTweets, set_current_user_and_in_firestore } from '../../../actions'
import moment from 'moment'

const algoliasearch = require('algoliasearch')
const client = algoliasearch(process.env.REACT_APP_ALGOLIA_ID, process.env.REACT_APP_ADMIN_API_KEY)

class ContentCardContainer extends Component {

  constructor(props) {
    super(props);
  }

  async componentDidMount() {
    if (this.props.current_user) {
      await this.props.set_current_user_and_in_firestore(this.props.current_user)
    }
    const body_search_index = client.initIndex('body_search')
    var weekly_ranking_search_results
    const today_moment = moment(new Date())
    const last_week_start = today_moment.add(-100, 'days').startOf('days')._d.getTime()
    await body_search_index
      .search({
        query: '',
        filters: `created_at > ${last_week_start}`
      })
      .then(function (responses) {
        weekly_ranking_search_results = responses.hits
      })
      .catch(function (error) {
        console.log(error)
      })

    for (var counter = 0; counter < weekly_ranking_search_results.length; counter++) {
      if (this.props.current_user && weekly_ranking_search_results[counter].likers.includes(this.props.current_user.uid))
        weekly_ranking_search_results[counter].i_like = true
      else
        weekly_ranking_search_results[counter].i_like = false
    }
    await this.props.searchWeeklyRankingTweets(weekly_ranking_search_results)
  }

  async componentDidUpdate(prevProps) {
    const body_search_index = client.initIndex('body_search')
    if (prevProps.current_user !== this.props.current_user) {
      let weekly_ranking_search_results
      const today_moment = moment(new Date())
      const last_week_start = today_moment.add(-100, 'days').startOf('days')._d.getTime()
      await body_search_index
        .search({
          query: '',
          filters: `created_at > ${last_week_start}`
        })
        .then(function (responses) {
          weekly_ranking_search_results = responses.hits
        })
        .catch(function (error) {
          console.log(error)
        })
      for (var counter = 0; counter < weekly_ranking_search_results.length; counter++) {
        if (this.props.current_user && weekly_ranking_search_results[counter].likers.includes(this.props.current_user.uid))
          weekly_ranking_search_results[counter].i_like = true
        else
          weekly_ranking_search_results[counter].i_like = false
      }
      await this.props.searchWeeklyRankingTweets(weekly_ranking_search_results)
    }
  }

  render() {
    return (
      <div className={classes.ContentCardContainer}>
        {
          this.props.login_loading
            ?
            <CircularProgress />
            :
            this.props.weekly_ranking_search_results
              ?
              this.props.weekly_ranking_search_results.map(search_result => {
                return (
                  <Tweet tweet={search_result} i_like={search_result.i_like} loading={search_result.good_loading} key={search_result.created_at} />
                )
              })
              :
              <CircularProgress />

        }
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    weekly_ranking_search_results: state.firebase.weekly_ranking_search_results,
    current_user: state.firebase.current_user,
    login_loading: state.firebase.login_loading
  }
}

const mapDispatchToProps = ({
  searchWeeklyRankingTweets,
  set_current_user_and_in_firestore
})

export default connect(mapStateToProps, mapDispatchToProps)(ContentCardContainer)