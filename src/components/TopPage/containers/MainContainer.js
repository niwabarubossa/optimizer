import React, { Component } from 'react';
import classes from '../../../assets/mainPage/MainContainer.css'
import ContentCardContainer from './ContentCardContainer'
import RightContainer from '../RightContainer'
import { Grid } from "@material-ui/core"
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';

class MainContainer extends Component {

  render() {
    return (
      <div className={classes.mainContainer}>
        <Grid container spacing={16} >
          <Grid item xs={12} md={7}>
            <ListItem alignItems="flex-start" style={{ backgroundColor: '#2196F3', marginTop: "8px", marginBottom: '10px' }} >
              <ListItemText>
                <span style={{ fontWeight: 'bold', color: 'white' }}>今週の人気投稿</span>
              </ListItemText>
            </ListItem>
            <div className={classes.contentCardContainer}>
              <ContentCardContainer />
            </div>
          </Grid>
          <Grid item xs={12} md={4}>
            <RightContainer />
          </Grid>
        </Grid>
      </div>
    )
  }
}

export default MainContainer;