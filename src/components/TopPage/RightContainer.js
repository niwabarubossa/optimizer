import React, { Component } from 'react';
import ContentsHeader from './ContentsHeader'
import classes from '../../assets/mainPage/RightContainer.css'
import TrendContainer from './TrendContainer'
import RankingContainer from './RankingContainer'
import ChartContainer from '../ManagementPage/containers/ChartContainer'
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';

class RightContainer extends Component {
  render() {
    return (
      <React.Fragment>
        <TrendContainer />
      </React.Fragment>
    )
  }
}

export default RightContainer;
