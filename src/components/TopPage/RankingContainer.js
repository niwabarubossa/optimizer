import React, { Component } from 'react';
import css from '../../assets/mainPage/RankingContainer.css'
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';


class RankingContainer extends Component {

  render() {

    return (
      <div className={css.rankingContainer}>
        <List>
          <ListItem alignItems="flex-start" >
            <ListItemText
              primary="Brunch this weekend?"
            />
          </ListItem>

          <Divider />
          <ListItem alignItems="flex-start" >
            <ListItemText
              primary="Brunch this weekend?"
            />
          </ListItem>

          <Divider />

        </List>
      </div>
    )
  }
}

export default RankingContainer