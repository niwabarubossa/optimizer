import React, { Component } from 'react';
import { updateUserInformationRequest } from '../../../actions'
import { connect } from 'react-redux'
import { Field, reduxForm } from 'redux-form'
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'
import Grid from '@material-ui/core/Grid'

class MainContainer extends Component {
  constructor(props) {
    super(props)
    this.renderTaskNameField = this.renderTaskNameField.bind(this)
    this.renderField = this.renderField.bind(this)
    this.onSubmit = this.onSubmit.bind(this)
  }
  async onSubmit(values) {
    const argument = { task_name: values.task_name, profile: values.profile }
    this.props.history.push('/')
    // await this.props.updateUserInformationRequest(this.props.current_user, argument)
  }

  renderTaskNameField(field) {
    const { input, meta: { error, touched } } = field
    return (
      <React.Fragment>
        <p>{(error && touched && <span style={{ color: 'red' }}>!{error}</span>)}</p>
        <TextField fullWidth={true} {...input} variant="outlined" placeholder="変更後のタスク名を入力してください.." />
      </React.Fragment>
    )
  }

  renderField(field) {
    const { input, meta: { error, touched } } = field
    return (
      <React.Fragment>
        <p>{(error && touched && <span style={{ color: 'red' }}>!{error}</span>)}</p>
        <TextField fullWidth={true} multiline={true} rows={3} rowsMax={6} {...input} variant="outlined" placeholder="テキストを入力してください..." />
      </React.Fragment>
    )
  }

  render() {
    const { handleSubmit } = this.props
    return (
      <Grid container alignItems="center" justify="center" spacing={16}>
        <form style={{ width: '100%', align: 'center', textAlign: 'center' }} onSubmit={handleSubmit(this.onSubmit)}>
          <Grid item xs={12} md={6}>
            <div style={{ margin: '20px 10px' }}><Field label="task_name" name="task_name" type="text" component={this.renderTaskNameField} /></div>
          </Grid>
          <Grid item xs={12}>
            <div style={{ margin: '10px' }}><Field label="text" name="text" type="text" component={this.renderField} /></div>
          </Grid>
          <Button color="primary" label="Submit" type="submit" variant="outlined">更新する</Button>
        </form>
      </Grid>
    )
  }

}

const validate = values => {
  const errors = {}
  if (!values.task_name) errors.task_name = "タスク名が入力されていません"
  if (!values.text) errors.text = "テキストが入力されていません"
  return errors
}

const mapStateToProps = (state) => {
  return {
    current_user: state.firebase.current_user
  }
}
const mapDispatchToProps = ({
  updateUserInformationRequest
})

export default reduxForm({
  validate,
  form: 'formname'
})(connect(mapStateToProps, mapDispatchToProps)(MainContainer))
