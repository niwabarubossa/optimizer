import React, { Component } from 'react';
import { updateUserInformationRequest, uploadUserImageAndUpdate } from '../../../actions'
import { connect } from 'react-redux'
import { Field, reduxForm } from 'redux-form'
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'
import Grid from '@material-ui/core/Grid'
import InputFileField from '../../TopPage/FieldFileInput'
import CircularProgress from '@material-ui/core/CircularProgress'

class MainContainer extends Component {
  constructor(props) {
    super(props)
    this.renderDisplayNameField = this.renderDisplayNameField.bind(this)
    this.renderProfileField = this.renderProfileField.bind(this)
    this.onSubmit = this.onSubmit.bind(this)
    this.state = {
      submit_loading: false
    }
  }
  async onSubmit(values) {
    if (values.image !== undefined) {
      var blob = new Blob([values.image], { type: "image/jpg" });
      const argument = { current_user: this.props.current_user, displayName: values.display_name, profile: values.profile, blob: blob,file_name: values.image.name }
      this.setState({ submit_loading: true })
      await this.props.uploadUserImageAndUpdate(argument)
    } else {
      const argument = { current_user: this.props.current_user, displayName: values.display_name, profile: values.profile }
      this.setState({ submit_loading: true })
      await this.props.updateUserInformationRequest(argument, [null])
    }
    this.props.history.push('/')
  }

  renderDisplayNameField(field) {
    const { input, meta: { error, touched } } = field
    return (
      <React.Fragment>
        <p>{(error && touched && <span style={{ color: 'red' }}>!{error}</span>)}</p>
        <TextField fullWidth={true} {...input} variant="outlined" placeholder="表示名を入力してください.." />
      </React.Fragment>
    )
  }

  renderProfileField(field) {
    const { input, meta: { error, touched } } = field
    return (
      <React.Fragment>
        <p>{(error && touched && <span style={{ color: 'red' }}>!{error}</span>)}</p>
        <TextField fullWidth={true} multiline={true} rows={3} rowsMax={6} {...input} variant="outlined" placeholder="プロフィールを入力してください..." />
      </React.Fragment>
    )
  }

  render() {
    const { handleSubmit } = this.props
    return (
      <Grid container alignItems="center" justify="center" spacing={16}>
        {
          this.state.submit_loading
            ?
            <CircularProgress />
            :
            <form style={{ width: '100%', align: 'center' }} onSubmit={handleSubmit(this.onSubmit)}>
              <p>表示名</p>
              <Grid item xs={12} md={7}>
                <div style={{ margin: '20px 10px' }}><Field label="display_name" name="display_name" type="text" component={this.renderDisplayNameField} /></div>
              </Grid>
              <p>プロフィール</p>
              <Grid item xs={12}>
                <div style={{ margin: '10px' }}><Field label="profile" name="profile" type="text" component={this.renderProfileField} /></div>
              </Grid>
              <p>プロフィール画像</p>
              <div>
                <Field label="Body" name="image" type="file" component={InputFileField} />
              </div>
              <div style={{ paddingTop: '20px' }}>
                <Button color="primary" label="Submit" type="submit" variant="outlined">更新する</Button>
              </div>
            </form>
        }
      </Grid>
    )
  }

}

const validate = values => {
  const errors = {}
  if (!values.display_name) errors.display_name = "表示名が入力されていません"
  if (!values.profile) errors.profile = "プロフィールが入力されていません"
  if (!values.image) errors.image = "画像が登録されていません"
  return errors
}

const mapStateToProps = (state) => {
  return {
    current_user: state.firebase.current_user
  }
}
const mapDispatchToProps = ({
  updateUserInformationRequest,
  uploadUserImageAndUpdate
})

export default reduxForm({
  validate,
  form: 'formname'
})(connect(mapStateToProps, mapDispatchToProps)(MainContainer))
