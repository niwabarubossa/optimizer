import React, { Component } from 'react'
import { goodButtonClicked, addUserToLiker, removeUserFromLiker, goodButtonLoadingStart, goodButtonLoadingStartInUserPage } from '../../actions'
import { connect } from 'react-redux'
import { compose } from 'redux'
import FavIconContainer from './container/FavIconContainer'
import css from '../../assets/userPage/UserTweet.css';
import { Link } from 'react-router-dom'
import Typography from '@material-ui/core/Typography';
import CardActions from '@material-ui/core/CardActions';
import { withStyles } from '@material-ui/core/styles';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import FaceIcon from '@material-ui/icons/Face';
import Divider from '@material-ui/core/Divider';
import Linkify from 'linkifyjs/react';
import moment from 'moment'

const styles = theme => ({
  root: {
    width: '100%',
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
  },
  inline: {
    display: 'inline',
  },
  icon: {
    width: '40px',
    height: '40px'
  },
  card: {
    height: 'auto'
  },
  actions: {
    display: 'flex',
    justifyContent: 'center'
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
});

class ContentCard extends Component {

  constructor(props) {
    super(props);
    this.state = {
      like_count: this.props.tweet.like_count,
      local_state_like: null,
      disabled: false,
    }
  }

  async goodButtonClicked() {
    this.props.goodButtonLoadingStartInUserPage(this.props.tweet.id)
    if (this.props.tweet.likers.includes(this.props.current_user.uid)) {
      await this.props.removeUserFromLiker(this.props.current_user, this.props.tweet.id)
    } else {
      await this.props.addUserToLiker(this.props.current_user, this.props.tweet.id)
    }
  }

  render() {
    const { classes } = this.props;
    if (this.props.tweet.author_name)
      var displayName = this.props.tweet.author_name;
    else
      var displayName = 'none';

    return (
      <div className={css.cardContainer} >
        <Link to={`user/${this.props.tweet.author_id}`} >
          <ListItem className={classes.card}>
            <ListItemAvatar>
              {
                this.props.tweet.author_photo ?
                  <img src={this.props.tweet.author_photo} className={css.author_photo} ></img>
                  :
                  <FaceIcon className={classes.icon} />
              }
            </ListItemAvatar>
            <ListItemText
              primary={displayName}
              secondary={
                <React.Fragment>
                  <Typography component="span" className={classes.inline} color="textPrimary">
                    {`${this.props.tweet.score} スコアアップ！`}
                  </Typography>
                  {moment(this.props.tweet.created_at).format('YYYY年MM月DD日 HH:mm')}
                </React.Fragment>
              }
            />
          </ListItem>
        </Link>
        <div className={css.contentContainer}>
          <Linkify>{this.props.tweet.body}</Linkify>
          <p>
            {this.props.tweet.hashtags.map(hashtag => {
              return (
                <Link to={`/search/${hashtag}`}><span style={{ color: 'rgb(27, 149, 224)', fontWeight: 'bold' }}>#{hashtag} </span></Link>
              )
            })
            }
          </p>
          {/* <img className={css.tweetImage} src={this.props.tweet.image_url} ></img> */}
        </div>

        {
          this.props.current_user ?
            <CardActions className={classes.actions} disableActionSpacing>
              <FavIconContainer
                local_loading={this.props.local_loading}
                local_like_state={this.props.tweet.likers.includes(this.props.current_user.uid)}
                like_count={this.props.tweet.like_count}
                goodButtonClicked={() => this.goodButtonClicked()} />
            </CardActions>
            :
            <CardActions className={classes.actions} disableActionSpacing>
              <FavIconContainer
                local_loading={this.props.local_loading}
                local_like_state={false}
                like_count={this.props.tweet.like_count}
                goodButtonClicked={() => { alert('ログインしてください') }} />
            </CardActions>
        }

        <Divider />
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    current_user: state.firebase.current_user
  }
}
const mapDispatchToProps = ({
  goodButtonClicked,
  addUserToLiker,
  removeUserFromLiker,
  goodButtonLoadingStart,
  goodButtonLoadingStartInUserPage
})

export default compose(
  withStyles(styles, { withTheme: true }),
  connect(
    mapStateToProps,
    mapDispatchToProps
  ))(ContentCard)