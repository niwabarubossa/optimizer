import React, { Component } from 'react';
import css from '../../assets/userPage/UserImage.css'
import NoUser from '../../assets/images/no_user.png'

class UserImage extends Component {

  render() {
    return (
      <div className={css.userImageContainer}>
        {
          this.props.photoURL === null
            ?
            <img className={css.testImage} src={NoUser} />
            :
            <img className={css.testImage} src={this.props.photoURL} onError={(e) => e.target.src = NoUser} />
        }
      </div>
    )
  }
}

export default UserImage;
