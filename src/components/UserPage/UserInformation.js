import React, { Component } from 'react';
import Grid from '@material-ui/core/Grid'
import ColorBox from '../atomic/ColorBox'
import Typography from '@material-ui/core/Typography';

class UserInformation extends Component {
  render() {
    return (
      <div>
        <h4>{this.props.user.displayName}</h4>
        <div style={{ marginBottom: '20px' }}>
          <Typography>{this.props.user.profile}</Typography>
        </div>
      </div>
    )
  }
}

export default UserInformation;
