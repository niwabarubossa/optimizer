import React, { Component } from 'react';
import { getDisplayUserInformation, resetDisplayUser } from '../../actions'
import { connect } from 'react-redux'
import UserProfileContainer from './container/UserProfileContainer'
import UserTweetsContainer from './container/UserTweetsContainer'
import { Grid } from "@material-ui/core"
import UserActionLogsContainer from './container/UserActionLogsContainer.js'
import DisplayUserChartContainer from './container/DisplayUserChartContainer.js'

class UserPage extends Component {

  async componentDidMount() {
    await this.props.getDisplayUserInformation(this.props.match.params.id)
  }

  componentWillUnmount() {
    this.props.resetDisplayUser()
  }

  render() {

    return (
      <div style={{ color: 'black', display: 'flex', justifyContent: 'center', flexDirection: 'column' }}>
        <UserProfileContainer props={this.props} />
        <Grid container spacing={16} >
          <Grid item xs={12}>
            <DisplayUserChartContainer />
            <UserTweetsContainer id={this.props.match.params.id} />
            <UserActionLogsContainer />
          </Grid>
        </Grid>
      </div>
    )
  }
}
const mapStateToProps = (state) => {
  return {
    display_user_uid: state.firebase.display_user_uid
  }
}

const mapDispatchToProps = ({ getDisplayUserInformation, resetDisplayUser })

export default connect(mapStateToProps, mapDispatchToProps)(UserPage)