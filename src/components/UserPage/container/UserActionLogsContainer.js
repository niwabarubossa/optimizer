import React, { Component } from 'react';
import { getMyPosts, getDisplayUserActionLogRequest } from '../../../actions'
import { connect } from 'react-redux'
import classes from '../../../assets/mainPage/ContentCardContainer.css'
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ActoinLogCard from './ActionLogCard'

class UserActionLogsContainer extends Component {

  async componentDidMount() {
    if (this.props.display_user && this.props.display_user_action_logs.length === 0) {
      await this.props.getDisplayUserActionLogRequest(this.props.display_user.uid)
    }
  }
  async componentDidUpdate(prevProps) {
    if (prevProps.display_user !== this.props.display_user && this.props.display_user_action_logs.length === 0)
      await this.props.getDisplayUserActionLogRequest(this.props.display_user.uid)
  }

  render() {
    return (
      <div className={classes.middleContentContainer}>
        {
          this.props.display_user_action_logs && this.props.display_user_action_logs.map(action_log => {
            return (
              <div key={action_log.id}>
                <ActoinLogCard action_log={action_log} good_loading={action_log.good_loading} />
              </div>
            )
          })
        }
      </div>
    )
  }
}

const mapDispatchToProps = ({ getMyPosts, getDisplayUserActionLogRequest })
const mapStateToProps = (state) => {
  return {
    current_user: state.firebase.current_user,
    display_user_action_logs: state.firebase.display_user_action_logs,
    display_user: state.firebase.display_user
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(UserActionLogsContainer)
