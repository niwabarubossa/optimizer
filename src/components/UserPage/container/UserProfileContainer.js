import React, { Component } from 'react';
import css from '../../../assets/userPage/container/UserProfileContainer.css'
import { getDisplayUserInformation } from '../../../actions'
import { connect } from 'react-redux'
import UserInformation from '../UserInformation'
import UserImage from '../UserImage'
import CircularProgress from '@material-ui/core/CircularProgress'

class UserProfileContainer extends Component {

  render() {

    return (
      <div className={css.userProfileContainer}>
        {
          this.props.display_user ?
            <React.Fragment>
              <UserImage photoURL={this.props.display_user.photoURL} />
              <UserInformation user={this.props.display_user} />
            </React.Fragment>
            :
            <CircularProgress />
        }
      </div>
    )

  }
}

const mapStateToProps = (state) => {
  return {
    display_user: state.firebase.display_user,
  }
}

const mapDispatchToProps = ({
  getDisplayUserInformation
})

export default connect(mapStateToProps, mapDispatchToProps)(UserProfileContainer)
