import React, { Component } from 'react';
import { getDisplayUserTweets, resetDisplayUserTweets } from '../../../actions'
import { connect } from 'react-redux'
import UserTweet from '../UserTweet'

class UserTweetsContainer extends Component {

  async componentDidMount() {
    await this.props.getDisplayUserTweets(this.props.id)
  }

  render() {
    return (
      <div>
        {this.props.display_user_tweets && this.props.display_user_tweets.map(tweet => {
          return (
            <div key={tweet.tweet_id}>
              <UserTweet tweet={tweet} local_loading={tweet.good_loading} />
            </div>
          )
        })}
      </div>
    )
  }
}

const mapDispatchToProps = ({ getDisplayUserTweets, resetDisplayUserTweets })
const mapStateToProps = (state) => {
  return {
    display_user_tweets: state.firebase.display_user_tweets
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(UserTweetsContainer)