import React, { Component } from 'react';
import Grid from '@material-ui/core/Grid';
import SearchTweet from './SearchTweet'
import { searchTweets } from '../../actions'
import { connect } from 'react-redux'
import TextField from '@material-ui/core/TextField'
import { Field, reduxForm } from 'redux-form'
import Button from '@material-ui/core/Button'
import css from '../../assets/searchPage/SearchPageContainer.css'
import TrendContainer from '../TopPage/TrendContainer'

const algoliasearch = require('algoliasearch')
const client = algoliasearch(process.env.REACT_APP_ALGOLIA_ID, process.env.REACT_APP_ADMIN_API_KEY)


class SearchPageContainer extends Component {

  constructor(props) {
    super(props);
    this.renderField = this.renderField.bind(this)
    this.searchAlgolia = this.searchAlgolia.bind(this)
    this.state = {
      search_parameter: '',
      local_search_result: ''
    }
  }

  async componentDidMount() {
    let temp_results = []
    const body_search_index = client.initIndex('body_search')
    // if (this.props.match.params.hashtag) {
    if (this.props.match.params.hashtag !== undefined)
      document.getElementById("search_field").value = this.props.match.params.hashtag
    else
      document.getElementById("search_field").value = ''
    await body_search_index
      .search({
        query: this.props.match.params.hashtag
      })
      .then(function (responses) {
        temp_results = responses.hits
      })
      .catch(function (error) {
        console.log(error)
      })

    for (var counter = 0; counter < temp_results.length; counter++) {
      if (this.props.current_user && temp_results[counter].likers.includes(this.props.current_user.uid))
        temp_results[counter].i_like = true
      else
        temp_results[counter].i_like = false
    }
    this.props.searchTweets(temp_results)
  }

  async componentDidUpdate(prevProps) {
    let temp_results = []
    if (prevProps.match.params.hashtag !== this.props.match.params.hashtag) {
      document.getElementById("search_field").value = this.props.match.params.hashtag
      const body_search_index = client.initIndex('body_search')
      await body_search_index
        .search({
          query: this.props.match.params.hashtag
        })
        .then(function (responses) {
          temp_results = responses.hits
        })
        .catch(function (error) {
          console.log(error)
        })
      for (var counter = 0; counter < temp_results.length; counter++) {
        if (this.props.current_user && temp_results[counter].likers.includes(this.props.current_user.uid))
          temp_results[counter].i_like = true
        else
          temp_results[counter].i_like = false
      }
      this.props.searchTweets(temp_results)
    }
  }

  renderField(field) {
    const { input } = field
    return (
      <TextField {...input} />
    )
  }

  async searchAlgolia(e) {
    let temp_results = []
    const body_search_index = client.initIndex('body_search')
    await body_search_index
      .search({
        query: document.getElementById("search_field").value
      })
      .then(function (responses) {
        temp_results = responses.hits
      })
      .catch(function (error) {
        console.log(error)
      })

    for (var counter = 0; counter < temp_results.length; counter++) {
      if (this.props.current_user && temp_results[counter].likers.includes(this.props.current_user.uid))
        temp_results[counter].i_like = true
      else
        temp_results[counter].i_like = false
    }
    this.setState({ local_search_result: temp_results })
    this.props.searchTweets(temp_results)
  }

  render() {

    return (
      <Grid container spacing={3} >
        <Grid item xs={12} md={7}>
          {/* <div><input id="search_field" onChange={this.searchAlgolia} label="search_field" name="search_field"/></div> */}
          <div>
            <TextField
              style={{ width: '100%' }}
              id="search_field"
              placeholder="検索してください..."
              type="text"
              name="search_field"
              margin="normal"
              variant="outlined"
            />
            <Button color="primary" variant="outlined" onClick={this.searchAlgolia}>検索する</Button>
          </div>
          {
            this.props.search_results.length === 0
              ?
              <p style={{ marginTop: '10px' }}>検索結果が見つかりませんでした</p>
              :
              this.props.search_results.map(search_result => {
                return (
                  <SearchTweet tweet={search_result} i_like={search_result.i_like} loading={search_result.good_loading} key={search_result.created_at} />
                )
              })
          }
        </Grid>

        <Grid item xs={12} md={5}>
          <TrendContainer />
        </Grid>
      </Grid>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    search_results: state.firebase.search_results,
    current_user: state.firebase.current_user
  }
}

const mapDispatchToProps = ({ searchTweets })

export default reduxForm({
  form: 'tweetSearchForm'
})(connect(mapStateToProps, mapDispatchToProps)(SearchPageContainer))