import React, { Component } from 'react';
import IconButton from '@material-ui/core/IconButton';
import FavoriteIcon from '@material-ui/icons/Favorite';
import css from '../../assets/mainPage/FavIconContainer.css'
import CircularProgress from '@material-ui/core/CircularProgress';

class FavIconContainer extends Component {
  render() {
    return (
      <div onClick={this.props.goodButtonClicked} className={css.favIconContainer}>
        {
          this.props.local_loading
            ?
            <CircularProgress />
            :
            this.props.i_like ?
              <React.Fragment>
                <IconButton aria-label="Add to favorites"
                  disabled={this.props.disabled}
                  color="secondary"
                  onClick={this.goodButtonClicked}>
                  <FavoriteIcon />
                </IconButton>
                <div className={css.facIconCounter}>{this.props.like_count}</div>
              </React.Fragment>
              :
              <React.Fragment>
                <IconButton aria-label="Add to favorites"
                  disabled={this.props.disabled}
                  color="default"
                  onClick={this.goodButtonClicked}>
                  <FavoriteIcon />
                </IconButton>
                <div className={css.facIconCounter}>{this.props.like_count}</div>
              </React.Fragment>
        }
      </div>
    )
  }
}

export default FavIconContainer;
