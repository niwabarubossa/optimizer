import React, { Component } from 'react';
import { firebaseLogout, loginWithTwitter, loginLoadingStart, loginLoadingFinish } from '../../actions'
import { connect } from 'react-redux'
import Button from '@material-ui/core/Button';
import css from '../../assets/loginPage/MainContainer.css'
import CircularProgress from '@material-ui/core/CircularProgress';

class MainContainer extends Component {

  async loginWithTwitter() {
    this.props.loginLoadingStart()
    await this.props.loginWithTwitter()
    this.props.history.push('/')
    this.props.loginLoadingFinish()
  }

  render() {

    return (
      <div>
        <div className={css.loginFormContainer}>
          {this.props.login_loading
            ?
            <CircularProgress />
            :
            this.props.current_user ?
              <React.Fragment>
                <h3>すでにログインしています</h3>
                <Button variant="outlined" color="primary" onClick={this.props.firebaseLogout}>
                  ログアウト
                </Button>
              </React.Fragment>
              :
              <React.Fragment>
                <Button variant="outlined" color="primary" onClick={this.loginWithTwitter.bind(this)}>
                  Twitterでログイン
                        </Button>
              </React.Fragment>

          }
        </div>
      </div>
    )
  }
}

const mapDispatchToProps = ({
  firebaseLogout,
  loginWithTwitter,
  loginLoadingStart,
  loginLoadingFinish
})
const mapStateToProps = (state) => {
  return {
    current_user: state.firebase.current_user,
    login_loading: state.firebase.login_loading
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(MainContainer)


