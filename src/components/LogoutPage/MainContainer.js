import React, { Component } from 'react';
import { connect } from 'react-redux'
import { loginWithTwitter, loginLoadingStart, loginLoadingFinish } from '../../actions'
import Button from '@material-ui/core/Button';

class MainContainer extends Component {

  async loginWithTwitter() {
    this.props.loginLoadingStart()
    await this.props.loginWithTwitter()
    this.props.history.push('/')
    this.props.loginLoadingFinish()
  }

  render() {
    return (
      <div>
        <h3>ログアウトしました</h3>
        <p>現在ゲストユーザーとしてログインしています。</p>

        <p>新たなTwitterアカウントで登録したい場合はこちらから</p>
        <Button variant="outlined" color="primary" onClick={this.loginWithTwitter.bind(this)}>
          Twitterでログイン
        </Button>
      </div>
    )
  }
}

const mapDispatchToProps = ({
  loginWithTwitter,
  loginLoadingStart,
  loginLoadingFinish
})

export default connect(null, mapDispatchToProps)(MainContainer)
