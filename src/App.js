import React, { Component } from 'react';
import MainContainer from './components/TopPage/containers/MainContainer'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import AppBarMain from './components/appbar/AppBarMain'
import InNav from './components/appbar/InNav'
import ActionLogTImeLinePage from './components/ActionLogTImeLinePage/MainContainer'
import LatestMainContainer from './components/TopPage/containers/LatestMainContainer'
import ManagementPageMainContainer from "./components/ManagementPage/containers/MainContainer";
import UserPage from './components/UserPage/UserPage'
import LoginPageMainContainer from './components/LoginPage/MainContainer'
// import FloatingActionButton from 'material-ui/FloatingActionButton'
// import ContentAdd from 'material-ui/svg-icons/content/add'
import SubmitPage from './components/SubmitPage/MainContainer'
import LogoutPage from './components/LogoutPage/MainContainer'
import SearchPageContainer from './components/SearchPage/SearchPageContainer'
import MyFavPostsPage from './components/MyFavPostsPage/MainContainer'
import UsagePage from './components/UsagePage/MainContainer'
import UserEditPage from './components/UserEditPage/containers/MainContainer'
import PermanentLoginPage from './components/PermanentLoginPage/MainContainer'
import { Link } from 'react-router-dom'
import ScrollToTop from './ScrollToTop'
import { compose } from 'redux'
import { connect } from 'react-redux'
import ToastMessage from './ToastMessage'

const drawerWidth = 240;
const styles = theme => ({
  root: {
    display: 'flex',
  },
  drawer: {
    [theme.breakpoints.up('sm')]: {
      width: drawerWidth,
      flexShrink: 0,
    },
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing.unit * 1,
  },
});

class App extends Component {
  constructor() {
    super();
    this.state = {
      showPopup: false
    };
  }
  togglePopup() {
    this.setState({
      showPopup: !this.state.showPopup
    });
  }

  render() {

    const AddBtnStyle = {
      position: "fixed",
      right: 12,
      bottom: 12,
      zIndex: 10
    }

    const { classes, theme } = this.props;
    return (
      <BrowserRouter>
        <ScrollToTop>
          <div className="App" style={{ textAlign: 'center', marginTop: '64px', boxSizing: 'border-box' }}>

            <div className={classes.root}>
              <AppBarMain />
              <nav className={classes.drawer}>
                <InNav />
              </nav>
              <main className={classes.content} style={{ textAlign: 'center' }}>
                <Route exact path="/popular_tweets" component={MainContainer} />
                <Route exact path="/" component={ActionLogTImeLinePage} />
                <Route exact path="/latest" component={LatestMainContainer} />
                <Route path="/management" component={ManagementPageMainContainer} />
                <Route path="/user/:id" component={UserPage} />
                <Route path="/edit" component={UserEditPage} />

                <Route path="/permanent_login" component={PermanentLoginPage} />

                <Route path="/login" component={LoginPageMainContainer} />
                <Route path="/logout" component={LogoutPage} />
                <Route path="/submit" component={SubmitPage} />
                <Route exact path="/search" component={SearchPageContainer} />
                <Route exact path="/search/:hashtag" component={SearchPageContainer} />
                <Route path="/favorite" component={MyFavPostsPage} />
                <Route path="/usage" component={UsagePage} />
              </main>
              <ToastMessage toast_type={this.props.toast_type} toast_message={this.props.toast_message} />
            </div>
            {/* <Link to={'/submit'} style={{ textDecoration: 'none', color: 'white' }} >
              <FloatingActionButton style={AddBtnStyle} >
                <ContentAdd />
              </FloatingActionButton>
            </Link> */}
          </div>

        </ScrollToTop>
      </BrowserRouter>
    );
  }
}

App.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => {
  return {
    toast_type: state.firebase.toast_type,
    toast_message: state.firebase.toast_message
  }
}
// export default withStyles(styles, { withTheme: true })(App);
export default compose(
  withStyles(styles, { withTheme: true }),
  connect(
    mapStateToProps,
    null
  ))(App)