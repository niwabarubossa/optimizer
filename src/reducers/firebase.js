import { FIREBASELOGOUT, SUBMITTWEET, GET_POSTS_SUCCESS, HANDLE_DRAWER_TOGGLE, HANDLE_DRAWER_TOGGLE_RESET, LOGIN_WITH_TWITTER, LOGIN_WITH_TWITTER_SUCCESS, GET_USER_INFORMATION, GET_USER_INFORMATION_SUCCESS, GET_DISPLAY_USER_INFORMATION, GET_DISPLAY_USER_INFORMATION_SUCCESS, GOOD_BUTTON_CLICKED, GET_WEEKLY_POSTS_SUCCESS, GET_USER_CHART_INFORMATION_SUCCESS, SET_CURRENT_USER_AND_IN_FIRESTORE, GET_MY_POSTS_SUCCESS, GET_DISPLAY_USER_TWEETS_SUCCESS, GET_MY_FAV_POSTS_SUCCESS, GET_SNAP_TWEETS_SUCCESS, MODIFIED, GET_MY_POSTS_SNAP_TWEETS_SUCCESS, MODIFIED_MY_POSTS, MODIFIED_MY_FAV_POSTS, DELETE_MY_FAV_POSTS, MODIFIED_DISPLAY_USER_TWEET, LOGIN_LOADING_START, LOGIN_LOADING_FINISH, SUBMIT_LOADING_START, SUBMIT_LOADING_FINISH, GOOD_BUTTON_LOADING_START, SEARCH_TWEETS, GOOD_BUTTON_IN_SEARCH_PAGE_LOADING_START, GOOD_BUTTON_LOADING_FINISH_IN_SEARCH_AND_REMOVE, GOOD_BUTTON_LOADING_FINISH_IN_SEARCH_AND_ADD, GET_MORE_POSTS_SUCCESS, GET_MORE_POSTS_MODIFIED, GET_MORE_POSTS_START, GOOD_BUTTON_LOADING_START_IN_USERPAGE, RESET_DISPLAY_USER_TWEETS, GET_WEEKLY_RANKING_POSTS_SUCCESS, WEEKLY_RANKING_POSTS_MODIFIED, WEEKLY_RANKING_POSTS_GOOD_LOADING_START, GET_POPULAR_HASHTAGS_SUCCESS, GET_MORE_POPULAR_HASHTAGS_SUCCESS, RESET_DISPLAY_USER, UPDATE_USER_INFORMATION_SUCCESS, UPDATE_USER_INFORMATION_FAILURE, DELETE_TWEET_LOADING_START, SNAPSHOT_DELETE_TWEET, SNAPSHOT_MY_POSTS_DELETE_TWEET, SEARCH_WEEKLY_RANKING_TWEETS, GOOD_BUTTON_LOADING_START_IN_TOP_PAGE, GOOD_BUTTON_LOADING_FINISH_IN_TOPPAGE_AND_REMOVE, GOOD_BUTTON_LOADING_FINISH_IN_TOPPAGE_AND_ADD, GET_USER_INFORMATION_FAILURE, FAV_POSTS_GOOD_LOADING_START, MY_POSTS_GOOD_LOADING_START, GET_WEEKLY_ACTION_LOGS_SUCCESS, ANONYMOUS_USER_STATUS, GET_ACTION_LOG_REQUEST_SUCCESS, MODIFIED_ACTION_LOG, DELETE_ACTION_LOG, ACTION_LOG_GOOD_BUTTON_LOADING_START, GET_DISPLAY_USER_ACTION_LOG_REQUEST_SUCCESS, MODIFIED_DISPLAY_USER_ACTION_LOG, DELETE_DISPLAY_USER_ACTION_LOG, GET_DISPLAY_USER_MONTHLY_POSTS_SUCCESS, GET_DISPLAY_USER_MONTHLY_ACTION_LOGS_SUCCESS, GET_MORE_ACTION_LOGS_MODIFIED, GET_MORE_ACTION_LOGS_SUCCESS, GET_MORE_ACTION_LOGS_START, GET_MORE_ACTION_LOGS_FINISH } from '../actions'
import firebase from 'firebase';
import 'firebase/firestore';
import produce from "immer";

const initialState = {
  anonymous_user: false,
  action_logs: [],
  display_user_action_logs: [],
  tweets: [],
  weekly_ranking_tweets: [],
  weekly_ranking_search_results: null,
  weekly_posts: [],
  popular_hashtags: [],
  more_posts_loading: false,
  display_user: null,
  search_results: [],
  first_tasks: [],
  second_tasks: [],
  my_weekly_action_logs: [],
  display_user_monthly_posts: [],
  display_user_monthly_action_logs: [],
  user_in_firestore: null,
  more_action_logs_loading: false
}

export default (state = initialState, action) => {
  switch (action.type) {
    case GET_USER_INFORMATION_FAILURE:
      return Object.assign({}, state, {
        login_loading: action.login_loading
      })
    case GOOD_BUTTON_LOADING_START_IN_TOP_PAGE:
      var weekly_ranking_search_results_index = state.weekly_ranking_search_results.findIndex(({ objectID }) => objectID === action.objectID);
      return produce(state, draft => {
        draft.weekly_ranking_search_results[weekly_ranking_search_results_index].good_loading = true
      });
    case SEARCH_WEEKLY_RANKING_TWEETS:
      return Object.assign({}, state, {
        weekly_ranking_search_results: action.weekly_ranking_search_results
      })
    case GOOD_BUTTON_LOADING_START:
      var array_index = state.tweets.findIndex(({ id }) => id === action.tweet_id);
      return produce(state, draft => {
        draft.tweets[array_index].good_loading = true
      });
    case ACTION_LOG_GOOD_BUTTON_LOADING_START:
      // var array_index = state.action_logs.findIndex(({ id }) => id === action.actoin_log_id);
      var array_index = state[action.state_name].findIndex(({ id }) => id === action.actoin_log_id);
      return produce(state, draft => {
        // draft.action_logs[array_index].good_loading = true
        draft[action.state_name][array_index].good_loading = true
      });
    case SUBMIT_LOADING_START:
      return Object.assign({}, state, {
        submit_loading: action.submit_loading
      })
    case SUBMIT_LOADING_FINISH:
      return Object.assign({}, state, {
        submit_loading: action.submit_loading,
        toast_type: 'success',
        toast_message: '完了しました'
      })
    case LOGIN_LOADING_START:
      return Object.assign({}, state, {
        login_loading: action.login_loading
      })
    case LOGIN_LOADING_FINISH:
      return Object.assign({}, state, {
        login_loading: action.login_loading
      })
    case DELETE_MY_FAV_POSTS:
      var index = state.my_fav_posts.findIndex((v) => v.created_at === action.delete_fav_posts.created_at);
      var new_my_fav_posts = Object.assign([], state.my_fav_posts, null)
      new_my_fav_posts.splice(index, 1)
      return Object.assign({}, state, {
        my_fav_posts: new_my_fav_posts
      })
    case MODIFIED_MY_FAV_POSTS:
      var index = state.my_fav_posts.findIndex((v) => v.created_at === action.modifed_tweets.created_at);
      state.my_fav_posts.splice(index, 1, action.modifed_tweets)
      return Object.assign({}, state, {
        my_fav_posts: state.my_fav_posts.concat([])
      })
    case GET_MY_POSTS_SNAP_TWEETS_SUCCESS:
      if (!state.my_posts) {
        state.my_posts = []
      }
      return Object.assign({}, state, {
        my_posts: state.my_posts.concat(action.my_posts)
      })
    case MODIFIED_MY_POSTS:
      var index = state.my_posts.findIndex((v) => v.created_at === action.modifed_tweets.created_at);
      var after_my_posts = Object.assign([], state.my_posts, null)
      after_my_posts.splice(index, 1, action.modifed_tweets)
      return Object.assign({}, state, {
        my_posts: after_my_posts
      })
    case SNAPSHOT_MY_POSTS_DELETE_TWEET:
      var index = state.my_posts.findIndex((v) => v.created_at === action.deleted_tweet.created_at);
      const new_my_posts = state.my_posts
      new_my_posts.splice(index, 1)
      return Object.assign({}, state, {
        my_posts: new_my_posts
      })
    case GET_SNAP_TWEETS_SUCCESS:
      return Object.assign({}, state, {
        tweets: state.tweets.concat(action.tweets)
      })
    case GET_DISPLAY_USER_TWEETS_SUCCESS:
      if (!state.display_user_tweets) {
        state.display_user_tweets = []
      }
      return Object.assign({}, state, {
        display_user_tweets: state.display_user_tweets.concat(action.display_user_tweets)
      })
    case RESET_DISPLAY_USER_TWEETS:
      return Object.assign({}, state, {
        display_user_tweets: []
      })
    case RESET_DISPLAY_USER:
      return Object.assign({}, state, {
        display_user: null,
        display_user_tweets: [],
        display_user_action_logs: [],
        display_user_monthly_posts: [],
        display_user_monthly_action_logs: []
      })
    case MODIFIED:
      var index = state.tweets.findIndex((v) => v.created_at === action.modifed_tweets.created_at);
      action.modifed_tweets.good_loading = false
      return produce(state, draft => {
        draft.tweets.splice(index, 1, action.modifed_tweets)
      });
    case SNAPSHOT_DELETE_TWEET:
      var delete_index = state.tweets.findIndex((v) => v.created_at === action.delete_tweet.created_at);
      return produce(state, draft => {
        draft.tweets.splice(delete_index, 1)
      });
    case WEEKLY_RANKING_POSTS_GOOD_LOADING_START:
      var weekly_ranking_posts_index = state.weekly_ranking_search_results.findIndex(({ id }) => id === action.tweet_id);
      return produce(state, draft => {
        draft.weekly_ranking_search_results[weekly_ranking_posts_index].good_loading = true
      });
    case FAV_POSTS_GOOD_LOADING_START:
      var fav_posts_index = state.my_fav_posts.findIndex(({ id }) => id === action.tweet_id);
      return produce(state, draft => {
        draft.my_fav_posts[fav_posts_index].good_loading = true
      });
    case MY_POSTS_GOOD_LOADING_START:
      var my_posts_index = state.my_posts.findIndex(({ id }) => id === action.tweet_id);
      return produce(state, draft => {
        draft.my_posts[my_posts_index].good_loading = true
      });
    case SET_CURRENT_USER_AND_IN_FIRESTORE:
      return Object.assign({}, state, {
        login_fetching: false,
        user_in_firestore: action.user_in_firestore,
        current_user: action.current_user
      })
    case MODIFIED_DISPLAY_USER_TWEET:
      var index = state.display_user_tweets.findIndex((v) => v.created_at === action.modifed_tweets.created_at);
      return produce(state, draft => {
        draft.display_user_tweets.splice(index, 1, action.modifed_tweets)
      });
    case GOOD_BUTTON_LOADING_START_IN_USERPAGE:
      const display_user_tweets_index = state.display_user_tweets.findIndex(({ id }) => id === action.tweet_id);
      return produce(state, draft => {
        draft.display_user_tweets[display_user_tweets_index].good_loading = true
      });
    case GET_MY_POSTS_SUCCESS:
      return Object.assign({}, state, {
        my_posts: action.my_posts
      })
    case GET_MY_FAV_POSTS_SUCCESS:
      if (!state.my_fav_posts)
        state.my_fav_posts = []
      return Object.assign({}, state, {
        my_fav_posts: state.my_fav_posts.concat(action.my_fav_posts)
      })
    case FIREBASELOGOUT:
      firebase.auth().signOut()
        .then(_ => {
        }, err => {
        });
      return Object.assign({}, state, {
        current_user: null,
        user_in_firestore: null,
        anonymous_user: false,
        display_user_action_logs: [],
        weekly_posts: [],
        display_user: null,
        my_weekly_action_logs: [],
        display_user_monthly_posts: [],
        display_user_monthly_action_logs: [],
        tweets: [],
      })
    case SUBMITTWEET:
      return state
    case GET_POSTS_SUCCESS:
      return Object.assign({}, state, {
        get_posts_fetching: false,
        mobileOpen: state.mobileOpen,
        isFetching: false,
        items: action.posts,
        lastUpdated: action.receivedAt
      })
    case GET_WEEKLY_POSTS_SUCCESS:
      return Object.assign({}, state, {
        weekly_posts: state.weekly_posts.concat(action.weekly_posts)
      })
    case HANDLE_DRAWER_TOGGLE:
      return Object.assign({}, state, {
        mobileOpen: !state.mobileOpen,
        isFetching: false,
        lastUpdated: action.receivedAt
      })
    case HANDLE_DRAWER_TOGGLE_RESET:
      return Object.assign({}, state, {
        mobileOpen: false,
        isFetching: false,
        lastUpdated: action.receivedAt
      })
    case LOGIN_WITH_TWITTER:
      return state
    case LOGIN_WITH_TWITTER_SUCCESS:
      return Object.assign({}, state, {
        anonymous_user: false
      })
    case GET_USER_INFORMATION:
      return state
    case GET_USER_INFORMATION_SUCCESS:
      return Object.assign({}, state, {
        current_user: action.current_user,
        user_in_firestore: action.user_in_firestore,
        login_loading: action.login_loading
      })
    case GET_DISPLAY_USER_INFORMATION:
      return state
    case GET_DISPLAY_USER_INFORMATION_SUCCESS:
      return Object.assign({}, state, {
        display_user: action.display_user,
      })
    case GOOD_BUTTON_CLICKED:
      return state
    case GET_USER_CHART_INFORMATION_SUCCESS:
      return Object.assign({}, state, {
        chart_user: action.chart_user,
      })
    case SEARCH_TWEETS:
      return Object.assign({}, state, {
        search_results: action.search_results
      })
    case GOOD_BUTTON_IN_SEARCH_PAGE_LOADING_START:
      const index_1 = state.search_results.findIndex(({ objectID }) => objectID === action.objectID);
      const data_1 = state.search_results[index_1]
      return produce(state, draft => {
        draft.search_results[index_1].good_loading = true
      })
    case GOOD_BUTTON_LOADING_FINISH_IN_SEARCH_AND_REMOVE:
      const index_2 = state.search_results.findIndex(({ objectID }) => objectID === action.objectID);
      const data_2 = state.search_results[index_2]
      return produce(state, draft => {
        draft.search_results[index_2].good_loading = false
        draft.search_results[index_2].like_count = data_2.like_count - 1
        draft.search_results[index_2].i_like = false
      })
    case GOOD_BUTTON_LOADING_FINISH_IN_SEARCH_AND_ADD:
      const index_3 = state.search_results.findIndex(({ objectID }) => objectID === action.objectID);
      const data_3 = state.search_results[index_3]
      return produce(state, draft => {
        draft.search_results[index_3].good_loading = false
        draft.search_results[index_3].like_count = data_3.like_count + 1
        draft.search_results[index_3].i_like = true
      });
    case GET_MORE_POSTS_START:
      return Object.assign({}, state, {
        more_posts_loading: true
      })
    case GET_MORE_POSTS_SUCCESS:
      return Object.assign({}, state, {
        tweets: state.tweets.concat(action.tweets),
        more_posts_loading: false
      })
    case GET_MORE_POSTS_MODIFIED:
      const index_4 = state.tweets.findIndex((v) => v.created_at === action.modified_tweet.created_at);
      action.modified_tweet.good_loading = false
      state.tweets.splice(index_4, 1, action.modified_tweet)
      return Object.assign({}, state, {
        tweets: state.tweets.concat([]),
        more_posts_loading: false
      })
    case GET_POPULAR_HASHTAGS_SUCCESS:
      return Object.assign({}, state, {
        popular_hashtags: action.popular_hashtags
      })
    case GET_MORE_POPULAR_HASHTAGS_SUCCESS:
      return Object.assign({}, state, {
        popular_hashtags: state.popular_hashtags.concat(action.more_popular_hashtags)
      })
    case UPDATE_USER_INFORMATION_SUCCESS:
      return state
    case UPDATE_USER_INFORMATION_FAILURE:
      return state
    case GOOD_BUTTON_LOADING_FINISH_IN_TOPPAGE_AND_REMOVE:
      var index_5 = state.weekly_ranking_search_results.findIndex(({ objectID }) => objectID === action.objectID);
      var data_5 = state.weekly_ranking_search_results[index_5]
      return produce(state, draft => {
        draft.weekly_ranking_search_results[index_5].good_loading = false
        draft.weekly_ranking_search_results[index_5].like_count = data_5.like_count - 1
        draft.weekly_ranking_search_results[index_5].i_like = false
      })
    case GOOD_BUTTON_LOADING_FINISH_IN_TOPPAGE_AND_ADD:
      var index_6 = state.weekly_ranking_search_results.findIndex(({ objectID }) => objectID === action.objectID);
      var data_6 = state.weekly_ranking_search_results[index_6]
      return produce(state, draft => {
        draft.weekly_ranking_search_results[index_6].good_loading = false
        draft.weekly_ranking_search_results[index_6].like_count = data_6.like_count + 1
        draft.weekly_ranking_search_results[index_6].i_like = true
      });
    case GET_WEEKLY_ACTION_LOGS_SUCCESS:
      return Object.assign({}, state, {
        my_weekly_action_logs: action.my_weekly_action_logs
      })
    case ANONYMOUS_USER_STATUS:
      return Object.assign({}, state, {
        anonymous_user: true
      })
    case GET_ACTION_LOG_REQUEST_SUCCESS:
      return Object.assign({}, state, {
        action_logs: state.action_logs.concat(action.action_logs)
      })
    case MODIFIED_ACTION_LOG:
      var index = state.action_logs.findIndex((v) => v.created_at === action.modifed_action_logs.created_at);
      action.modifed_action_logs.good_loading = false
      return produce(state, draft => {
        draft.action_logs.splice(index, 1, action.modifed_action_logs)
      });
    case DELETE_ACTION_LOG:
      //TODO deleteは即時に削除されない
      var index = state.action_logs.findIndex((v) => v.created_at === action.delete_action_logs.created_at);
      const new_action_logs = state.action_logs
      new_action_logs.splice(index, 1)
      return Object.assign({}, state, {
        action_logs: new_action_logs
      })
    case GET_DISPLAY_USER_ACTION_LOG_REQUEST_SUCCESS:
      return Object.assign({}, state, {
        display_user_action_logs: state.display_user_action_logs.concat(action.display_user_action_logs)
      })
    case MODIFIED_DISPLAY_USER_ACTION_LOG:
      var index = state.display_user_action_logs.findIndex((v) => v.created_at === action.modifed_display_user_action_logs.created_at);
      return produce(state, draft => {
        draft.display_user_action_logs.splice(index, 1, action.modifed_display_user_action_logs)
      });
    case DELETE_DISPLAY_USER_ACTION_LOG:
      var index = state.display_user_action_logs.findIndex((v) => v.created_at === action.delete_display_user_action_logs.created_at);
      return produce(state, draft => {
        draft.display_user_action_logs.splice(index, 1, action.modifed_display_user_action_logs)
      });
    case GET_DISPLAY_USER_MONTHLY_POSTS_SUCCESS:
      return Object.assign({}, state, {
        display_user_monthly_posts: action.display_user_monthly_posts
      })
    case GET_DISPLAY_USER_MONTHLY_ACTION_LOGS_SUCCESS:
      return Object.assign({}, state, {
        display_user_monthly_action_logs: action.display_user_monthly_action_logs
      })
    case GET_MORE_ACTION_LOGS_SUCCESS:
      return Object.assign({}, state, {
        action_logs: state.action_logs.concat(action.more_action_logs)
      })
    case GET_MORE_ACTION_LOGS_MODIFIED:
      var index = state.action_logs.findIndex((v) => v.created_at === action.modified_action_log.created_at);
      return produce(state, draft => {
        draft.action_logs.splice(index, 1, action.modified_action_log)
      });
    case GET_MORE_ACTION_LOGS_START:
      return produce(state, draft => {
        draft.more_action_logs_loading = true
      });
    case GET_MORE_ACTION_LOGS_FINISH:
      return produce(state, draft => {
        draft.more_action_logs_loading = false
      });
    default:
      return state
  }
}