// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// The Cloud Functions for Firebase SDK to create Cloud Functions and setup triggers.
const functions = require('firebase-functions');

// The Firebase Admin SDK to access the Firebase Realtime Database.
const admin = require('firebase-admin');
admin.initializeApp();

// databaseの参照を作成
var fireStore = admin.firestore()

const algoliasearch = require("algoliasearch")
const ALGOLIA_ID = functions.config().algolia.app_id
const ALGOLIA_ADMIN_KEY = functions.config().algolia.api_key
const ALGOLIA_SEARCH_KEY = functions.config().algolia.search_key
const client = algoliasearch(ALGOLIA_ID, ALGOLIA_ADMIN_KEY)

exports.detectHashtagCreate = functions.firestore.document('hashtag/{hashtagId}').onCreate((snap, context) => {
  const data = snap.data()
  const hashtag_id = context.params.hashtagId
  const ALGOLIA_INDEX_NAME = "hashtag_search"
  const hashtag_index = client.initIndex(ALGOLIA_INDEX_NAME)
  const objects = [{
    objectID: hashtag_id,
    name: data.name,
    amount: data.amount
  }];
  return hashtag_index.addObjects(objects, (err, content) => {
  });
})

exports.detectHashtagUpdate = functions.firestore.document('hashtag/{hashtagId}').onUpdate((change, context) => {
  const newValue = change.after.data();
  const hashtag_id = context.params.hashtagId
  const ALGOLIA_INDEX_NAME = "hashtag_search"
  const hashtag_index = client.initIndex(ALGOLIA_INDEX_NAME)
  const objects = [{
    objectID: hashtag_id,
    name: newValue.name,
    amount: newValue.amount
  }];
  return hashtag_index.saveObjects(objects, (err, content) => {
  });
})

exports.detectTweetCreate = functions.firestore.document('tweets/{testId}').onCreate((snap, context) => {
  const data = snap.data()
  data.objectID = context.params.testId
  const ALGOLIA_INDEX_NAME = "body_search"
  const index = client.initIndex(ALGOLIA_INDEX_NAME)
  return index.addObject(data)
})

exports.httpFavCreate = functions.https.onCall((data, context) => {
  console.log(request)
  var tweetRef = fireStore.collection('tweets').doc(data)
  tweetRef.get().then(doc => {
    if (!doc.exists) {
      response.send('No such document!')
      console.log('no such document')
    } else {
      console.log(doc.data())
    }
  })
    .catch(err => {
      console.log('detectFavCreate error')
    })
  // tweetRef.update({
  //     likers: admin.firestore.FieldValue.arrayUnion(data)
  // });
  tweetRef.update({
    likers: firebase.firestore.FieldValue.arrayUnion(data, true)
  })
  return null;
})

exports.helloWorld = functions.https.onRequest((request, response) => {
  response.send("Hello from Firebase!");
  console.log('hello firebase')
});

exports.addMessage = functions.https.onCall((data, context) => {
  console.log(data)
  // { text: 'aaaaaa' }
  console.log(context)
  // addMessage({text: 'aaaaaa'}
  const aiueo = 'aiueo'
  return aiueo
  //{ data: 'aiueo' }
});

exports.detectFavCreate = functions.firestore.document('tweets/{tweetId}/liker/{likerId}').onCreate((change, context) => {
  var tweetRef = fireStore.collection('tweets').doc(context.params.tweetId)
  tweetRef.get().then(function (doc) {
    if (doc.exists) {
      console.log('doc.data()')
      console.log(doc.data())
      var userRef = fireStore.collection('users').doc(doc.data().author_id)
      userRef.update({
        contribution: admin.firestore.FieldValue.increment(1.0),
      });
    } else {
    }
  }).catch(function (error) {
    console.log("Error getting document:", error);
  });

  return tweetRef.update({
    like_count: admin.firestore.FieldValue.increment(1.0),
    likers: admin.firestore.FieldValue.arrayUnion(context.params.likerId)
  });
})

exports.detectFavDelete = functions.firestore.document('tweets/{tweetId}/liker/{likerId}').onDelete((change, context) => {
  //user情報を取得して contribution をインクリメント
  var tweetRef = fireStore.collection('tweets').doc(context.params.tweetId)
  tweetRef.get().then(function (doc) {
    if (doc.exists) {
      var userRef = fireStore.collection('users').doc(doc.data().author_id)
      userRef.update({
        contribution: admin.firestore.FieldValue.increment(-1.0),
      });
    } else {
    }
  }).catch(function (error) {
    console.log("Error getting document:", error);
  });

  return tweetRef.update({
    like_count: admin.firestore.FieldValue.increment(-1.0),
    likers: admin.firestore.FieldValue.arrayRemove(context.params.likerId)
  });
})

exports.detectTweetUpdate = functions.firestore.document('tweets/{tweetId}').onUpdate((change, context) => {
  const newData = change.after.data();
  const tweet_id = context.params.tweetId
  const ALGOLIA_INDEX_NAME = "body_search"
  const index = client.initIndex(ALGOLIA_INDEX_NAME)

  //weeklyにもあれば、
  index.partialUpdateObject({
    objectID: tweet_id,
    likers: newData.likers,
    like_count: newData.like_count
  }, (err, content) => {
    if (err) throw err;
    console.log(content);
  });
  return null
});

exports.detectTweetDelete = functions.firestore.document('tweets/{tweetId}').onDelete((change, context) => {
  const ALGOLIA_INDEX_NAME = "body_search"
  const index = client.initIndex(ALGOLIA_INDEX_NAME)
  index.deleteObject(context.params.tweetId, (err, content) => {
    if (err) throw err;
    console.log(content);
  });
})


exports.detectRootActionCreate = functions.firestore.document('actions/{actionId}').onCreate((snap, context) => {
  const data = snap.data()
  const ALGOLIA_INDEX_NAME = "action_log_suggest"
  const action_log_index = client.initIndex(ALGOLIA_INDEX_NAME)
  const objectID = context.params.actionId
  const objects = [{
    objectID: objectID,
    id: data.id,
    name: data.name,
    log_amount: data.log_amount,
    kaizen_amount: data.kaizen_amount,
    total_done_actions_amount: data.total_done_actions_amount,
    total_logs_score: data.total_logs_score
  }];
  return action_log_index.addObjects(objects, (err, content) => {
  });
});

exports.detectRootActionUpdate = functions.firestore.document('actions/{actionId}').onUpdate((change, context) => {
  const new_data = change.after.data();
  const ALGOLIA_INDEX_NAME = "action_log_suggest"
  const action_log_index = client.initIndex(ALGOLIA_INDEX_NAME)
  const objectId = context.params.actionId
  const objects = [{
    objectID: objectId,
    id: new_data.id,
    name: new_data.name,
    log_amount: new_data.log_amount,
    kaizen_amount: new_data.kaizen_amount,
    total_done_actions_amount: new_data.total_done_actions_amount,
    total_logs_score: new_data.total_logs_score
  }];
  return action_log_index.saveObjects(objects, (err, content) => {
  });
})

exports.detectActionLogFavCreate = functions.firestore.document('action_logs/{actionLogId}/liker/{likerId}').onCreate((change, context) => {
  const actionLogRef = fireStore.collection('action_logs').doc(context.params.actionLogId)
  actionLogRef.get().then(function (doc) {
    if (doc.exists) {
      console.log(doc.data())
      var userActoinLogRef = fireStore.collection('users').doc(doc.data().author_id).collection('action_logs').doc(context.params.actionLogId)
      userActoinLogRef.update({
        like_count: admin.firestore.FieldValue.increment(1.0),
        likers: admin.firestore.FieldValue.arrayUnion(context.params.likerId)
      });
    } else {
    }
  }).catch(function (error) {
    console.log("Error getting document:", error);
  });
  return actionLogRef.update({
    like_count: admin.firestore.FieldValue.increment(1.0),
    likers: admin.firestore.FieldValue.arrayUnion(context.params.likerId)
  });
})

exports.detectActionLogFavDelete = functions.firestore.document('action_logs/{actionLogId}/liker/{likerId}').onDelete((change, context) => {
  const actionLogRef = fireStore.collection('action_logs').doc(context.params.actionLogId)
  actionLogRef.get().then(function (doc) {
    if (doc.exists) {
      console.log(doc.data())
      var userActoinLogRef = fireStore.collection('users').doc(doc.data().author_id).collection('action_logs').doc(context.params.actionLogId)
      userActoinLogRef.update({
        like_count: admin.firestore.FieldValue.increment(-1.0),
        likers: admin.firestore.FieldValue.arrayRemove(context.params.likerId)
      });
    } else {
    }
  }).catch(function (error) {
    console.log("Error getting document:", error);
  });
  return actionLogRef.update({
    like_count: admin.firestore.FieldValue.increment(-1.0),
    likers: admin.firestore.FieldValue.arrayRemove(context.params.likerId)
  });
})